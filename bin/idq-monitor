#!/usr/bin/env python

__usage__ = "idq-monitor [--options] config.ini [gps_start [gps_end]]"
__description__ = "an executable that monitors streaming processes and generates metrics for online reporting"
__author__ = "Patrick Godwin(patrick.godwin@ligo.org)"
__doc__ = "\n\n".join([__usage__, __description__, __author__])

#-------------------------------------------------

from optparse import OptionParser
import os
import sys
import traceback

import numpy as np

from ligo.scald.io import hdf5, influx

from idq import configparser
from idq import factories
from idq import logs
from idq import names
from idq import stream
from idq import utils

#-------------------------------------------------
### constants

DEFAULT_AGGREGATOR_PROCESSES = 2
DEFAULT_METRICS = ('pglitch', 'loglike', 'rank', 'latency')

#-------------------------------------------------
### functions

def monitor(config_path, gps_start=None, gps_end=None, verbose=False):
    """
    set up a monitor to view iDQ online pipeline health and statistics
    """
    ### set up gps ranges
    gps_start, gps_end = stream.gps_range(gps_start=gps_start, gps_end=gps_end)

    ### read in config
    config = configparser.path2config(config_path)

    ### define factories to generate objects
    reporter_factory = factories.ReporterFactory()
    classifier_factory = factories.ClassifierFactory()

    #-----------------------------------------
    ### extract common parameters
    #-----------------------------------------
    tag = config.tag

    rootdir = os.path.abspath(config.rootdir)
    timeseriesdir = names.tag2timeseriesdir(tag, rootdir=rootdir)
    monitordir = names.tag2monitordir(tag, rootdir=rootdir)
    logdir = names.tag2logdir(tag, rootdir=rootdir)

    if not os.path.exists(monitordir):
        os.makedirs(monitordir)

    ### extract information necessary for channel names in timeseries files
    instrument = config.instrument

    target_bounds = configparser.config2bounds(config.samples['target_bounds'])
    assert 'frequency' in target_bounds, 'must specify a frequency range within target_bounds'
    freq_min, freq_max = target_bounds['frequency']
    assert isinstance(freq_min, int) and isinstance(freq_max, int), 'frequency bounds must be integers!'

    ### extract aggregation configuration
    assert 'backend' in config.monitor, 'backend not specified in monitor section in config file!'
    agg_backend = config.monitor['backend']

    #-----------------------------------------
    ### set up logging
    #-----------------------------------------
    if verbose:
        print("writing log : %s"%logdir)

    logger = logs.get_logger(
        names.tag2monitor_logname(tag),
        log_level=config.timeseries['log_level'],
        rootdir=logdir,
        verbose=verbose,
    )
    logger.info( "using config : %s"%config_path )

    #------------------------------------------
    ### set up aggregator
    #------------------------------------------

    logger.info( 'setting up aggregator with backend: {}'.format(agg_backend) )

    if agg_backend == 'hdf5':
        aggregator = hdf5.Aggregator(
            rootdir=monitordir,
            num_processes=DEFAULT_AGGREGATOR_PROCESSES,
            webdir=monitordir,
            reduce_across_tags=False,
        )
    elif agg_backend == 'influx':
        aggregator = influx.Aggregator(
            hostname=config.monitor['hostname'],
            port=config.monitor['port'],
            db=config.monitor['database_name'],
            webdir=monitordir,
            reduce_across_tags=False,
        )
    else:
        raise ValueError( '{} backend is not a valid option for monitor'.format(agg_backend) )

    ### register schema for stored metrics
    for metric in DEFAULT_METRICS:
        aggregator.register_schema(metric, columns='data', column_key='data', tags='job', tag_key='job')

    #-----------------------------------------
    ### set up how we record results
    #-----------------------------------------

    ### the boundaries for these will be re-set within the main loop, hence, set to zero initially
    ritems = config.timeseries['reporting']
    timeseriesreporter = reporter_factory(timeseriesdir, 0, 0, **configparser.add_missing_kwargs(ritems, group=names.tag2group(tag, 'timeseries')))

    #-----------------------------------------
    ### figure out which classifiers we'll run
    #-----------------------------------------
    klassifiers = [] ### funny spelling to avoid conflict with module named "classifiers"
    for nickname in config.classifiers:
        items = config.items(nickname)
        logger.info( nickname +' -> ' + ' '.join('%s:%s'%(k, v) for k, v in items.items()))

        ### instantiate the object and add it to the list
        klassifiers.append(classifier_factory(nickname, rootdir=monitordir, **items))

    #-----------------------------------------
    ### figure out how we'll read data
    #-----------------------------------------

    # set up cadence manager
    sitems = config.timeseries['stream'] ### NOTE: THIS IS NOT A TYPO. We need the timeseries and reporting jobs to be tightly linked
                                         ### I'm very intentionally taking the stream cadence from the timeseries jobs for the reporting jobs

    logger.info('cadence_manager -> '+' '.join('%s:%s'%(k, v) for k, v in sitems.items()))

    assert ('max_timestamp' not in sitems), 'max_timestamp is set by hand to gps_end and cannot be passed through INI file via timeseries stream'
    manager = utils.CadenceManager(gps_start, max_timestamp=gps_end, logger=logger, **sitems)

    timeseries_nicknames = dict((classifier.nickname, names.nickname2timeseries_topic(instrument, classifier.nickname)) for classifier in klassifiers)
    templates = dict((classifier.nickname, names.channel_name_template(instrument, classifier.nickname, freq_min, freq_max)) for classifier in klassifiers)

    #-----------------------------------------
    ### run monitoring job
    #-----------------------------------------

    ### start up streaming pipeline
    logger.info('starting online monitor')
    while manager.timestamp < gps_end:

        logger.info('--- monitor stride: [%.3f, %.3f) ---'%manager.seg)

        timestamp, _ = manager.wait() ### manage cadence and sleep logic here

        ### acquire timeseries from latest stride
        for classifier in klassifiers:
            nickname = classifier.nickname

            ### grab data products
            logger.info( 'retrieving %s timeseries from disk'%nickname )
            timeseries = timeseriesreporter.retrieve(timeseries_nicknames[nickname], preferred=True)

            if timeseries is not None:
                for series in timeseries:
                    data = series['data']

                    ### format metrics
                    times = np.arange(data[templates[nickname]%'LOGLIKE'].size) * series['deltaT'] + series['t0']

                    columns = {metric: data[templates[nickname]%metric.upper()].tolist() for metric in DEFAULT_METRICS if metric != 'latency'}
                    columns.update({'latency': utils.gps2latency(times)})

                    ### store and aggregate metrics
                    logger.info( 'storing p(glitch), log-likelihood, rank, latency metrics' )
                    for metric, column in columns.items():
                        aggregator.store_columns(metric, {'timeseries': {'time': times, 'fields': {'data': column}}}, aggregate='max')

    #--- not strictly necessary, but it's nice to end with a return statement...
    return None


#-------------------------------------------------
### main

if __name__ == '__main__':

    ### parse arguments
    parser = OptionParser(usage=__usage__, description=__description__)

    parser.add_option('-v', '--verbose', default=False, action='store_true',
        help='print to stdout in addition to writing to automatically generated log')

    opts, args = parser.parse_args()

    assert len(args) >= 1 and len(args) <= 3, 'please supply between 1 and 3 input arguments\n%s'%__usage__

    config_path = args[0]
    args[1:] = [float(_) for _ in args[1:]]
    gps_start, gps_end = utils.parse_variable_args(args[1:], 2)

    try:
        monitor(config_path, gps_start=gps_start, gps_end=gps_end, **vars(opts))

    except Exception as e: ### allow us to clean up forked jobs if needed (no zombies for us!)
        trcbck = traceback.format_exc().strip("\n")

        ### print to log for safe-keeping
        config = configparser.path2config(config_path)
        tag = config.tag
        logger = logs.configure_logger(
            stream.logger,
            names.tag2monitor_logname(tag), #logname
            log_level=config.timeseries['log_level'],
            rootdir=names.tag2logdir(tag, rootdir=os.path.abspath(config.rootdir)),
            verbose=opts.verbose,
        )
        logger.error(trcbck)

        sys.exit(1)
