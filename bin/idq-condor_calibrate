#!/usr/bin/env python

__usage__ = "idq-condor_calibrate [--options] gps_start gps_end calibratedir nickname"
__description__ = "an executable used within calibration's delegation to condor. Expects all data necessary to be already written to disk, including a pickled version of the classifier object and Reporter object, and then just picks it up and runs it."
__author__ = "Reed Essick (reed.essick@ligo.org)"
__doc__ = "\n\n".join([__usage__, __description__, __author__])

#---------------------------------------------------------------------------------------------------

from optparse import OptionParser

### dependencies not in standard Python libraries
from idq import batch

#---------------------------------------------------------------------------------------------------

### parser arguments
parser = OptionParser(usage=__usage__, description=__description__)

parser.add_option('-v', '--verbose', default=False, action='store_true',
    help='print to stdout in addition to writing to automatically generated log')

opts, args = parser.parse_args()

assert len(args)==4, 'please supply exactly 4 input arguments\n%s'%__usage__
gps_start, gps_end = [float(_) for _ in args[:2]]
assert gps_end > gps_start, "gps_end (%.3f) must be > gps_start (%.3f)!"%(gps_end, gps_start)

calibratedir, nickname = args[2:]

#-------------------------------------------------

batch.condor_calibrate(gps_start, gps_end, calibratedir, nickname, **vars(opts))
