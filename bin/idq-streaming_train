#!/usr/bin/env python

__usage__ = "idq-streaming_train [--options] config.ini [gps_start [gps_end]]"
__description__ = "an executable to generate trained classifiers. This is meant as a streaming implementation"
__author__ = "Reed Essick (reed.essick@ligo.org)"
__doc__ = "\n\n".join([__usage__, __description__, __author__])

#---------------------------------------------------------------------------------------------------

import os
import sys
import traceback
from optparse import OptionParser

### dependencies not in standard Python libraries
from idq import configparser
from idq import logs
from idq import names
from idq import stream
from idq import utils

#---------------------------------------------------------------------------------------------------

### parse arguments
parser = OptionParser(usage=__usage__, description=__description__)

parser.add_option('-v', '--verbose', default=False, action='store_true',
    help='print to stdout in addition to writing to automatically generated log')

parser.add_option('--initial-lookback', default=stream.DEFAULT_INITIAL_LOOKBACK, type=float,
    help='start off classifiers with this much lookback time. DEFAULT=%.3f'%stream.DEFAULT_INITIAL_LOOKBACK)

opts, args = parser.parse_args()

assert len(args) >= 1 and len(args) <= 3, 'please supply between 1 and 3 input arguments\n%s'%__usage__

config_path = args[0]
args[1:] = [float(_) for _ in args[1:]]
gps_start, gps_end = utils.parse_variable_args(args[1:], 2)

#-------------------------------------------------

try:
    stream.train(config_path, gps_start=gps_start, gps_end=gps_end, **vars(opts))

except Exception as e: ### allow us to clean up forked jobs if needed (no zombies for us!)
    trcbck = traceback.format_exc().strip("\n")

    ### print to log for safe-keeping
    config = configparser.path2config(config_path)
    tag = config.tag
    logger = logs.configure_logger(
        stream.logger,
        names.tag2train_logname(tag),
        log_level=config.train['log_level'],
        rootdir=names.tag2logdir(tag, rootdir=os.path.abspath(config.rootdir)),
        verbose=opts.verbose,
    )
    logger.error(trcbck)

    sys.exit(1)
