#!/usr/bin/env python

"""a quick little tool to check how many target times live within a stretch of data. Performs the necessary queries and then prints the corresponding times
"""
__author__ = "Reed Essick (reed.essick@ligo.org)"

#-------------------------------------------------

from argparse import ArgumentParser

from ligo.segments import segment, segmentlist

from idq import factories
from idq import configparser
from idq import utils
from idq import names


#-------------------------------------------------

parser = ArgumentParser(description=__doc__)

parser.add_argument('config', type=str)
parser.add_argument('gps_start', type=float)
parser.add_argument('gps_end', type=float)

parser.add_argument('-v', '--verbose', default=False, action='store_true')
parser.add_argument('--job', default='train', type=str, help='the job you want to check. This is used to look up the section in your config. \
eg, to use [train data discovery] you should specify "train". \
DEFAULT=train')

parser.add_argument('-d', '--delimiter', default=',', type=str)
parser.add_argument('-e', '--exclude', default=[], type=str, action='append',
    help='exclude this column from the resulting table. Can exclude multiple columns by repeating this argument. Note, users cannot exclude the "time" column')

args = parser.parse_args()

#-------------------------------------------------

if args.verbose:
    print('reading config from: '+args.config)
config = configparser.path2config(args.config)
tag = config.tag

target_channel = config.samples['target_channel']
target_bounds = configparser.config2bounds(config.samples['target_bounds'])

items = getattr(config, args.job)['data_discovery']
items = configparser.add_missing_kwargs(items, group=names.tag2group(tag, args.job))
time = items.pop('time')

#------------------------

if items['ignore_segdb']:
    if args.verbose:
        print("ignoring segdb")
    segs = segmentlist([segment(args.gps_start, args.gps_end)])

else:
    segdb_intersect = config.segments["intersect"].split() if "intersect" in config.segments else []
    segdb_exclude = config.segments["exclude"].split() if "exclude" in config.segments else []
    segdb_url = config.segments["segdb_url"]

    if args.verbose:
        print("querying %s within [%.3f, %.3f) for: intersect=%s ; exclude=%s"%(segdb_url, args.gps_start, args.gps_end, ','.join(segdb_intersect), ','.join(segdb_exclude)))

    segs = utils.segdb2segs(
        args.gps_start,
        args.gps_end,
        intersect=segdb_intersect,
        exclude=segdb_exclude,
        segdb_url=segdb_url,
    )
    if args.verbose:
        print("retained %.3f sec of livetime"%utils.livetime(segs))

#------------------------

if args.verbose:
    print('constructing classifier data for '+args.job)
classifier_data = factories.ClassifierDataFactory()(
    args.gps_start,
    args.gps_end,
    segs=segs,
    **items
)

if args.verbose:
    print('identifying target times')
triggers = utils.filter_triggers(classifier_data.triggers(channels=target_channel)[target_channel], bounds=target_bounds)

if args.verbose:
    print('identified %d target_times'%len(triggers))
names = [time]+[_ for _ in triggers.dtype.names if (_ != time) and (_ not in args.exclude)]
print(args.delimiter.join(names))

fmt = args.delimiter.join('%.6f' for _ in names)
for trg in triggers[names]:
    print(fmt%tuple(trg))
