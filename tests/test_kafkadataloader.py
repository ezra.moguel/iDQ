__description__ = "a module that tests several aspects of KafkaDataLoader"
__author__ = "Patrick Godwin (patrick.godwin@ligo.org)"
__doc__ = "\n\n".join([__description__, __author__])

#--------------------------------------------------------------------------

import sys
import pytest

import numpy as np

from idq import exceptions
from idq import utils

#--------------------------------------------------------------------------

#----------------------
#        tests
#----------------------

@pytest.mark.usefixtures('kafka_dataloader')
class TestKafkaDataLoader(object):
    """
    Tests several aspects of KafkaDataLoader.
    """
    def test_times(self, kafka_dataloader_conf):
        assert self.dataloader.start == kafka_dataloader_conf['start_time'], 'incorrect start time set'
        assert self.dataloader.end == kafka_dataloader_conf['end_time'], 'incorrect end time set'
        
    def test_columns(self, kafka_dataloader_conf):
        triggers = {}
        while not triggers: ### keep fetching for triggers if there's no data available
            try:
                triggers = self.dataloader.query()
            except (exceptions.NoDataError, exceptions.IncontiguousDataError):
                continue

        assert set(self.dataloader.columns) == set(kafka_dataloader_conf['columns']), 'incorrect columns retrieved from DataLoader'

    def test_channels(self, kafka_dataloader_conf):
        triggers = {}
        while not triggers: ### keep fetching for triggers if there's no data available
            try:
                triggers = self.dataloader.query()
            except exceptions.NoDataError:
                continue

        assert set(self.dataloader.channels) == set(kafka_dataloader_conf['channels']), 'incorrect channels stored in DataLoader'

    def test_triggers(self, kafka_dataloader_conf):
        triggers = {}
        while not triggers: ### keep fetching for triggers if there's no data available
            try:
                triggers = self.dataloader.query()
            except exceptions.NoDataError:
                continue

    def test_pop(self, kafka_dataloader_conf):
        triggers = {}
        while not triggers: ### keep fetching for triggers if there's no data available
            try:
                triggers = self.dataloader.query()
            except exceptions.NoDataError:
                continue

        test_channel = kafka_dataloader_conf['channels'][0]
        test_triggers = self.dataloader.pop(test_channel)
        assert test_channel not in self.dataloader.channels, 'popped channel still present in DataLoader'
