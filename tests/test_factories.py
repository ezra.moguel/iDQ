__description__ = "a module that tests several aspects of the io submodule"
__author__ = "Patrick Godwin (patrick.godwin@ligo.org)"
__doc__ = "\n\n".join([__description__, __author__])

#--------------------------------------------------------------------------

import sys
import pytest

import numpy as np

from idq import configparser
from idq import factories
from idq import utils
from idq.io.triggers import snax

#--------------------------------------------------------------------------

#----------------------
#        tests
#----------------------

@pytest.mark.usefixtures('snax_dataloader')
class TestFactories(object):
    """
    Tests several features within Factory classes.
    """
    def test_dataloader_factory(self, snax_dataloader_conf):
        conf = snax_dataloader_conf
        conf['flavor'] = 'snax:hdf5'

        factory = factories.DataLoaderFactory()
        start_time = snax_dataloader_conf['start_time']
        end_time = snax_dataloader_conf['end_time']
        newdataloader = factory(start_time, end_time, **conf)

        ### check that this factory generated object derives from the right class
        ### and has the same properties
        assert isinstance(newdataloader, snax.SNAXDataLoader), 'factory-generated DataLoader not the right class'
        assert newdataloader.start == self.dataloader.start, 'factory-generated DataLoader start time not set properly'
        assert newdataloader.end == self.dataloader.end, 'factory-generated DataLoader end time not set properly'
        assert newdataloader.segs == self.dataloader.segs, 'factory-generated DataLoader segs not set properly'
        assert newdataloader.columns == self.dataloader.columns, 'factory-generated DataLoader columns not set properly'
        assert newdataloader.channels == self.dataloader.channels, 'factory-generated DataLoader channels not set properly'

    def test_dataset_factory_labeled(self, snax_dataloader_conf):
        target_channel = snax_dataloader_conf['channels'][0]
        target_bounds = configparser.config2bounds({'snr': [7, 'inf']})
        dirty_bounds = configparser.config2bounds({'snr': [4, 'inf']})
        dirty_window = 0.2
        time = 'time'
        rate = 0.0625

        target_times = self.dataloader.target_times(time, target_channel, target_bounds)
        random_times, _ = self.dataloader.random_times(time, target_channel, dirty_bounds, dirty_window, rate)
        dataset = factories.DatasetFactory(self.dataloader).labeled(target_times, random_times)

        assert len(dataset) == (len(target_times) + len(random_times)), 'dataset not the right size'
        assert dataset.segs == self.dataloader.segs, 'dataset segs not set properly'
        assert dataset.start == self.dataloader.start, 'dataset start time not set properly'
        assert dataset.end == self.dataloader.end, 'dataset end time not set properly'

        num_glitch, num_clean = dataset.vectors2num()
        assert num_glitch == len(target_times), 'dataset does not have the right number of glitch samples'
        assert num_clean == len(random_times), 'dataset does not have the right number of clean samples'

    def test_dataset_factory_unlabeled(self, snax_dataloader_conf):
        dt = 0.0625
        dataset = factories.DatasetFactory(self.dataloader).unlabeled(dt=dt)

        assert len(dataset) == (utils.livetime(self.dataloader.segs) / 0.0625)
        assert dataset.segs == self.dataloader.segs, 'dataset segs not set properly'
        assert dataset.start == self.dataloader.start, 'dataset start time not set properly'
        assert dataset.end == self.dataloader.end, 'dataset end time not set properly'

        for vector in dataset:
            assert np.isnan(vector.label), 'feature vector should be unlabeled'
            assert np.isnan(vector.rank), 'feature vector should be unranked'
