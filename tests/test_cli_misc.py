__description__ = "a module that tests other misc scripts"
__author__ = "Patrick Godwin (patrick.godwin@ligo.org)"
__doc__ = "\n\n".join([__description__, __author__])


def test_cli_check_config(script_runner):
    ret = script_runner.run("idq-check_config", "--help")
    assert ret.success


def test_cli_simtimeseries(script_runner):
    ret = script_runner.run("idq-simtimeseries", "--help")
    assert ret.success


def test_cli_target_times(script_runner):
    ret = script_runner.run("idq-target-times", "--help")
    assert ret.success
