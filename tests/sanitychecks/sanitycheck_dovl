#!/usr/bin/python

__usage__ = 'sanitycheck_ovl [--options] start end'
__doc__ = 'basic sanity checking for OVL to shake bugs out of the plumbing. We rely on a KWMClassifierData instance to retrieve data.'
__author__ = 'reed.essick@ligo.org'

#-------------------------------------------------

import os

from optparse import OptionParser

### non-standard libraries
from idq import utils
from idq import io
from idq import classifiers
from idq import features

#-------------------------------------------------

parser = OptionParser(usage=__usage__, description=__doc__)

parser.add_option('-v', '--verbose', default=False, action='store_true')
parser.add_option('-V', '--Verbose', default=False, action='store_true')

parser.add_option('', '--channel', default='L1_CAL-DELTAL_EXTERNAL_DQ_32_2048', type='string')
parser.add_option('', '--significance', default=30, type='float')
parser.add_option('', '--channels', default='channels.txt', type='string')

parser.add_option('', '--kws-rootdir', default='/gds-l1/dmt/triggers/L-KW_TRIGGERS', type='string')

parser.add_option('-o', '--outdir', default='.', type='string')

opts, args = parser.parse_args()
assert len(args)==2, 'please supply exactly 2 input arguments\n%s'%__usage__
start, end = [float(_) for _ in args]

if not os.path.exists(opts.outdir):
    os.makedirs(opts.outdir)

channels = io.path2channels(opts.channels)
time = 'time'
signif = 'significance'
columns = [time, signif]

opts.verbose |= opts.Verbose

#-------------------------------------------------

if opts.verbose:
    print('instantiating KWMClassifierData')
kws = io.KWSClassifierData(start, end, rootdir=opts.kws_rootdir, columns=columns)

### identify the glitch times
if opts.verbose:
    print('building quiver')
# identify glitchy times
gch_times = kws.triggers(opts.channel, verbose=opts.Verbose)[opts.channel]
gch_times = gch_times['time'][gch_times['significance']>=opts.significance]

# identify clean times
dirty_seg = utils.times2segments(gch_times, 0.1) ### hard-coded window for dirty-segs
random_times = utils.draw_random_times(utils.remove_segments(kws.segs, dirty_seg), rate=0.01) ### hard-coded rate

# actually build the quiver object
quiver = features.QuiverFactory(kws).labeled(
    gch_times,
    random_times,
)

#------------------------

if opts.verbose:
    print('instantiating DOVL')
dovl = classifiers.DOVL(
    'test_dovl',
    opts.outdir,
    incremental=10,
    minima={'eff_fap':0.5, 'poisson_signif':0.1},
    num_recalculate=10,
    metric='eff_fap',
    safe_channels_path=opts.channels,
    time=time, 
    significance=signif,
)

#--- actually do stuff now
if opts.verbose:
    print('training')
dovl.train(quiver)
