__description__ = "a module that tests DataLoader"
__author__ = "Patrick Godwin (patrick.godwin@ligo.org)"
__doc__ = "\n\n".join([__description__, __author__])

#--------------------------------------------------------------------------

import sys
import pytest

import numpy as np

from ligo import segments as ligo_segments

from idq import utils
from idq.io import triggers

#--------------------------------------------------------------------------

#----------------------
#        tests
#----------------------

@pytest.mark.usefixtures('dataloader')
class TestDataLoader(object):
    """
    Tests several aspects of a base DataLoader to check basic functionality.
    """
    def test_segments(self, dataloader_conf):
        test_seg = ligo_segments.segmentlist([ligo_segments.segment(dataloader_conf['start_time'], dataloader_conf['end_time'])])
        assert self.dataloader.segs == test_seg, 'incorrect segments returned'

    def test_pop(self, dataloader_conf):
        assert self.dataloader.pop('test_channel') is None, 'pop returned something when it should not have'

    def test_add(self, dataloader_conf):
        other_start_time = dataloader_conf['end_time']
        other_end_time = other_start_time + 1000
        other_dataloader = triggers.DataLoader(other_start_time, other_end_time)

        self.dataloader + other_dataloader
