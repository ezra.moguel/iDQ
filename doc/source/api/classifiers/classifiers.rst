.. idq-classifiers:

idq.classifiers
####################################################################################################

We support several types of classifiers within this object oriented architecture.
In particular, the :class:`idq.classifiers.SupervisedClassifier` and :class:`idq.classifiers.IncrementalSupervisedClassifier` objects declare the API for how classifiers intereact with the outside world.

This specification will be followed within :doc:`../../executables/idq-timeseries`, :doc:`../../executables/idq-train`, :doc:`../../executables/idq-evaluate`, and :doc:`../../executables/idq-calibrate`, meaning that new algorithms can be stood up quickly by following this API. 
Note, actual inheritence from the parent classes is recommended, but may not be required.

.. contents::

.. _classifiers-class_architecture:

Class Architecture
====================================================================================================

We support 2 types of supervised classification schemes, each encapsulated in a single object

* :class:`idq.classifiers.SupervisedClassifier` and
* :class:`idq.classifiers.IncrementalSupervisedClassifier`.

These objects are conceptually similar and generally follow the same API with the single exception in how they (re)train their internal models.
`idq.classifiers.SupervisedClassifier` and its children train through a *batch* prescription; that is they re-train by starting from scratch and analyzing a large batch of data.
This means that if any historical information is to be retained through the re-trainig process, that data must be included in the set passed to the :func:`idq.classifiers.SupervisedClassifier.train` call.

In contrast, :class:`idq.classifiers.IncrementalSupervisedClassifier` and its children train incrementally. 
This means that the data passed through the call to :func:`idq.classifiers.IncrementalSupervisedClassifier.train` is *added* to the previously used data in some sense. 
The incremental scheme should be computationally lighter, particularly when we retrain continuously, and better matches the streaming nature of the overall architecture.

We note that :class:`idq.classifiers.IncrementalSupervisedClassifier` is a subclass of :class:`idq.classifiers.SupervisedClassifier` and therefore re-uses a lot of the code define therein.
This also means the API is specified within :class:`idq.classifiers.SupervisedClassifier`, with a few exceptions (see :class:`idq.classifiers.OVL` for an example).

.. _classifiers-attributes_and_method_inheritance:

Attribute and Method Inheritance
----------------------------------------------------------------------------------------------------

Describe the following attributes and the rationale for their inheritance behavior

* __flavor
* _required_kwargs
* _model
* _calibration_map
* kwargs

Describe the following methods and their inheritance logic

* get_flavor
* get_model
* set_model
* get_calibration_map
* set_calibration_map
* calibrate
* train
* evaluate
* timeseries
* featureImportance

Note: this logic is also described within the :class:`idq.classifiers.SupervisedClassifier` docstring.

.. _classifiers-exceptions:

Exceptions
----------------------------------------------------------------------------------------------------

We define :class:`idq.classifiers.UntrainedError` and :class:`idq.classifiers.UncalibratedError` to handle use cases where :class:`idq.classifiers.SupervisedClassifier` objects have not been trained or calibrated, respectively.
These are raised if you attempt to do something that requires the object to have been trained or calibrated.
Hopefully, this will make the Traceback more informative.

.. _classifiers-docstrings:

API Reference
====================================================================================================

.. toctree::
    :maxdepth: 2

    base
    ovl
    sklearn
    keras
    xgboost
