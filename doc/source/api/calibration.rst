.. _calibration:

idq.calibration
####################################################################################################

describe the point of this module (estimating pdfs robustly).
this is useful within calibration (required by all `idq.clasifiers.SupervisedClassifier` and `idq.classifiers.IncrementalSupervisedClassifier` subclasses) and can be re-used within `idq.classifiers.NaiveBayes` and `idq.classifiers.IncrementalNaiveBayes`.

describe why we declare a class to handle calibration mapping (`idq.classifiers.SupervisedClassifier` instances retain a pointer to this object and delegate when they want to generate calibrated probabilities from ranks).

.. _calibration-docstrings:

.. automodule:: idq.calibration
    :members:
