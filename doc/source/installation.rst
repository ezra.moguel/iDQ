.. _installation:

Installation
#############

There are various ways to get started with iDQ:

  * :ref:`Install the latest release <install-release>`.
  * :ref:`Use a version provided in an IGWN reference distribution <install-igwn>`. This option is available to members of the International Gravitational-Wave Obervatory Network (IGWN). 
  * :ref:`Building the package from source <install-source>`. This is needed for users who wish to contribute to the project.

.. _install-release:

Installing the latest release
==============================

Conda installation
"""""""""""""""""""

Install conda using the `miniconda <https://docs.conda.io/projects/conda/en/latest/user-guide/install/>`_ installer, then run:

.. code:: bash

    $ conda install -c conda-forge idq

In order to check your installation, you can use:

.. code:: bash

    $ conda list idq  # to check which version is installed
    $ idq-batch --help

.. _install-igwn:

IGWN distributions of iDQ
==========================

If you are an IGWN member and have access to shared computing resources, up-to-date iDQ libraries are available on the IGWN Conda distribution, maintained by the IGWN Computing and Software Working Group. For more information, see `computing.docs.ligo.org/conda/ <https://computing.docs.ligo.org/conda/>`_.

.. _install-source:

Building from source
=====================

Building from source is required for development (bug fixes, new features, documentation improvements). You can check out the latest source of iDQ from git:

.. code:: bash

    git clone https://git.ligo.org/lscsoft/iDQ.git
    cd iDQ

It is recommended to install iDQ in a conda environment as certain dependencies are not available on PyPI. A development conda environment is provided within iDQ as well. Given you have conda installed, you can install iDQ from source (assuming you have checked out the latest source above and are in the project root) via:

.. code:: bash

    conda env create -n idq-dev -f environment.yml
    conda activate idq-dev
    pip install .
