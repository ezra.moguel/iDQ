.. _idq-report:

idq-report
####################################################################################################

describe script, which modules it relies upon, etc. This script should be used for reports to GraceDb as well as general summary pages (those should be the same thing moving forward...)

.. program-output:: idq-report --help
   :nostderr:
