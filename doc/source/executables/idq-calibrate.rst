.. _idq-calibrate:

idq-calibrate
####################################################################################################

describe script, which modules it relies upon, etc

.. graphviz::

   digraph idq_calibrate {
      labeljust = "r";
      label=""
      rankdir=LR;
      graph [fontname="Roman", fontsize=24];
      edge [ fontname="Roman", fontsize=10 ];
      node [fontname="Roman", shape=box, fontsize=11];
      style=rounded;
      labeljust = "r";
      fontsize = 14;


      Calibrate [label="idq-calibrate"];
      CalibMap [label="calibration map"];
      Quiver [label="quiver"];

      Quiver -> Calibrate;
      Calibrate -> CalibMap;

   }

.. program-output:: idq-calibrate --help
   :nostderr:
