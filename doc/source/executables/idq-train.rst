.. _idq-train:

idq-train
####################################################################################################

describe script, which modules it relies upon, etc

.. graphviz::

   digraph idq_train {
      labeljust = "r";
      label=""
      rankdir=LR;
      graph [fontname="Roman", fontsize=24];
      edge [ fontname="Roman", fontsize=10 ];
      node [fontname="Roman", shape=box, fontsize=11];
      style=rounded;
      labeljust = "r";
      fontsize = 14;


      DataSrc [label="auxiliary features"];
      Train [label="idq-train"];
      Model [label="model"]

      DataSrc -> Train;
      Train -> Model;

   }

.. program-output:: idq-train --help
   :nostderr:
