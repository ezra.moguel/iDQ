.. _reporters-workflow:

Reporters
####################################################################################################

``Reporters`` are a way for iDQ to record a variety of data products in a consistent fashion so that
various asynchronous jobs can discover and access them easily. These include models, quivers,
calibration maps, and timeseries that contain data products like p(glitch), log-likelihood, etc.

Each ``Reporter`` provides access to data by instantiating one with a location where data can be read from,
and the data range in which to access data, along with protocol-specific keyword arguments. Some reporters
are specific to the type of data they store and retrieve, i.e. ``GPSTimesReporter``. Others are
fairly generic and can be used for a variety of data products, i.e. ``PickleReporter``.

.. _io-file_api:

API
----------------------------------------------------------------------------------------------------

.. autoclass:: idq.io.reporters.Reporter
       :inherited-members:

Generic
----------------------------------------------------------------------------------------------------

.. autoclass:: idq.io.reporters.DiskReporter

.. autoclass:: idq.io.reporters.hdf5.HDF5Reporter

.. autoclass:: idq.io.reporters.pkl.PickleReporter

.. _io-file_model:

Models
----------------------------------------------------------------------------------------------------

.. autoclass:: idq.io.reporters.pkl.PickleReporter

.. _io-file_dataset:

Datasets
----------------------------------------------------------------------------------------------------

.. autoclass:: idq.io.reporters.hdf5.DatasetReporter

.. _io-file_calibmap:

Calibration Maps
----------------------------------------------------------------------------------------------------

.. autoclass:: idq.io.reporters.hdf5.CalibrationMapReporter

.. _io-file_seglist:

GPS Times
----------------------------------------------------------------------------------------------------

.. autoclass:: idq.io.reporters.hdf5.GPSTimesReporter

Segment Lists
----------------------------------------------------------------------------------------------------

.. autoclass:: idq.io.reporters.ligolw.LIGOLWSegmentReporter

.. autoclass:: idq.io.reporters.hdf5.HDF5SegmentReporter

.. _io-file_series:

Timeseries
----------------------------------------------------------------------------------------------------

.. autoclass:: idq.io.reporters.gwf.GWFSeriesReporter

.. autoclass:: idq.io.reporters.hdf5.HDF5SeriesReporter

.. _io-stream_reporter:

Stream-based
----------------------------------------------------------------------------------------------------

.. autoclass:: idq.io.reporters.kafka.KafkaReporter
