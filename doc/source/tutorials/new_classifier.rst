.. _new-classifier:

Defining a New Classifier
####################################################################################################

This tutorial will guide you through the process of defining a new classifier, which we'll call ``SimpleClassifier``.
It provides step-by-step instructions for iDQ's inheritence schemes and what developers will need to define for each new classifier.
The classifier declared in this tutorial is an incredibly simple model and is not expected to perform well on actual data.
Instead, it is meant to pedagogically demonstrate how one could construct a classification scheme based on auxiliary features to classify target times.

Our ``SimpleClassifier`` will classify data by optimizing a threshold for a single auxiliary channel parameters (specified as ``feature_name``.
We will define a very simple ``model`` for the ``SimpleClassifier``; more complex models can also be declared or imported as developers need them.
A simple example might be importing existing machine learning algorithms from an external package like ``sk_learn``.
We won't demonstrate that in this tutorial, but it should be obvious how one could do just that by the end.

.. contents::

.. _new_classifier-goals:

Learning Goals
====================================================================================================

By the time you have completed this tutorial, you will be able to

* describe the basic tasks each classifier must be able to perform

  * describe the purpose of the `train` method
  * describe the purpose of the `calibrate` method
  * describe the purpose of the `evaluate` method
  * describe the purpose of the `timeseries` method
  * describe the purpose of the `featureImportance` method

* enumerate the basic API for iDQ's classifier objects

  * describe the input/output signature of `train` methods
  * describe the input/output signature of `calibrate` methods
  * describe the input/output signature of `evaluate` methods
  * describe the input/output signature of `timeseries` methods
  * describe the input/output signature of `featureImportance` methods

* describe the conceptual difference between an instance of :class:`idq.classifiers.SupervisedClassifier` and an instance of :class:`idq.classifiers.IncrementalSupervisedClassifier`
* set up an INI config file to use your new classifier

.. _new_classifier-step1:

Step 1: fork the iDQ source code and clone a copy
====================================================================================================

The first thing you'll need to do is to fork a copy of the iDQ source code, following the fork-and-pull development model.

* go to https://git.ligo.org/reed.essick/iDQ.
* click *fork*.
* click on the namespace to which you want to fork the repo. This will almost certainly be your *albert.einstein* username.
* clone your new fork of the repo into a working directory with something like the following, replacing the namespace as needed.

:: 

    git clone https://git.ligo.org/albert.einstein/iDQ ${repo}
    cd ${repo}

.. _new_classifier-step2:

Step 2 : declare your new object
====================================================================================================

All classifiers are represented as objects within iDQ and they must all be delcared within :class:`idq.classifiers`.
The basic classifier object is called :class:`idq.classifiers.SupervisedClassifier` and this declaration sets the API expected of all classifiers.
We'll get to that in shortly (:ref:`new_classifier-step4`).
iDQ also supports another generic set of classifiers, :class:`idq.classifiers.IncrementalSupervisedClassifier`, which inherit from :class:`idq.classifiers.SuprevisedClassifier`.
The difference between these two *parent classes* is encapsulated in how they train themselves.

  * :class:`idq.classifiers.SupervisedClassifier` subclasses retrain themselves *from scratch* in a *batch* mode. This means they only ever look at the data that is passed through the ``train`` call (:ref:`new_classifier-step4.1`). Each call to ``train`` will completely overwrite any existing ``model`` within the classifier.
  * :class:`idq.classifiers.IncrementalSupervisedClassifier` subclasses retrain themselves using the data passed through the call to ``train`` along with their exisitng model. They use the additional model to *update* their existing models, rather than overwriting them completely and starting from scratch.

Some algorithms are better suited to batch training and some are better suited to incremental training.
This tutorial will implement a subclass of :class:`idq.classifiers.SupervisedClassifier` and therefore an algorithm which re-trains from scratch in a batch mode.

iDQ also defines automated look-up of any classifier defined within :class:`idq.classifiers`. 
Therefore, it is critical that you declare your new classifier above the comment saying::

    # ALL CLASSIFIERS SHOULD BE DECLARED ABOVE THE FOLLOWING LOOP

Classifiers declared after that loop may not be recognized during automatic look-up.

Now, it's time to declare our new classifier, which will inherit from :class:`idq.classifiers.SupervisedClassifier`.
Open ``idq/classifiers.py`` with your favorite text editor, find a spot near the bottom of the file, and declare a new class::

    class SimpleClassifier(SupervisedClassifier):
        """
        a simple classifier constructed during a tutorial
        """
        __flavor = 'simple_classifier'

Strictly speaking, that's all you need to do.
``SimpleClassifier`` will inherit everything else from :class:`idq.classifiers.SupervisedClassifier` and be used in production.
You will note that we declared the ``__flavor`` attribute along with the class.
This is used for look-up and some error reporting and therefore it is crucial that each class declares its own value for ``__flavor``.
Note, if you forgot to declare this attribute within ``SimpleClassifier``, you will likely see errors like ``AttributeError: SupervisedClassifier has no instance _SimpleClassifier__flavor``.

You may also want to declare a few other attributes, although these can be inherited from :class:`idq.classifiers.SupervisedClassifier` without issue.

* ``_required_kwargs``
    * this tells iDQ which kwargs are required when instantiating your ``SimpleClassifier``. The default within :class:`idq.classifiers.SupervisedClassifier` is an empty list (no kwargs required), but you can add them in as needed. Specifically, ``_required_kwargs`` specifies what must be present in you INI file when you run the pipeline (:ref:`new_classifier-step6`).
* ``_trains_with_quiver``
    * this is used to identify special cases that require special signatures when calling ``train``. You should not have to worry about this. ``SimpleClassifier`` should inherit the default value from :class:`idq.classifiers.SupervisedClassifier` (``True``) unless you absolutely, completely, and utterly know what you're doing.

For demonstration purposes, let's make our ``SimpleClassifier`` require a single kwarg corresponding to an auxiliary channel name. 
Change your declaration to overwrite the parent's attribute as follows::

    class SimpleClassifier(SupervisedClassifier):
        """
        a simple classifier constructed during a tutorial
        """
        __flavor = 'simple_classifier'
        _required_kwargs = ['feature_name']

Now, before we move on, we should declare place-holder method declarations for the key methods all classifiers must support.
We'll fill these in later (:ref:`new_classifier-step4`), but for now these are meant to warn us that we have not actually implemented this functionality instead of falling back to :class:`idq.classifiers.SupervisedClassifier`'s methods silently.
Add the following place-holders so your class declaration looks like::

    class SimpleClassifier(SupervisedClassifier):
        """
        a simple classifier constructed during a tutorial
        """
        __flavor = 'simple_classifier'
        _required_kwargs = ['feature_name']

        def train(self, quiver):
            """
            train the :class:`idq.classifiers.SimpleClassifier` with a :class:`idq.featuers.Quiver` of :class:`idq.features.FeatureVectors`s
            """
            return NotImplementedError

        def evaluate(self, quiver):
            """
            evaluate the :class:`idq.features.FeatureVectors`s in a :class:`idq.featuers.Quiver` using this :class:`idq.classifiers.SimpleClassifier`
            """
            return NotImplementedError

        def timeseries(self, classifier_data):
            """
            generate a timeseries of predictions based on :class:`idq.io.ClassifierData` and this :class:`idq.classifiers.SimpleClassifier`
            """
            return NotImplementedError

        def calibrate(self, quiver):
            """
            calibrate the predictions of this :class:`idq.classifiers.SimpleClassifier` using a :class:`idq.featuers.Quiver` of :class:`idq.features.FeatureVectors`s
            """
            raise NotImplementedError

        def featureImporance(self):
            """
            report which features this :class:`idq.classifiers.SimpleClassifier` thinks are important
            """
            return NotImplementedError

You should note that we've declared helpstrings throughout with somewhat funny notation.
That's so our documenation can automatically link together different pieces of code when you look at the full doc-strings for :class:`idq.classifiers`.

.. _new_classifier-step3:

Step 3 : implement your object's model
====================================================================================================

Your classifier's model must be an object, but that's not hard in Python where base types are objects.
Specifically, your object must be serializable with Python's ``pickle`` module, but that is also not hard to do.
We will use a Python ``tuple`` for our model as an example, but you can of course declare a specific class as your model or import one from another library.

Because ``SimpleClassifier``'s model is so simple (just a ``tuple``), we do not declare a separate class.
Specifically, our model records a threshold and whether we should call samples with features above that threshold ``label=1`` or events below that threshold ``label=1``.

The model will be instantiated within the call to ``train`` (see :ref:`new_classifier-step4.1`).
This is common to all subclasses of :class:`idq.classifiers.SupervisedClassifier`.

.. _new_classifier-step4:

Step 4 : implement your object's key methods
====================================================================================================

Now, we return to the placeholder methods we declared in :ref:`new_classifier-step2`. 
We'll address each of thes in turn, explaining what it does and then implementing ``SimpleClassifier``'s guts.

.. _new_classifier-step4.1:

Step 4.1 : implement SimpleClassifier.train
----------------------------------------------------------------------------------------------------

``train``'s signature is just an instance of :class:`idq.features.Quiver`.
This abstraction automatically handles agglomeration of features into array structures necessary for training.
We note that setting ``_trains_with_quiver=False`` will change the signature used when calling ``train`` but that is beyond the scope of this tutorial.
Instead, ``SimpleClassifier`` will use the standard signature.

Furthermore, because ``SimpleClassifier`` inherits directly from :class:`idq.classifiers.SupervisedClassifier`, it will train in a batch mode.
This means we need to instantiate a new model (for ``SimpleClassifier`` that means a ``tuple``) and then set the parameters of that model in order to optimize its performance based on ``quiver``.

So, let's build up our method.
The first thing we'll need to do is extract the relevant data from ``quiver``::

        def train(self, quiver):
            """
            train the :class:`idq.classifiers.SimpleClassifier` with a :class:`idq.featuers.Quiver` of :class:`idq.features.FeatureVectors`s
            """
            feature_name = self.kwargs['feature_name']
            vectors = quiver.vectorize('label', feature_name) ### extract an array representing training data

We delegate to :class:`idq.features.Quiver.vectorize` to extract the relevant data and format it into a structured array.
Note that we can assume ``self.kwargs`` has the ``feature_name`` key because we specified it in ``_required_kwargs``. 
In general, one can use :class:`idq.features.Quiver.vectorize` to generate much larger structured arrays with many columns; ``SimpleClassifier`` just uses a simple version because it is simple.

At this point, you can define whatever algorithm you want.
This could be delegating to ``sk_learn``'s objects' ``fit`` method, or it could involve home-grown code.
For ``SimpleClassifier``, we find the single threshold that best separates the data into the correct groupings (defined by ``label``).
We also assume binary labels at the moment, either 1.0 or 0.0, and choose a somewhat ad hoc *cost function*. 
This tutorial supplies a home-grown training algorithm for you based on a very simple decision tree with a single node.
See if you can follow along withthe logic, which attempts to maximize the purity of the classified training samples by adjusting the threshold.

::

            ### set up counters for iteration
            c = 0.
            N = len(vectors)
            c1 = 0.
            N1 = 1.*np.sum(vectors['label']==1)
            c0 = 0.
            N0 = N-N1

            ### set up reference variable to figure out which threshold is best
            ###   a good max_purity is close to 2 and implies we should call everyting below max_purity_thr label=1
            ###   a good min_purity is close to 0 and implies we should call everything above min_purity_thr label=1
            max_purity = -np.infty       
            max_purity_thr = -np.infty

            min_purity = np.infty
            min_purity_thr = -np.infty

            ### iterate over sorted array (smallest feature value to largest)
            ### figure out which threshold correspond to the max and min purities
            for vector in vectors[vectors[feature_name].argsort()][:-1]: ### stop at the penultimate item to avoid divergent purity
                label = vect['label']
                value = vect[feature_name]
    
                c1 += label
                c0 += 1.-label
                c += 1
    
                purity = c1/c + (N0-c0)/(N-c) ### our ad hoc cost function
                                              ### this is the sum of the purity fraction of each grouping

                if purity > max_purity:
                    max_purity = purity
                    max_purity_thr = value
    
                if purity < min_purity:
                    min_purity = purity
                    min_purity_thr = value

            ### figure out which possible classification scheme does best: "greater_than" or "less_than"
            if (2-max_purity) > (min_purity):        ### we do better over-all if we call everything below max_purity_thr label=1
                model = ('less_than', max_purity_thr) ### define the model as a tuple
            else:
                model = ('greather_than', min_purity_thr)

            ### set the model internally
            self.set_model(model)

Now, we have an algorithm to ``train`` our basic model based on data, and the last thing we're required to do is return a reference to ``self``. 
This is done to facilitate *daisy-chaining* function calls should the user desire and is specified within :class:`idq.classifiers.SupervisedClassifier.train`.

Therefore, our entire ``train`` method should read

::

        def train(self, quiver):
            """
            train the :class:`idq.classifiers.SimpleClassifier` with a :class:`idq.featuers.Quiver` of :class:`idq.features.FeatureVectors`s
            """
            feature_name = self.kwargs['feature_name']
            vectors = quiver.vectorize('label', feature_name) ### extract an array representing training data

            ### set up counters for iteration
            c = 0.
            N = len(vectors)
            c1 = 0.
            N1 = 1.*np.sum(vectors['label']==1)
            c0 = 0.
            N0 = N-N1

            ### set up reference variable to figure out which threshold is best
            ###   a good max_purity is close to 2 and implies we should call everyting below max_purity_thr label=1
            ###   a good min_purity is close to 0 and implies we should call everything above min_purity_thr label=1
            max_purity = -np.infty       
            max_purity_thr = -np.infty

            min_purity = np.infty
            min_purity_thr = -np.infty

            ### iterate over sorted array (smallest feature value to largest)
            ### figure out which threshold correspond to the max and min purities
            for vector in vectors[vectors[feature_name].argsort()][:-1]: ### stop at the penultimate item to avoid divergent purity
                label = vect['label']
                value = vect[feature_name]

                c1 += label
                c0 += 1.-label
                c += 1

                purity = c1/c + (N0-c0)/(N-c) ### our ad hoc cost function
                                              ### this is the sum of the purity fraction of each grouping

                if purity > max_purity:
                    max_purity = purity
                    max_purity_thr = value

                if purity < min_purity:
                    min_purity = purity
                    min_purity_thr = value

            ### figure out which possible classification scheme does best: "greater_than" or "less_than"
            if (2-max_purity) > (min_purity):        ### we do better over-all if we call everything below max_purity_thr label=1
                model = (True, max_purity_thr) ### define the model as a tuple
            else:
                model = (False, min_purity_thr)

            ### set the model internally
            self.set_model(model)

            return self ### required by API defined within :class:`idq.classifiers.SupervisedClassifier`


.. _new_classifier-step4.2:

Step 4.2 : implement SimpleClassifier.evaluate
----------------------------------------------------------------------------------------------------

The ``evaluate`` method exists to apply the ``model`` to new observations.
For this reason, it takes in a :class:`idq.features.Quiver`, iterates over that quiver, and applies the ``model`` to determine how to classify that sample. 
It returns a reference to the same :class:`idq.features.Quiver` it received after modifying its elements in place.

We note that ``SimpleClassifier`` make binary predictions (``rank=0`` or ``rank=1``). 
In general, classifiers can assign any ``rank`` within ``0<=rank<=1``.

We implement a very simple iteration for this in ``SimpleClassifier``.
Can you think of a faster way to accomplish the same thing?

::

        def evaluate(self, quiver):
            """
            evaluate the :class:`idq.features.FeatureVectors`s in a :class:`idq.featuers.Quiver` using this :class:`idq.classifiers.SimpleClassifier`
            """
            feature_name = self.kwargs['feature_name']
            
            ### extract the stored model
            flag, thr = self.get_model()

            ### iterate over each :class:`idq.features.FeatureVector` within quiver and set its rank
            for featurevector in quiver: 
                value = featurevector.vectorize(feature_name)

                if flag: ### call value label=1 if value<=thr
                    featurevector.set_rank(int(value<=thr))

                else: call value label=1 if value>=thr
                    featurevector.set_rank(int(value>=thr))

            return quiver ### required syntatically by API defined in :class:`idq.classifeirs.SupervisedClassifier`

.. _new_classifier-step4.3:

Step 4.3 : implement SimpleClassifier.timeseries
----------------------------------------------------------------------------------------------------

``timeseries`` provides a simplified interface to generate time-series representations of predictions.
This is useful for streaming processes, which are designed to generate regularly sampled times-series data.
The ``timeseries`` method accepts an instance of :class:`idq.io.ClassifierData` and a standard sample spacing in seconds (the inverse of the sampling rate).
From these two arguments, it should extract the data needed needed to make predictions for all times within the segments returned by :class:`idq.io.ClassifierData.get_segments`.
It returns a list of ``numpy.ndarray`` objects representing the rank time-series within each segment.

We will implement an extremely simple version of this which delegates to ``SimpleClassifier.evaluate`` to do its heavy lifting.
However, certain algorithms may support faster solutions, and therefore delegation to ``evaluate`` is not strictly necessary.

::

        def timeseries(self, classifier_data, dt=default_dt):
            """
            generate a timeseries of predictions based on :class:`idq.io.ClassifierData` and this :class:`idq.classifiers.SimpleClassifier`
            """
            ### a list that will hold our data products (np.ndarrays)
            ranks = []

            ### iterate over all segments within classifier_data
            for start, end in classifier_data.get_segments():
                quiver = features.Quiver() ### define this quiver for delegation to self.evaluate

                ### iterate over time sampling, instantiating a FeatureVector for each sample and adding it to quiver
                for gps in np.arange(start, end, dt) ### define time sampling
                    quiver.append( features.FeatureVector(gps, None, classifier_data) )

                ### delegate to self.evaluate to evaluate every FeatureVector in quiver
                self.evaluate(quiver) ### modifies quiver in place

                ### construct a numpy.ndarray of the ranks assigned to the FeatureVectors within quiver, and append to ranks
                ranks.append( np.array([featurevector.get_rank() for featurevector in quiver]) )

            return ranks ### return the result

.. _new_classifier-step4.4:

Step 4.4 : (optional) implement SimpleClassifier.calibrate
----------------------------------------------------------------------------------------------------

New classifiers will almost certainly be able to inherit this functionality from :class:`idq.classifiers.SupervisedCalssifier`.
However, :class:`idq.classifiers.SupervisedCalssifier.calibrate` requires all :class:`idq.features.FeatureVector` within the instance of :class:`idq.features.Quiver` to have been evaluated; it will raise an exception if that is not the case.
We extend our ``SimpleClassifier`` so that, if any :class:`idq.features.FeatureVector` has not been evaluated, it evaluates that vector instead of raising the error. 
It then delegates to the parent class.

::

        def calibrate(self, quiver):
            """
            calibrate the predictions of this :class:`idq.classifiers.SimpleClassifier` using a :class:`idq.featuers.Quiver` of :class:`idq.features.FeatureVectors`s
            """
            for vector in quiver:
                if not vector.is_evaluated():
                    self.evaluate(features.Quiver(vector))
            super(SimpleClassifier, self).calibrate(quiver) ### returns self

.. _new_classifier-step4.5:

Step 4.5 : (optional) implement SimpleClassifier.featureImportance
----------------------------------------------------------------------------------------------------

Children of :class:`idq.classifiers.SupervisedClassifier` should possess a concept of which features of their model are important.
However, because the models of different children can be radically different, it is difficult to specify a single format for that information.
Instead, each child defines its own format and exposes it to the user via the :class:`idq.classifiers.SupervisedClassifier.featureImportance` method.

For our ``SimpleClassifier``, we have already specified which feature will be used when instantiating the object.
The key pieces of information contained in the model are the threshold and whether we call things above or below the threshold ``label=1``.
Therefore, we define a very simple return format: a string summarizing that information.

Note, we also check whether the ``SimpleClassifier`` has been trained and raise an :class:`idq.classifiers.UntrainedError` if it has not.

::

        def featureImporance(self):
            """
            report which features this :class:`idq.classifiers.SimpleClassifier` thinks are important
            """
            if self.get_model()==None:
                raise UntrainedError

            less_than, thr = self.get_model()
            if less_than: 
                return "samples with %s <= %.3f are assigned rank=1"%(self.kwargs['feature_name'], thr)
            else:
                return "samples with %s >= %.3f are assigned rank=1"%(self.kwargs['feature_name'], thr)


.. _new_classifier-step5:

Step 5 : installing the source code
====================================================================================================

You should make sure your code is bug-free and test it.
The first step of this is installing the code.
You can do that with the standard Python setup tools.
Remember to update your environment so your installed libraries are discoverable!

::

    python setup.py install --prefix=/path/to/opt

.. _new_classifier-step6:

Step 6 : set up your INI file
====================================================================================================

Now that you've declared your new classifier and installed the code, you're ready to set up your pipeline.
Copy the example config file from ``${repo}/etc/idq.ini`` to your working directory.

When running the pipeline, you will need to change the following basic options

    | [general]
    | rootdir = /path/to/your/working/directory

You may also need to modify some data discovery options depending on which input data you'd like to use and where you're running.

To use your new classifier, you need to add a section that looks like

    | [whateverNameYouWant]
    | flavor = SimpleClassifier
    | feature_name = L1_OMC-DCPD_NULL_OUT_DQ_32_2048_significance

and make sure your ``[general]`` section lists ``newClassifier`` under the ``classifiers`` option

    | [general]
    | classifiers = whateverNameYouWant

Within the INI file, classifiers are referenced by their ``nickname``. 
This is declared as the ``[section]`` name for the classifier, and you must tell the pipeline to run the classifier specified by that section in the ``[general]`` section.
Note, you can run using as many versions of your classifier as you'd like.
Just make sure you give them all separate ``nicknames`` and list them all in the ``[general]`` section.

    | [general]
    | classifiers = significance frequency dt
    |
    | [significance]
    | flavor = SimpleClassifier
    | feature_name = L1_OMC-DCPD_NULL_OUT_DQ_32_2048_significance
    |
    | [frequency]
    | flavor = SimpleClassifier
    | feature_name = L1_OMC-DCPD_NULL_OUT_DQ_32_2048_frequency
    |
    | [dt]
    | flavor = SimpleClassifier
    | feature_name = L1_OMC-DCPD_NULL_OUT_DQ_32_2048_dt

.. _new_classifier-step7:

Step 7 : run the pipeline with your new classifier
====================================================================================================

Reference :doc:`running_batch_pipeline` for more detail, but you should be able to run a one-off training job via::

    idq-train /path/to/your/config.ini ${gps_start} ${gps_end}

This will train your classifier using data bounded by ``${gps_start}`` and ``${gps_end}``.

.. _new_classifier-step8:

Step 8 : merge your new classifier into the production code
====================================================================================================

**Note**: merge requests produced by completing this tutorial are not likely to be accepted. 
However, if you'd like to follow through, your request will be reviewed and you can iterate with your reviewer if you have any questions.

When you are confident that your new classifier is ready for deployment (i.e.: after you've thoroughly tested it), you should open a *merge request*.
Go to your fork on git.ligo.org and click on ``Merge Requests`` on the left-hand panel.
Then click ``New merge request``.
Select the branch you'd like to merge and the branch into which you'd like to merge it, then click ``Compare branches and continue``.
At this point, please provide a descriptive title and detailed description of your changes.
This should include a discussion of 

* what you changed,
* how you tested it, and
* why this should be merged.

Although not necessary, you can assign the request to a specific person.
Submit the merge request and be sure to follow up to make sure it is processed and accepted.
