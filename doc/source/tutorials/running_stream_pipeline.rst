.. _running-stream:

Running the Streaming Pipeline
####################################################################################################

This is the *how to* guide for new folks who want to run their own version of the pipeline.
The syntax for `iDQ`'s executables are fairly standard (and fairly limited), and so much of the work setting up `iDQ` lies with correctly specifying the config file.
We address both points in this tutorial.

While we note what would be needed to configure `iDQ` for several different sources of features (triggers), we only provide a full example for synthetic trigger streams (:class:`idq.io.MockClassifierData`).

.. _running-stream_config:

Configuration file
====================================================================================================

A complete description of `iDQ`'s configuration (INI) files can be found here: :ref:`configuration`.
However, we provide enough information below to get you started.

The INI file has several required sections, which we'll introduce in turn.
An example is provided alongside the source code (`~etc/idq.ini`), but we repeat a simplified version below.

In addition to the INI file, analysts must manage the list of channels to be used in the analysis.
These are typically determined via safety studies (i.e.: hardware injections), but this simplified INI uses synthetic data generated on the fly.
As such, you will also have to manage a config file for your synthetic data in addition to the channel list.

**idq.ini**

::

    #-------------------------------------------------
    # high-level shared parameters
    [general]
    tag = test
    instrument = Fake1
    rootdir = .

    classifiers = ovl

    [samples]
    target_channel = target_channel
    target_bounds = 

    dirty_bounds =
    dirty_window = 0.

    #-------------------------------------------------
    # parameters for training jobs
    [train]
    workflow = block 
    log_level = 10
    random_rate = 0.1

    [train data discovery]
    flavor = MockClassifierData
    time = time
    ignore_segdb = False

    columns = ['time', 'snr', 'frequency']

    config = 

    [train stream]
    stride =
    delay = 

    [train reporting]
    flavor = PickleReporter

    #-------------------------------------------------
    # parameters for evaluation jobs
    [evaluate]
    workflow = 
    log_level =
    random_rate = 

    [evaluate data discovery]
    flavor = MockClassifierData
    time = time
    ignore_segdb = False

    columns = ['time', 'snr', 'frequency']

    config = 

    [evaluate stream]
    stride =
    delay = 

    [evaluate reporting]
    flavor = QuiverReporter

    #-------------------------------------------------
    # parameters for calibration jobs
    [calibrate]
    workflow = block
    log_level = 10

    [calibrate reporting]
    flavor = CalibrationMapReporter

    #-------------------------------------------------
    # parameters for timeseries jobs
    [timeseries]
    workflow = block
    log_level = 10
    srate = 128

    [timeseries data discovery]
    flavor = MockClassifierData
    time = time
    ignore_segdb = False

    columns = ['time', 'snr', 'frequency']

    config = 

    [timeseries stream]
    stride =
    delay =

    [timeseries reporting]
    flavor = GWFSeriesReporter

    #-------------------------------------------------
    # parameters for classifiers
    [ovl]
    flavor = OVL

    incremental = 100
    num_recalculate = 10
    metric = eff_fap
    minima = {'eff_fap': 3, 'poisson_signif':5, 'use_percentage':1e-3}

    time = time
    significance = significance

**mcd.ini**

::

    WRITE AN EXAMPLE HERE WITH VERY FEW CHANNELS

**channels.txt**

::

    WRITE this

.. _running-stream-tasks:

Streaming Tasks
====================================================================================================

In order to run the streaming pipeline, you will need to have an instance of Kafka
running in the background, which is needed for ``KafkaReporter`` to run
correctly. This can be done in one of the following two ways:

1. Connect via an already-running instance of Kafka, set up in an existing LDG cluster.

  * If this option is available, no further configuration is necessary and the default iDQ configuration
    file will contain everything needed to get up and running.

2. Run an instance of Kafka as a background process.

  * Running a separate instance requires Kafka to be installed, as well as its C and Python bindings,
    librdkafka and confluent-python-kafka, respectively. These dependencies can all be installed using
    the Makefile provided in /etc.

  * Sourcing the environment script that is built alongside the iDQ dependencies, one can start up background
    instances of Kafka and Zookeeper as follows, which takes in a path to their respective configuration files:

    a. zookeeper-server-start.sh zookeeper.properties
    b. kafka-server-start.sh kafka.properties

    Both instances take a few seconds to start up. Sample configuration files for both are provided within /etc.

TODO:

Describe how to manage (asynchronous) processes via ``idq-stream``.
Describe how that will manage

* ``idq-streaming_train``
* ``idq-streaming_calibrate``
* ``idq-streaming_evaluate``

Describe the input/output data streams for each.
