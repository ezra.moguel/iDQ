.. _running-batch:

Running the Batch Pipeline
####################################################################################################

This should be the *how to* guide for new folks who want to run their own version of the pipeline.

Write a description of what must be present for this all to work.

.. _running-batch_config:

Configuration file
====================================================================================================

In order to run one-off or batch tasks, you'll need to provide iDQ with a INI file. An example
configuration is located at ``${repo}/etc/idq.ini``. Below is a guide that will get you started with
common configuration options, an exhaustive list of options is located at :doc:`../configuration`.

**Common options that will be used throughout batch jobs:**

::

    [general]
    tag = name_of_run
    instrument = ifo
    rootdir = /path/to/your/working/directory

    classifiers = classifier1
                  classifier2
                  classifier3

**Options for defining glitch/clean samples:**

::

    [samples]
    target_channel = channel_name
    target_bounds =
        significance lower_bound upper_bound

    dirty_window = time_window
    dirty_bounds =
        significance lower_bound upper_bound

Glitches are determined by looking at a specified target channel and finding features that fall
within these target bounds.

Clean samples are determined by first finding all times that fall within dirty bounds and removing
all times within the dirty window specified. Whatever time is left will be sampled to generate clean
samples.

**Options for batch jobs:**

Possible job options are:

  1. train
  2. evaluate
  3. calibrate
  4. timeseries

::

    [job]
    log_level = number
    workflow = workflow_type

    random_rate = rate (train/evaluate only)
    min_stride = stride (train only)
    srate = rate (timeseries only)

    [job data discovery]
    flavor = classifier_data_type
    whatever kwargs are needed by this classifier_data

    [job reporting]
    flavor = reporter_type
    whatever kwargs are needed by this reporter

**Classifier options:**

Here, you'll be creating a section, one per classifier, with the keyword arguments needed for that
particular classifier. For example, for a support vector machine classifier:

::

    [svm]
    flavor = SupportVectorMachine

    safe_channels_path = channel_list.txt

    default = 0
    window = 0.1
    time = trigger_time
    significance = snr

    num_proc = 8
    param_grid = {'C': (1e-4, 1e3, 8),
                  'gamma': (1e-4, 1e3, 8)}

**Segment options:**

In addition, you'll need to provide a way for iDQ to query DQSegDB for valid segments.

::

    [segments]
    segdb_url = https://segments.ligo.org

    intersect = H1:DMT-ANALYSIS_READY:1
    exclude =

**Condor options:**

If you're planning on using condor workflows in any part of iDQ, you'll also have to specify options
for condor submission as well.

::

    [condor]
    universe = vanilla
    retry = 3

    accounting_group = your.accounting.group
    accounting_group_user = albert.einstein


After you've set up your configuration file, you're ready to launch one-off or batch iDQ tasks.

.. _running-batch_one_off:

One-off Tasks
====================================================================================================

* ``idq-train``:

.. program-output:: idq-train --help
      :nostderr:

.. graphviz::

   digraph idq_train {
      labeljust = "r";
      label=""
      rankdir=LR;
      graph [fontname="Roman", fontsize=24];
      edge [ fontname="Roman", fontsize=10 ];
      node [fontname="Roman", shape=box, fontsize=11];
      style=rounded;
      labeljust = "r";
      fontsize = 14;


      DataSrc [label="auxiliary features"];
      Train [label="idq-train"];
      Model [label="model"]

      DataSrc -> Train;
      Train -> Model;

   }

* ``idq-evaluate``:

.. program-output:: idq-evaluate --help
      :nostderr:

.. graphviz::

   digraph idq_evaluate {
      labeljust = "r";
      label=""
      rankdir=LR;
      graph [fontname="Roman", fontsize=24];
      edge [ fontname="Roman", fontsize=10 ];
      node [fontname="Roman", shape=box, fontsize=11];
      style=rounded;
      labeljust = "r";
      fontsize = 14;


      DataSrc [label="auxiliary features"];
      Model [label="model"]
      Evaluate [label="idq-evaluate"];
      Quiver [label="quiver"];

      DataSrc -> Evaluate;
      Model -> Evaluate;
      Evaluate -> Quiver;

   }

* ``idq-calibrate``:

.. program-output:: idq-calibrate --help
      :nostderr:

.. graphviz::

   digraph idq_calibrate {
      labeljust = "r";
      label=""
      rankdir=LR;
      graph [fontname="Roman", fontsize=24];
      edge [ fontname="Roman", fontsize=10 ];
      node [fontname="Roman", shape=box, fontsize=11];
      style=rounded;
      labeljust = "r";
      fontsize = 14;


      Calibrate [label="idq-calibrate"];
      CalibMap [label="calibration map"];
      Quiver [label="quiver"];

      Quiver -> Calibrate;
      Calibrate -> CalibMap;

   }

* ``idq-timeseries``

.. program-output:: idq-timeseries --help
      :nostderr:

.. graphviz::

   digraph idq_timeseries {
      labeljust = "r";
      label=""
      rankdir=LR;
      graph [fontname="Roman", fontsize=24];
      edge [ fontname="Roman", fontsize=10 ];
      node [fontname="Roman", shape=box, fontsize=11];
      style=rounded;
      labeljust = "r";
      fontsize = 14;


      DataSrc [label="auxiliary features"];
      Model [label="model"]
      CalibMap [label="calibration map"];
      Timeseries [label="idq-timeseries"];
      PGlitch [label="p(glitch) timeseries"];

      DataSrc -> Timeseries;
      Model -> Timeseries;
      CalibMap -> Timeseries;
      Timeseries -> PGlitch;

   }

.. _running-batch_stream:

Batch Tasks
====================================================================================================

* ``idq-batch``:

.. program-output:: idq-batch --help
      :nostderr:

.. graphviz::

   digraph idq_batch {
      labeljust = "r";
      label=""
      rankdir=LR;
      graph [fontname="Roman", fontsize=24];
      edge [ fontname="Roman", fontsize=10 ];
      node [fontname="Roman", shape=box, fontsize=11];
      style=rounded;
      labeljust = "r";
      fontsize = 14;


      DataSrc [label="auxiliary features"];

      Model [label="model"]
      Quiver [label="quiver"];
      CalibMap [label="calibration map"];

      Train [label="batch.train"];
      Evaluate [label="batch.evaluate"];
      Calibrate [label="batch.calibrate"];
      Timeseries [label="batch.timeseries"];

      PGlitch [label="p(glitch) timeseries"];

      DataSrc -> Train;
      Train -> Model;

      DataSrc -> Evaluate;
      Model -> Evaluate;
      Evaluate -> Quiver;

      Quiver -> Calibrate;
      Calibrate -> CalibMap;

      DataSrc -> Timeseries;
      Model -> Timeseries;
      CalibMap -> Timeseries;
      Timeseries -> PGlitch;

   }
