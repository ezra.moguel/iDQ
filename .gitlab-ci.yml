
# -- setup --------------------------------------

image: docker:latest

stages:
  - build
  - test
  - deploy
  - docs
  - publish

variables:
  DOCKER_DRIVER: overlay
  DOCKER_BRANCH: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME
  DOCKER_LATEST: $CI_REGISTRY_IMAGE:latest
  BRANCH: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME
  COMMIT: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
  NIGHTLY: $CI_REGISTRY_IMAGE:nightly
  TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
  GIT_STRATEGY: none

# -- stages -------------------------------------

.in-tmpdir: &in-tmpdir
  before_script:
    - WORKING_DIRECTORY="$(mktemp -d)"
    - cd "${WORKING_DIRECTORY}"
  after_script:
    - cd "${CI_PROJECT_DIR}"
    - rm -rf "${WORKING_DIRECTORY}"

# Build source distribution
sdist:
  image: python:slim
  stage: build
  variables:
    GIT_STRATEGY: fetch
  before_script:
    - apt-get -q update
  script:
    - python setup.py sdist
    - mv dist/* .
  artifacts:
    paths:
      - '*.tar.gz'

# Build binary distribution
bdist:
  image: python:slim
  stage: test
  <<: *in-tmpdir
  script:
    - tar --strip-components 1 -xf ${CI_PROJECT_DIR}/*.tar.*
    - python setup.py bdist_wheel --dist-dir ${CI_PROJECT_DIR}
  dependencies:
    - sdist
  artifacts:
    paths:
      - '*.whl'

# Build Docker container for dependencies
.dependencies: &dependencies
  stage: build
  variables:
    GIT_STRATEGY: fetch
    IMAGE_TAG: $CI_REGISTRY_IMAGE/$CI_JOB_NAME:$CI_COMMIT_REF_NAME
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - |
      cat <<EOF > Dockerfile
      FROM igwn/base:conda
      COPY environment.yml .
      RUN conda config --set always_yes yes
      RUN conda config --add channels conda-forge
      RUN conda update -n base conda setuptools
      RUN conda env update -n base -f environment.yml
      RUN conda install vim wget curl
      RUN rm -f environment.yml
      ENTRYPOINT bash
      EOF
    - docker build -t $IMAGE_TAG .
    - docker push $IMAGE_TAG
    - if [ "${CI_COMMIT_TAG:0:1}" = "v" ]; then docker tag $IMAGE_TAG ${IMAGE_TAG%:*}:latest; docker push ${IMAGE_TAG%:*}:latest; fi
  only:
    changes:
      - environment.yml
dependencies/conda:
  <<: *dependencies

# Run unit tests and coverage measurement
.test: &test
  stage: test
  coverage: '/^TOTAL\s+.*\s+(\d+\.?\d*)%/'
  <<: *in-tmpdir
  script:
    - tar --strip-components 1 -xf ${CI_PROJECT_DIR}/*.tar.*
    - conda config --set always_yes yes
    - conda config --add channels conda-forge
    - conda install pytest pytest-cov pytest-console-scripts
    - pip install .
    - python -m pytest -vv --cov=idq tests --junit-xml=${CI_PROJECT_DIR}/junit.xml --doctest-modules --ignore setup.py --ignore doc/ --ignore build/ --ignore idq/classifiers/keras.py
  dependencies:
    - sdist
  artifacts:
    reports:
      junit: junit.xml
test/conda:
  image: $CI_REGISTRY_IMAGE/dependencies/conda:$CI_COMMIT_REF_NAME
  <<: *test

# Build docker container for library
.docker: &docker
  stage: deploy
  script:
    - IMAGE_TAG=$CI_REGISTRY_IMAGE/${CI_JOB_NAME#*/}:$CI_COMMIT_REF_NAME
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - |
      cat <<EOF > Dockerfile
      FROM $CI_REGISTRY_IMAGE/dependencies/conda:$CI_COMMIT_REF_NAME
      COPY *.tar.* .
      RUN pip install *.tar.*
      ENTRYPOINT bash
      EOF
    - docker build -t $IMAGE_TAG .
    - docker push $IMAGE_TAG
    - if [ "${CI_COMMIT_TAG:0:1}" = "v" ]; then docker tag $IMAGE_TAG ${IMAGE_TAG%:*}:latest; docker push ${IMAGE_TAG%:*}:latest; fi
  dependencies:
    - sdist
docker/conda:
  <<: *docker

# Generate documentation
doc:
  image: $CI_REGISTRY_IMAGE/conda:$CI_COMMIT_REF_NAME
  stage: docs
  <<: *in-tmpdir
  script:
    - tar --strip-components 1 -xf ${CI_PROJECT_DIR}/*.tar.*

    # install tex dependencies
    - apt-get update -y
    - apt-get install -y dvipng texlive-latex-base texlive-latex-extra

    # install doc dependencies
    - conda install -y -c conda-forge 'sphinx < 3.0' graphviz sphinx_rtd_theme sphinxcontrib-programoutput

    # build docs
    - python setup.py build_sphinx
    - mv build/sphinx/html $CI_PROJECT_DIR
  artifacts:
    paths:
    - html
  dependencies:
    - docker/conda
    - sdist

# Publish docs
pages:
  stage: publish
  script:
    - mv html/ public/
  artifacts:
    paths:
      - public
  only:
    - master
  dependencies:
    - doc
