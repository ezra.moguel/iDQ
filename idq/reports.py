__description__ = "a module that houses methods and classes used to produce html reports"
__author__ = "Reed Essick (reed.essick@ligo.org), Patrick Godwin (patrick.godwin@ligo.org)"
__doc__ = "\n\n".join([__description__, __author__])

#-------------------------------------------------

from collections import defaultdict
import getpass
import json
import logging
import os
import time

import numpy as np

try:
    from html import HTML
except ImportError: ### python3 version of html
    try:
        from html3.html3 import HTML
    except ImportError:
        HTML = None

try:
    from lal.gpstime import tconvert
except ImportError:
    tconvert = None

from . import configparser
from . import plots
from . import names
from . import features
from . import factories
from . import logs
from . import utils
from .io.reporters.hdf5 import HDF5SegmentReporter

from ligo.segments import segment, segmentlist

from idq import __version__ ### import this for the "entire library"
__process_name__ = "iDQ"


logger = logging.getLogger('idq')

#-------------------------------------------------

REPORT_NAME = 'report'
COMPARISON_NAME = 'Comparison'

DEFAULT_PGLITCH_THRESHOLD = 0.9
DEFAULT_FIGTYPE = 'png'
DEFAULT_LINKS = [
    ('https://git.ligo.org/reed.essick/iDQ', 'iDQ source code'),
    ('https://git.ligo.org/reed.essick/iDQ/wikis', 'iDQ wiki'),
    ('https://docs.ligo.org/reed.essick/iDQ/', 'iDQ docs'),
]

#-------------------------------------------------

class Report(object):
    """generate everything needed for a DQR report
    """
    _title_fontsize = 14

    def __init__(self, config_path, start, end, t0=None, zoom_start=None, zoom_end=None):
        self._start = start
        self._end = end
        self._t0 = t0
        self._zoom_start = zoom_start
        self._zoom_end = zoom_end
        self._path2config(config_path)

    @property
    def config(self):
        return self._config

    @property
    def config_path(self):
        return self._config_path

    @property
    def instrument(self):
        return self._instrument

    @property
    def start(self):
        return self._start

    @property
    def end(self):
        return self._end

    @property
    def zoom_start(self):
        return self._zoom_start if (self._zoom_start is not None) else self._start

    @property
    def zoom_end(self):
        return self._zoom_end if (self._zoom_end is not None) else self._end

    @property
    def t0(self):
        return self._t0 if (self._t0 is not None) else 0.5*(self.zoom_start+self.zoom_end)

    @property
    def tag(self):
        return self._tag

    @property
    def dataloader(self):
        return self._dataloader

    @property
    def dataset_factory(self):
        return factories.DatasetFactory(self.dataloader)

    def _path2config(self, config_path):
        self._config_path = os.path.abspath(os.path.realpath(config_path))
        self._config = configparser.path2config(config_path)
        self._instrument = self.config.instrument
        self._tag = self.config.tag

        if 'frequency' in self.config.features:
            target_bounds = configparser.config2bounds(self.config.samples['target_bounds'])
            frequency = self.config.features['frequency']
            assert frequency in target_bounds, f'must specify a frequency range (called {frequency}) within target_bounds'
            freq_min, freq_max = target_bounds[frequency]
            assert isinstance(freq_min, int) and isinstance(freq_max, int), 'frequency bounds must be integers!'
            self._freq_bounds = freq_min, freq_max
        else:
            self._freq_bounds = None

        rootdir = os.path.abspath(os.path.realpath(self.config.rootdir))
        timeseriesdir = names.tag2timeseriesdir(self.tag, rootdir=rootdir)
        traindir = names.tag2traindir(self.tag, rootdir=rootdir)
        evaluatedir = names.tag2evaluatedir(self.tag, rootdir=rootdir)
        calibratedir = names.tag2calibratedir(self.tag, rootdir=rootdir)

        ### reporters
        reporter_factory = factories.ReporterFactory()

        # timeseries
        ritems = self.config.timeseries['reporting']
        self.timeseriesreporter = reporter_factory(timeseriesdir, self.start, self.end, **configparser.add_missing_kwargs(ritems, group=names.tag2group(self.tag, 'timeseries')))

        # train
        ritems = self.config.train['reporting']
        self.trainreporter = reporter_factory(traindir, self.start, self.end, **configparser.add_missing_kwargs(ritems, group=names.tag2group(self.tag, 'timeseries')))

        # evaluate
        ritems = self.config.evaluate['reporting']
        self.evaluatereporter = reporter_factory(evaluatedir, self.start, self.end, **configparser.add_missing_kwargs(ritems, group=names.tag2group(self.tag, 'timeseries')))

        # calibrate
        ritems = self.config.calibrate['reporting']
        self.calibratereporter = reporter_factory(calibratedir, self.start, self.end, **configparser.add_missing_kwargs(ritems, group=names.tag2group(self.tag, 'timeseries')))

        ### classifier data
        dataloader_factory = factories.DataLoaderFactory()
        citems = self.config.features
        citems = configparser.add_missing_kwargs(citems, group=names.tag2group(self.tag, 'timeseries'))
        self._dataloader = dataloader_factory(self.start, self.end, **citems)

    def _filter_series(self, series):
        start = series['t0']
        dt = series['deltaT']
        end = start + dt*len(list(series['data'].values())[0])
        times = np.arange(start, end, dt)
        series['data']['times'] = times

        if start < self.start: ### truncate the start
            truth = series['data']['times']>=self.start
            for key, val in series['data'].items():
                series['data'][key] = val[truth]
            series['t0'] = self.start

        if end > self.end: ### turncate the end
            truth = series['data']['times']<self.end
            for key, val in series['data'].items():
                series['data'][key] = val[truth]

        return series

    def _concatenate_series(self, series):
        if len(series)==0:
            return series
        ans = []

        current = series[0]
        start = current['t0']
        dt = current['deltaT']
        end = start + dt*len(list(current['data'].values())[0])
        model = current['model']
        calib = current['calib']

        for key, val in current['data'].items():
            current['data'][key] = (val,) ### set up for concatenating

        for thing in series[1:]:
            s = thing['t0']
            d = thing['deltaT']
            e = s + d*len(list(thing['data'].values())[0])
            if (s==end) and (dt==d) and (model==thing['model']) and (calib==thing['calib']):
                end = e
                for key, value in current['data'].items():
                    current['data'][key] = value + (thing['data'][key],) ### assume all series share the same keys
                continue ### don't need to append this to ans yet
                
            ans.append(current)
            current = thing
            start = s
            dt = d
            end = e
            model = thing['model']
            calib = thing['calib']
            for key, val in current['data'].items():
                current['data'][key] = (val,) ### set up for concatenating

        ans.append(current)

        for thing in ans: ### actually concatenate the arrays
            for key, value in thing['data'].items():
                if len(value):
                    thing['data'][key] = np.concatenate(value)

        return ans

    def _series2segs(self, series):
        segs = segmentlist([segment(s['t0'], s['t0']+s['deltaT']*len(list(s['data'].values())[0])) for s in series])
        segs.coalesce() ### just to be safe
        return segs

    def _find_segs(self, series_dict, logger=None, start=None, end=None):
        if start is None:
            start = self.start
        if end is None:
            end = self.end
        if logger is not None:
            logger.info('finding spans used for models within [%.3f, %.3f)'%(start, end))
        models_segdict = {nickname: defaultdict(segmentlist) for nickname in series_dict.keys()}
        calibs_segdict = {nickname: defaultdict(segmentlist) for nickname in series_dict.keys()}
        for nickname, series in series_dict.items():
            for s in series:
                seg = segment(s['t0'], s['t0']+ s['data']['times'].size * s['deltaT'])
                models_segdict[nickname][s['model']].append(seg)
                calibs_segdict[nickname][s['calib']].append(seg)
        for nickname in models_segdict.keys():
            for model in models_segdict[nickname].keys():
                models_segdict[nickname][model].coalesce()
        for nickname in calibs_segdict.keys():
            for calib in calibs_segdict[nickname].keys():
                calibs_segdict[nickname][calib].coalesce()
        return models_segdict, calibs_segdict

    def _glob_timeseries(self, nicknames, logger=None, start=None, end=None):
        if start is None:
            start = self.start
        if end is None:
            end = self.end
        if logger is not None:
            logger.info('globbing timeseries within [%.3f, %.3f)'%(start, end))
        series = defaultdict(list)
        for nickname in nicknames:
            for _, _, S in self.timeseriesreporter.glob(names.nickname2timeseries_topic(self.instrument, nickname), start, end):
                for s in S:
                    s = self._filter_series(s)
                    if len(s['data']['times']): ### some remaining time
                        series[nickname].append(s)
        if logger is not None:
            for nickname in nicknames:
                logger.info('found %d series for %s'%(len(series[nickname]), nickname))
        return dict((nickname, self._concatenate_series(s)) for nickname, s in series.items())

    def _glob_models(self, nicknames, logger=None, start=None, end=None):
        if start is None:
            start = self.start
        if end is None:
            end = self.end
        if logger is not None:
            logger.info('globbing models within [%.3f, %.3f)'%(start, end))
        models = defaultdict(dict)
        for nickname in nicknames:
            for s, e, m in self.trainreporter.glob(names.nickname2train_topic(nickname), start, end):
                models[nickname].update({names.start_end2hash(s,e):m})
        if logger is not None:
            for nickname in nicknames:
                logger.info('found %d models for %s'%(len(models[nickname]), nickname))
        return models

    def  _retrieve_models(self, hashes_dict, logger=None, start=None, end=None):
        if start is None:
            start = self.start
        if end is None:
            end = self.end
        if logger is not None:
            logger.info('retrieving models within [%.3f, %.3f) base on specified hashes'%(start, end))
        models = defaultdict(dict)
        for nickname, hashes in hashes_dict.items():
            name = names.nickname2train_topic(nickname)
            for h in hashes:
                self.trainreporter.start, self.trainreporter.end = names.hash2start_end(h)
                models[nickname].update({h:self.trainreporter.retrieve(name)})
        if logger is not None:
            for nickname, hashes in hashes_dict.items():
                logger.info('found %d models out of %d requested for %s'%(len(models[nickname]), len(hashes), nickname))
        return models

    def _glob_calibration_maps(self, nicknames, logger=None, start=None, end=None):
        if start is None:
            start = self.start
        if end is None:
            end = self.end
        if logger is not None:
            logger.info('globbing calibration maps within [%.3f, %.3f)'%(start, end))
        calibs = defaultdict(dict)
        for nickname in nicknames:
            for s, e, c in self.calibratereporter.glob(names.nickname2calibrate_topic(nickname), start, end):
                calibs[nickname].update({names.start_end2hash(s,e):c})
        if logger is not None:
            for nickname in nicknames:
                logger.info('found %d calibration maps for %s'%(len(calibs[nickname]), nickname))
        return calibs

    def _retrieve_calibration_maps(self, hashes_dict, logger=None, start=None, end=None):
        if start is None:
            start = self.start
        if end is None:
            end = self.end
        if logger is not None:
            logger.info('retrieving calibration maps within [%.3f, %.3f) based on specified hashes'%(start, end))
        calibs = defaultdict(dict)
        for nickname, hashes in hashes_dict.items():
            name = names.nickname2calibrate_topic(nickname)
            for h in hashes:
                self.calibratereporter.start, self.calibratereporter.end = names.hash2start_end(h)
                calibs[nickname].update({h:self.calibratereporter.retrieve(name)})
        if logger is not None:
            for nickname, hashes in hashes_dict.items():
                logger.info('found %d calibration maps out of %d requested for %s'%(len(calibs[nickname]), len(hashes), nickname))
        return calibs

    def _glob_datasets(self, nicknames, logger=None, start=None, end=None):
        if start is None:
            start = self.start
        if end is None:
            end = self.end
        if logger is not None:
            logger.info('globbing evaluated datasets within [%.3f, %.3f)'%(start, end))

        datasets = {nickname: features.Dataset(start=start, end=end) for nickname in nicknames}

        segs = segmentlist([segment(start, end)])
        for nickname in nicknames:
            for _, _, dataset in self.evaluatereporter.glob(names.nickname2evaluate_topic(nickname), start, end):
                dataset.filter(segs=utils.segments_intersection(dataset.segs, segs)) ### do this so we don't mess up dataset's internals
                if len(dataset) > 0:  # only combine non-empty datasets
                    datasets[nickname] += dataset
        if logger is not None:
            for nickname in nicknames:
                gch, cln = datasets[nickname].vectors2classes()
                logger.info('found %d gch samples and %d cln samples for %s'%(len(gch), len(cln), nickname))
        for nickname in nicknames:
            datasets[nickname]._dataloader = self.dataloader
        return datasets

    def path(self, name, output_dir='.', suffix='png'):
        return os.path.join(output_dir, '%s-%s.%s'%(name, self.instrument+__process_name__+self.tag, suffix))

    def _stats(self,
            nicknames,
            datasets,
            models,
            calibmaps,
            segs=None,
            series=None,
            logger=None,
            pglitch_thr=DEFAULT_PGLITCH_THRESHOLD,
            zoom=False,
        ):
        if zoom:
            start = self.zoom_start
            end = self.zoom_end
        else:
            start = self.start
            end = self.end
        if segs is None:
            segs = segmentlist([segment(start, end)])

        if logger is not None:
            logger.info('computing basic statistics')
        cols = [
            'nickname',
            'No. glitch samples',
            'No. clean samples',
            'No. models',
            'No. calibration maps',
        ]
        if series is not None:
            cols += [
                '% time with p(glitch|aux)>='+'%.3f'%pglitch_thr,
                'walltime duty cycle',
                'science duty cycle',
            ]

        data = []
        for nickname in nicknames:
            # do some quick data manipulations
            gch, cln = datasets[nickname].vectors2classes()
            datum = [
                nickname,
                '%d'%len(gch),
                '%d'%len(cln),
                '%d'%len(models[nickname]),
                '%d'%len(calibmaps[nickname]),
            ]

            if series is not None:
                assert self._freq_bounds is not None, ('must specify a frequency range within target_bounds as well as a '
                                                       'frequency column in [timeseries data discovery] if timeseries is specified')
                key = names.channel_name_template(self._instrument, nickname, *self._freq_bounds)%names.PGLITCH
                times = np.concatenate(tuple(s['data']['times'] for s in series[nickname]))
                pglitch = np.concatenate(tuple(s['data'][key] for s in series[nickname]))
                pglitch = pglitch[(self.zoom_start<=times)*(times<self.zoom_end)]

                datum.append('%.3e'%(100.*np.sum(pglitch>=pglitch_thr)/len(pglitch)))

                these_segs = self._series2segs(series[nickname])
                datum.append('%.3e'%min(1., utils.livetime(these_segs)/(end-start))) ### series might slightly over-cover requested segs
                datum.append('%.3e'%(utils.livetime(utils.segments_intersection(these_segs, segs))/utils.livetime(segs)))

            data.append(datum)

        return cols, data

    def _timeseries_figure(self, nicknames, series, gch_gps=[], legend=True, segs=None, output_dir='.', figtype=DEFAULT_FIGTYPE, logger=None, title=None, zoom=True):
        if logger is not None:
            logger.info('generating timeseries plots')
        alttext = 'timeseries'

        if zoom:
            start = self.zoom_start
            end = self.zoom_end
        else:
            start = self.start
            end = self.end
        fig = plots.timeseries(nicknames, series, start, end, self.t0, gch_gps=gch_gps, legend=legend, segs=segs)
        if title is None:
            title = '%s Timeseries\nwithin [%.3f, %.3f)'%(self.instrument, self.start, self.end)
        fig.suptitle(title, fontsize=self._title_fontsize)
        path = self.path(alttext+'-'+'-'.join(nicknames)+'-', output_dir=output_dir, suffix=figtype)

        if logger is not None:
            logger.info('saving: '+path)
        fig.savefig(path)
        plots.close(fig)
        return alttext, path

    def _dataset_corner(self, nicknames, datasets, output_dir='.', figtype=DEFAULT_FIGTYPE, logger=None):
        if logger is not None:
            logger.info('generating dataset corner')

        ### divide up
        gch = dict()
        cln = dict()
        for nickname in nicknames:
            gch[nickname], cln[nickname] = datasets[nickname].vectors2classes()

        ### plot glitch correlations
        gch_alttext = 'glitch_corner'
        fig = plots.dataset_corner(nicknames, gch)
        title = '%s Glitch Correlations\nwithin [%.3f, %.3f)'%(self.instrument, self.start, self.end)
        fig.suptitle(title, fontsize=self._title_fontsize)
        gch_path = self.path(gch_alttext+'-'+'-'.join(nicknames)+'-', output_dir=output_dir, suffix=figtype)

        if logger is not None:
            logger.info('saving: '+gch_path)
        fig.savefig(gch_path)
        plots.close(fig)

        ### plot clean correlations
        cln_alttext = 'clean_corner'
        fig = plots.dataset_corner(nicknames, cln)
        title = '%s Clean Correlations\nwithin [%.3f, %.3f)'%(self.instrument, self.start, self.end)
        fig.suptitle(title, fontsize=self._title_fontsize)
        cln_path = self.path(cln_alttext+'-'+'-'.join(nicknames)+'-', output_dir=output_dir, suffix=figtype)

        if logger is not None:
            logger.info('saving: '+cln_path)
        fig.savefig(cln_path)
        plots.close(fig)

        return (gch_alttext, gch_path), (cln_alttext, cln_path)

    def _calibration_coverage(self, nicknames, series, datasets, legend=True, output_dir='.', figtype=DEFAULT_FIGTYPE, logger=None, title=None):
        if logger is not None:
            logger.info('generating calibration coverage plots')
        alttext = 'calibration_coverage'

        fig = plots.calibration_coverage(nicknames, series, datasets, legend=legend)
        if title is None:
            title = '%s Calibration Coverage\nwithin [%.3f, %.3f)'%(self.instrument, self.start, self.end)
        fig.suptitle(title, fontsize=self._title_fontsize)
        path = self.path(alttext+'-'+'-'.join(nicknames)+'-', output_dir=output_dir, suffix=figtype)

        if logger is not None:
            logger.info('saving: '+path)
        fig.savefig(path)
        plots.close(fig)
        return alttext, path

    def _calibration_distribs(self, nicknames, calibmaps, legend=True, output_dir='.', figtype=DEFAULT_FIGTYPE, logger=None, title=None):
        if logger is not None:
            logger.info('generating calibration distribution plots')
        alttext = 'calibration_distribs'

        fig = plots.calibration_distribs(nicknames, calibmaps, legend=legend)
        if title is None:
            title = '%s Calibration Distributions\nwithin [%.3f, %.3f)'%(self.instrument, self.start, self.end)
        fig.suptitle(title.replace('_','\_'), fontsize=self._title_fontsize)
        path = self.path(alttext+'-'+'-'.join(nicknames)+'-', output_dir=output_dir, suffix=figtype)

        if logger is not None:
            logger.info('saving: '+path)
        fig.savefig(path)
        plots.close(fig)
        return alttext, path

    def _roc(self, nicknames, datasets, series=None, legend=True, output_dir='.', figtype=DEFAULT_FIGTYPE, logger=None, title=None):
        if logger is not None:
            logger.info('generating ROC plots')
        alttext = 'roc'

        fig = plots.roc(nicknames, series, datasets, legend=legend)
        if title is None:
            title = '%s Receiver Operating Characteristics\nwithin [%.3f, %.3f)'%(self.instrument, self.start, self.end)
        fig.suptitle(title.replace('_','\_'), fontsize=self._title_fontsize)
        path = self.path(alttext+'-'+'-'.join(nicknames)+'-', output_dir=output_dir, suffix=figtype)

        if logger is not None:
            logger.info('saving: '+path)
        fig.savefig(path)
        plots.close(fig)
        return alttext, path

    def _feature_importance(self, nicknames, models, datasets=None, segdict=None, series=None, legend=True, output_dir='.', figtype=DEFAULT_FIGTYPE, logger=None, zoom=True):

        # instantiate classifiers to pick up some standard stuff...
        classifier_factory = factories.ClassifierFactory()
        klassifiers = {nickname: classifier_factory(nickname, rootdir=output_dir, **self.config.classifier_map[nickname]) for nickname in nicknames}

        # actually compute stuff
        if zoom:
            start = self.zoom_start
            end = self.zoom_end
        else:
            start = self.start
            end = self.end

        if logger is not None:
            logger.info('generating feature importance plots and tables within [%.3f, %.3f)'%(start, end))

        figs, tabs = plots.featureimportance(nicknames, models, klassifiers, datasets, start, end, self.t0, segdict=segdict, legend=legend)
        paths = []
        cols_datas = []
        rocs = []
        for nickname in nicknames:
            for key, model in models[nickname].items():
                model_name = '%s[%s)'%(nickname, model.hash.replace('_',','))

                fig = figs[nickname][key]
                tab = tabs[nickname][key]
                if (fig is None) and (tab is None) and (logger is not None):
                    logger.warn('feature importance not available for nickname='+nickname)

                if fig is not None:
                    alttext = nickname+'-'+self.instrument+'_featureimportance-'+model.hash+'-'
                    path = self.path(alttext, output_dir=output_dir, suffix=figtype)
                    if logger is not None:
                        logger.info('saving: '+path)
                    fig.savefig(path)
                    plots.close(fig)
                    paths.append((model_name, alttext, path))
                else:
                    paths.append((model_name, None, None))

                if tab is not None:
                    cols, data = tab
                    cols_datas.append((model_name, cols, data))
                else:
                    cols_datas.append((model_name, None, None))

                ### generate roc for this segment
                if datasets is not None:
                    dataset = datasets[nickname]
                    if segdict is not None: ### get just the samples within this segment
                        dataset = dataset.copy()
                        dataset.filter(segs=utils.segments_intersection(dataset.segs, segdict[nickname][model.hash]))

                    alttext, path = self._roc(
                        [model_name],
                        {model_name:dataset},
                        series={model_name:series[nickname]} if series is not None else None,
                        legend=legend,
                        output_dir=output_dir,
                        figtype=figtype,
                        logger=logger,
                        title=model_name,
                    )
                    rocs.append((model_name, alttext, path))
                else:
                    rocs.append((model_name, None, None))

        return paths, cols_datas, rocs

    def report(self,
            nicknames,
            reportdir='.',
            logdir='.',
            verbose=False,
            figtype=DEFAULT_FIGTYPE,
            links=DEFAULT_LINKS,
            ignore_segdb=False,
            skip_timeseries=False,
            overview_only=False,
            annotate_gch_gps=False,
            single_calib_plots=True,
            pglitch_thr=DEFAULT_PGLITCH_THRESHOLD,
            legend=True,
            segments=None,
            log_level=logs.DEFAULT_LOG_LEVEL,
        ):
        ### act like a DiskReporter...
        reportdir = names.start_end2fragmented_dir(self.start, self.end, rootdir=reportdir)
        if not os.path.exists(reportdir):
            os.makedirs(reportdir)

        logs.configure_logger(
            logger,
            names.tag2report_logname(self.tag),
            log_level=log_level,
            rootdir=logdir,
            verbose=verbose
        )
        logger.info('generating report for %s within [%.3f, %.3f) with %s'%(', '.join(nicknames), self.start, self.end, self.config_path))

        ### go get all the data you'll need
        # evaluated datasets
        datasets = self._glob_datasets(nicknames, logger=logger)

        # models and calibmaps
        if skip_timeseries: ### just grab everything available
            series = None
            models = self._glob_models(nicknames, logger=logger)
            calibmaps = self._glob_calibration_maps(nicknames, logger=logger)
            models_segdict = calibs_segdict = None
        else: ### grab series and only load the ones used to generate this data
            series = self._glob_timeseries(nicknames, logger=logger)
            models = self._retrieve_models(dict((nickname, set(s['model'] for s in series[nickname])) for nickname in nicknames), logger=logger)
            calibmaps = self._retrieve_calibration_maps(dict((nickname, set(s['calib'] for s in series[nickname])) for nickname in nicknames), logger=logger)
            models_segdict, calibs_segdict = self._find_segs(series, logger=logger)

        if ignore_segdb:
            logger.info( "ignoring segdb" )
            segs = segmentlist([segment(self.start, self.end)])

        elif segments is not None:
            logger.info("segments provided ahead of time, skipping query to segdb")
            segs = segmentlist(segments)

        else:
            segdb_intersect = self.config.segments["intersect"].split() if "intersect" in self.config.segments else []
            segdb_exclude = self.config.segments["exclude"].split() if "exclude" in self.config.segments else []
            segdb_url = self.config.segments["segdb_url"]

            logger.info( "querying %s within [%.3f, %.3f) for: intersect=%s ; exclude=%s"%(segdb_url, self.start, self.end, ','.join(segdb_intersect), ','.join(segdb_exclude)) )

            segs = utils.segdb2segs(
                self.start,
                self.end,
                intersect=segdb_intersect,
                exclude=segdb_exclude,
                segdb_url=segdb_url,
                logger=logger,
            )
        lvtm = utils.livetime(segs)
        logger.info('retained %.3f sec of livetime'%lvtm)

        ### record segments
        ### do this backflip so segment reporter doesn't create subdirectories...
        segmentreporter = HDF5SegmentReporter(reportdir, self.start, self.end)
        path = os.path.join(reportdir, names.start_end2path('segments', self.start, self.end, suffix=segmentreporter.suffix))
        segmentreporter._write(path, segs)
        logger.info( 'segments written to '+path )

        ### generate report as an html document
        generate_html = HTML is not None ### build a doc if we have the library available

        # add banner header
        if generate_html:
            doc, _, body = self.html_head_body()
            body.br()
            navbar = body.nav(klass='navbar navbar-expand-lg navbar-light bg-light')

            navbar.a(klass="navbar-brand").big('%s Report at %s (%s)'%(__process_name__, self.instrument, self.tag))

            div = body.div(klass='container-fluid')
            table = div.table(klass="table table-condensed table-hover")
            thead = table.thead(style='display:none;')
            thead.th('type')
            thead.th('info')
            tbody = table.tbody()
            tr = tbody.tr()
            tr.th('start', scope='row')
            if tconvert is None:
                tr.td('%.3f'%self.start)
            else:
                tr.td('%.3f (%s)'%(self.start, tconvert(self.start)))
            tr = tbody.tr()
            tr.th('end', scope='row')
            if tconvert is None:
                tr.td('%.3f'%self.end)
            else:
                tr.td('%.3f (%s)'%(self.end, tconvert(self.end)))
            tr = tbody.tr()
            tr.th('livetime', scope='row')
            tr.td('%.3f seconds'%lvtm)
            tr = tbody.tr()
            tr.th('detector duty cycle', scope='row')
            tr.td('%.3f'%(lvtm/(self.end-self.start)))

        ### generate a table summarizing the basic numbers for each nickname
        if generate_html:
            args = self._stats(
                nicknames,
                datasets,
                models,
                calibmaps,
                segs=segs,
                series=series,
                logger=logger,
                pglitch_thr=pglitch_thr,
            )
            body.hr()
            div = body.div(klass='container-fluid', id='BasicStatistics')
            div.h2('Basic Statistics')
            div.br()
            self._add_table(div, *args)
            div.br()

        ### make plots
        # ROC curve
        args = self._roc(
            nicknames,
            datasets,
            series=series,
            legend=legend,
            output_dir=reportdir,
            figtype=figtype,
            logger=logger,
        )
        if generate_html:
            body.hr()
            div = body.div(klass='container-fluid', id='ROC')
            div.h2('Receiver Operating Characteristic Curves')
            div.br()
            self._add_figure(div, *args)
            div.br()

        # series and calib stuff that depends on series
        if series is not None:
            gch_gps = []
            if annotate_gch_gps:
                for q in datasets.values():
                    gch_gps += list(q.vectors2classes()[0].times)
                gch_gps = sorted(set(gch_gps))

            # timeseries
            args = self._timeseries_figure(
                nicknames,
                series,
                gch_gps=gch_gps,
                legend=legend,
                segs=segs,
                output_dir=reportdir,
                figtype=figtype,
                logger=logger,
            )
            if generate_html:
                body.hr()
                div = body.div(klass='container-fluid', id='Timeseries')
                div.h2('Timeseries')
                div.br()
                self._add_figure(div, *args)

            ### FIXME: add cumulative histograms of timeseries behavior

            # calibration coverage
            args = self._calibration_coverage(
                nicknames,
                series,
                datasets,
                legend=legend,
                output_dir=reportdir,
                figtype=figtype,
                logger=logger,
            )
            if generate_html:
                body.hr()
                div = body.div(klass='container-fluid', id='Calibration') ### will be used below as well
                div.h2('Calibration')
                div.br()
                self._add_figure(div, *args)

        elif generate_html:
            div = body.div(klass='container-fluid', id='Calibration') ### will be used below

        # calibration distributions
        args = self._calibration_distribs(
            nicknames,
            calibmaps,
            legend=legend,
            output_dir=reportdir,
            figtype=figtype,
            logger=logger,
        )
        if generate_html:
            self._add_figure(div, *args)
            div.br()

        # add correlation plots
        if len(nicknames)>1:
            gch_args, cln_args = self._dataset_corner(
                nicknames,
                datasets,
                output_dir=reportdir,
                figtype=figtype,
                logger=logger,
                )
            if generate_html:
                div = body.div(klass='container-fluid', id='Comparison')
                div.h2('Comparisons')
                div.br()
                self._add_figure(div, *gch_args)
                self._add_figure(div, *cln_args)

            ### FIXME: add a corner for series as well?

        ### generate single-classifier summary pages
        if not overview_only:
            single_paths = self.classifier_report(
                nicknames,
                datasets,
                models,
                calibmaps,
                segs,
                series=series,
                models_segdict=models_segdict,
                calibs_segdict=calibs_segdict,
                logger=logger,
                annotate_gch_gps=annotate_gch_gps,
                pglitch_thr=pglitch_thr,
                output_dir=reportdir,
                figtype=figtype,
            )
            if generate_html: ### add link to single-classifier page in navbar
                ul = navbar.ul(klass='navbar-nav mr-auto')
                ul.li.button(klass='btn btn-link').a(COMPARISON_NAME, href="#")
                for nickname in nicknames:
                    ul.li.button(klass='btn btn-link').a(nickname, href=os.path.basename(single_paths[nickname]))

        # write date into a footer
        if generate_html:
            body.hr()
            div = body.div(klass='container-fluid', id='footer')
            div.p('generated at %s'%time.strftime("%H:%M:%S %Z %a %d %b %Y", time.localtime()))
            ul = div.ul()
            ul.li('hostname : '+os.environ.get('HOSTNAME', 'unkown'))
            ul.li('username : '+os.environ.get('USER', 'unkown'))

            ### write html page
            path = self.write_html(doc, reportdir=reportdir)
            logger.info('html document written to: '+path)

        else:
            path = None

        ### return path to the top-level html page
        return path

    def classifier_report(
            self,
            nicknames,
            datasets,
            models,
            calibmaps,
            segs,
            series=None,
            models_segdict=None,
            calibs_segdict=None,
            logger=None,
            annotate_gch_gps=False,
            single_calib_plots=True,
            pglitch_thr=DEFAULT_PGLITCH_THRESHOLD,
            legend=True,
            output_dir='.',
            figtype=DEFAULT_FIGTYPE,
        ):
        """
        generate single-classifier summary information
        """
        ### generate report as an html document
        generate_html = HTML is not None ### build a doc if we have the library available
        toplevel_path = self.path(REPORT_NAME, output_dir=output_dir, suffix='html')

        lvtm = utils.livetime(segs)

        paths = dict()
        for nickname in nicknames:
            if logger is not None:
                logger.info('generating single-classifier HTML page for '+nickname)

            if generate_html:
                doc, _, body = self.html_head_body()
                body.br()
                navbar = body.nav(klass='navbar navbar-expand-lg navbar-light bg-light')

                navbar.a(klass="navbar-brand").big('%s Report for %s at %s (%s)'%(__process_name__, nickname, self.instrument, self.tag))

                ul = navbar.ul(klass='navbar-nav mr-auto')
                ul.li.button(klass='btn btn-link').a(COMPARISON_NAME, href=os.path.basename(toplevel_path))
                for nname in nicknames:
                    ul.li.button(klass='btn btn-link').a(nname, href=os.path.basename(self.path(nname, output_dir=output_dir, suffix='html')))

                div = body.div(klass='container-fluid')
                table = div.table(klass="table table-condensed table-hover")
                thead = table.thead(style='display:none;')
                thead.th('type')
                thead.th('info')
                tbody = table.tbody()
                tr = tbody.tr()
                tr.th('start', scope='row')
                if tconvert is None:
                    tr.td('%.3f'%self.start)
                else:
                    tr.td('%.3f (%s)'%(self.start, tconvert(self.start)))
                tr = tbody.tr()
                tr.th('end', scope='row')
                if tconvert is None:
                    tr.td('%.3f'%self.end)
                else:
                    tr.td('%.3f (%s)'%(self.end, tconvert(self.end)))
                tr = tbody.tr()
                tr.th('livetime', scope='row')
                tr.td('%.3f seconds'%lvtm)
                tr = tbody.tr()
                tr.th('detector duty cycle', scope='row')
                tr.td('%.3f'%(lvtm/(self.end-self.start)))

                ### generate a table summarizing the basic numbers for each nickname
                args = self._stats(
                    [nickname],
                    datasets,
                    models,
                    calibmaps,
                    segs=segs,
                    series=series,
                    logger=logger,
                    pglitch_thr=pglitch_thr,
                )
                body.hr()
                div = body.div(klass='container-fluid', id='BasicStatistics')
                div.h2('Basic Statistics')
                div.br()
                self._add_table(div, *args)
                div.br()

            ### make plots
            # ROC curve
            args = self._roc(
                [nickname],
                datasets,
                series=series,
                legend=legend,
                output_dir=output_dir,
                figtype=figtype,
                logger=logger,
            )
            if generate_html:
                body.hr()
                div = body.div(klass='container-fluid', id='ROC')
                div.h2('Receiver Operating Characteristic Curves')
                div.br()
                self._add_figure(div, *args)
                div.br()

            # series and calib stuff that depends on series
            if series is not None:
                gch_gps = []
                if annotate_gch_gps:
                    for q in datasets.values():
                        gch_gps += list(q.vectors2classes()[0].times)
                    gch_gps = sorted(set(gch_gps))

                # timeseries
                args = self._timeseries_figure(
                    [nickname],
                    series,
                    gch_gps=gch_gps,
                    legend=legend,
                    segs=segs,
                    output_dir=output_dir,
                    figtype=figtype,
                    logger=logger,
                    zoom=False, ### do not restrict ourselves to zoom window here
                )
                if generate_html:
                    body.hr()
                    div = body.div(klass='container-fluid', id='Timeseries')
                    div.h2('Timeseries')
                    div.br()
                    self._add_figure(div, *args)

                ### FIXME: add cumulative histograms of timeseries behavior

                # calibration coverage
                args = self._calibration_coverage(
                    [nickname],
                    series,
                    datasets,
                    legend=legend,
                    output_dir=output_dir,
                    figtype=figtype,
                    logger=logger,
                )
                if generate_html:
                    body.hr()
                    div = body.div(klass='container-fluid', id='Calibration') ### will be used below as well
                    div.h2('Calibration')
                    div.br()
                    self._add_figure(div, *args)

            elif generate_html:
                div = body.div(klass='container-fluid', id='Calibration') ### will be used below

            # calibration distributions
            args = self._calibration_distribs(
                [nickname],
                calibmaps,
                legend=legend,
                output_dir=output_dir,
                figtype=figtype,
                logger=logger,
            )
            if generate_html:
                self._add_figure(div, *args)
                div.br()

            # individual calibration distribs
            if single_calib_plots:
                if generate_html:
                    D = div.div(klass='accordian', id='CalibrationAccordian')
                for calibmap in calibmaps[nickname].values(): ### add a section to calibration for each individual map
                    model_name = '%s[%s)'%(nickname, calibmap.hash.replace('_',','))

                    # calibration coverage for only the samples evaluated with this map
                    if series is not None:
                        dataset = datasets[nickname]
                        if calibs_segdict is not None: ### filter this to only contain times when which this calibmap was used
                            dataset = dataset.copy()
                            dataset.filter(segs=utils.segments_intersection(dataset.segs, calibs_segdict[nickname][calibmap.hash]))

                        coverage_args = self._calibration_coverage(
                            [model_name],
                            {model_name:series[nickname]},
                            {model_name:dataset},
                            legend=legend,
                            output_dir=output_dir,
                            figtype=figtype,
                            logger=logger,
                            title=model_name,
                        )
                    else:
                        coverage_args = None

                    # distribution showing only this map
                    distrib_args = self._calibration_distribs(
                        [model_name],
                        {model_name:{calibmap.hash:calibmap}},
                        legend=legend,
                        output_dir=output_dir,
                        figtype=figtype,
                        logger=logger,
                        title=model_name,
                    )

                    if generate_html:
                        nname = nickname+calibmap.hash
                        card = D.div(klass='card')

                        d = card.div(klass='card-header', id='headerCalibration'+nname)
                        d.raw_text('<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#bodyCalibration%(nname)s" aria-expanded="false" aria-controls="bodyCalibration%(nname)s"><big>%(model_name)s</big></button>'%{'nname':nname, 'model_name':model_name}) ### done because bootstrap wants hyphens in it's attributes

                        card_body = HTML(newlines=True).div(klass='card-body')
                        if coverage_args is not None:
                            self._add_figure(card_body, *coverage_args)
                        self._add_figure(card_body, *distrib_args)
                        div.br()
                        d = card.raw_text('<div id="bodyCalibration%(nname)s" class="collapse" aria-labelledby="headerCalibration%(nname)s" data-parent="#CalibrationAccordian">'%{'nname':nname} + str(card_body) + '</div>')

            # feature importance
            if generate_html:
                body.hr()
                div = body.div(klass='container-fluid', id='FeatureImportance')
                div.h2('Feature Importance')
                D = div.div(klass='accordian', id='FeatureImportanceAccordian')

            for i, (model_hash, model) in enumerate(models[nickname].items(), 1):
                if logger is not None:
                    logger.info('generating {}/{} feature importance plots'.format(i, len(models[nickname])))
                importance_paths, importance_cols_datas, importance_rocs = self._feature_importance(
                    [nickname],
                    {nickname: {model_hash: model}},
                    datasets,
                    segdict=models_segdict,
                    series=series,
                    legend=legend,
                    output_dir=output_dir,
                    figtype=figtype,
                    logger=logger,
                    zoom=False, ### do not restrict ourselves to the zoom window for these plots
                )

                if generate_html:
                    for (model_name, alttext, path), (_, cols, data), (_, roc_alttext, roc_path) in zip(importance_paths, importance_cols_datas, importance_rocs):
                        nname = model_name.replace('[','').replace(')','').replace(',','_') ### undo the mapping so we don't break javascript
                        card = D.div(klass='card')

                        d = card.div(klass='card-header', id='headerFeatureImportance'+nname)
                        d.raw_text('<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#bodyFeatureImportance%(nname)s" aria-expanded="false" aria-controls="bodyFeatureImportance%(nname)s"><big>%(model_name)s</big></button>'%{'nname':nname, 'model_name':model_name}) ### done because bootstrap wants hyphens in it's attributes

                        card_body = HTML(newlines=True).div(klass='card-body')
                        if path is not None:
                            self._add_figure(card_body, alttext, path)
                        if roc_path is not None:
                            self._add_figure(card_body, roc_alttext, roc_path)
                        if cols is not None:
                            self._add_table(card_body, cols, data)
                        d = card.raw_text('<div id="bodyFeatureImportance%(nname)s" class="collapse" aria-labelledby="headerFeatureImportance%(nname)s" data-parent="#FeatureImportanceAccordian">'%{'nname':nname} + str(card_body) + '</div>')

            # add footer
            if generate_html:
                body.hr()
                div = body.div(klass='container-fluid', id='footer')
                div.p('generated at %s'%time.strftime("%H:%M:%S %Z %a %d %b %Y", time.localtime()))
                ul = div.ul()
                ul.li('hostname : '+os.environ.get('HOSTNAME', 'unkown'))
                ul.li('username : '+os.environ.get('USER', 'unkown'))

                ### write html page
                paths[nickname] = self.write_html(doc, output_dir, nickname=nickname)
                logger.info('html document written to: '+paths[nickname])

            else:
                paths[nickname] = None

        return paths

    def write_html(self, doc, reportdir, nickname=REPORT_NAME):
        path = self.path(nickname, output_dir=reportdir, suffix='html')
        with open(path, 'w') as obj:
            obj.write(str(doc))
        return path

    @staticmethod
    def _add_table(div, cols, data):
        table = div.table(klass="table table-condensed table-hover")
        tr = table.tr()
        for col in cols:
            tr.th(col)
        for datum in data:
            tr = table.tr()
            for thing in datum:
                tr.td(str(thing))

    @staticmethod
    def _add_figure(div, alttext, path, width=1000, **kwargs):
        div.img(klass='img-fluid', src=os.path.basename(path), alt=alttext, width=str(width), **dict((key,str(val)) for key, val in kwargs.items()))

    def html_head_body(self):
        doc = HTML(newlines=True)
        doc.raw_text('<!DOCTYPE html>')
        htmldoc = doc.html(lang='en')
        head = htmldoc.head()

        ### set up header for bootstrap
        head.meta(charset='utf-8')
        head.meta(content='IE=edge')._attrs.update({'http-equiv':"X-UA-Compatible"}) ### forced into modifying _attr by "-" in attribute name
        head.meta(name="viewport", content="width=device-width, initial-scale=1")

        ### set up header for this specific template
        head.link(
            href="https://stackpath.bootstrapcdn.com/bootswatch/4.3.1/flatly/bootstrap.min.css",
            rel="stylesheet",
        )

        head.raw_text('<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>')
        head.raw_text('<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>')
        head.raw_text('<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>')

        ### other header information
        head.meta(name="description", content=__process_name__+" report for %s (%s) within [%.3f, %.3f)"%(self.instrument, self.tag, self.start, self.end))
        head.meta(name="author", content=getpass.getuser()) ### whoever ran this is the author

        #----------------
        ### build body
        body = htmldoc.body()
        body = body.div(klass='container-fluid')

        return doc, head, body
