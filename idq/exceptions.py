__description__ = "a python module housing custom exceptions"
__author__ = "Reed Essick (reed.essick@ligo.org), Patrick Godwin (patrick.godwin@ligo.org)"
__doc__ = "\n\n".join([__description__, __author__])

#-------------------------------------------------

class IncontiguousDataError(Exception):
    """there is a gap in the data read from a Kafka queue"""
    def __init__(self, gap_start, gap_end, new_timestamp):
         self.timestamp = new_timestamp
         self.gap_start = gap_start
         self.gap_end = gap_end

    def __str__(self):
        return "Data was not contiguous. Gap found: [%.3f, %.3f). Suggest searching for data starting at %.3f"%(self.gap_start, self.gap_end, self.timestamp)

class NoDataError(Exception):
    """No data was available from a Kafka queue"""
    def __init__(self, timestamp, stride):
         self.timestamp = timestamp
         self.stride = stride

    def __str__(self):
        return "No data found within [%.3f, %.3f), timestamp requested is %.3f"%(self.timestamp, self.timestamp+self.stride, self.timestamp)

class LatencyError(Exception):
    """A process is too far behind realtime"""
    def __init__(self, current_time, latency, new_timestamp):
        self.current_timestamp = current_time
        self.latency = latency
        self.timestamp = new_timestamp

    def __str__(self):
        return "Too far behind realtime (%.3f sec behind %.3f). The most recent sensible stride would start at %.3f"%(self.latency, self.current_timestamp, self.timestamp)

class MaxIter(StopIteration):
    def __init__(self, max_iter):
        self.max_iter = max_iter

    def __str__(self):
        return "Too many iterations (%d)"%self.max_iter

class UntrainedError(Exception):
    """
    an exception raised if there is no trained model present within a SupervisedClassifier object
    """
    pass

class UncalibratedError(Exception):
    """
    an exception raised if there is no calibration model present within a SupervisedClassifier object
    """
    pass

class UnevaluatedError(Exception):
    """
    an exception raised if the FeatureVector object has not been evaluated (the rank has not been set)
    """
    pass

class ConfigurationInconsistency(Exception):
    """
    an exception raised if there is an inconsistency discovered within a configuration file
    """
    pass
