__description__ = "a module that houses methods used to produce visualizations"
__author__ = "Patrick Godwin (patrick.godwin@ligo.org)"
__doc__ = "\n\n".join([__description__, __author__])

#-------------------------------------------------

import os
import copy
from collections import defaultdict
import itertools
import numpy as np

import matplotlib
matplotlib.use('Agg')
from matplotlib import figure
from matplotlib import pyplot as plt

from ligo.segments import segment, segmentlist

### non-standard packages
from idq import utils
from idq import features
from idq import names
from idq import calibration

#-------------------------------------------------
### default plot settings

matplotlib.rcParams.update({
    "font.size": 9.0,
    "axes.titlesize": 10.0,
    "axes.labelsize": 8.0,
    "xtick.labelsize": 8.0,
    "ytick.labelsize": 8.0,
    "legend.fontsize": 8.0,
    'font.family': ['sans-serif'],
    'font.sans-serif': [
        'FreeSans',
        'Helvetica Neue',
        'Helvetica',
        'Arial',
    ] + matplotlib.rcParams['font.sans-serif'],
    "figure.dpi": 300,
    "savefig.dpi": 300,
    "text.usetex": False,
    "path.simplify": True,
    "agg.path.chunksize": 50000,
})

DEFAULT_COLORMAP = 'copper_r'
COLORS = 'b r g c m k'.split()
DEFAULT_ALPHA = 0.75
DEFAULT_SHADE_ALPHA = 0.25
DEFAULT_SERIES_LINEWIDTH = 0.8

#-------------------------------------------------

PGLITCH_LABEL = '$p(\mathrm{glitch}|\mathrm{aux})$'
LOGLIKE_LABEL = '$\ln \Lambda^\mathrm{glitch}_\mathrm{clean}$'
EFF_FAP_LABEL = 'Efficiency/FAP'
FAP_LABEL = 'FAP'
RANK_LABEL = 'Rank'

CUM_GLITCH_LIKE_LABEL = '$P(\mathrm{aux}|\mathrm{glitch})$'
GLITCH_LIKE_LABEL = '$p(\mathrm{aux}|\mathrm{glitch})$'
CUM_CLEAN_LIKE_LABEL = '$P(\mathrm{aux}|\mathrm{clean})$'
CLEAN_LIKE_LABEL = '$p(\mathrm{aux}|\mathrm{clean})$'

#-------------------------------------------------
### DQR plotting helper functions

def color_generator():
    return itertools.cycle(COLORS)

def timeseries(nicknames, series, start, end, t0, legend=True, fig=None, gch_gps=[], segs=None):
    """plot the series information in a standard way
    """
    if fig is None:
        fig = plt.figure()
    ax_pglitch = plt.subplot(5, 1, 1)
    ax_loglike = plt.subplot(5, 1, 2)
    ax_eff_fap = plt.subplot(5, 1, 3)
    ax_fap = plt.subplot(5, 1, 4)
    ax_rank = plt.subplot(5, 1, 5)

    # plot
    get_color = color_generator()
    for nickname in nicknames:
        color = next(get_color)
        kwargs = dict(alpha=DEFAULT_ALPHA, color=color, label=nickname, lw=DEFAULT_SERIES_LINEWIDTH)
        for s in series[nickname]:
            if 'times' in s['data']:
                times = s['data']['times']
            else:
                times = np.arange(s['t0'], s['deltaT']*len(list(s['data'].values())[0]), s['deltaT'])

            # check that the times are relevant for the actual requested stretch
            truth = (start<=times)*(times<end)
            if not np.any(truth):
                continue ### simply skip this
            times = times[truth] - t0

            for key, val in s['data'].items():
                ### NOTE: the way we extract channels might be a bit fragile...
                ### FIXME: predict the names based on nickname
                if names.LOGLIKE in key:
                    ax_loglike.plot(times, val[truth], **kwargs)
                elif names.PGLITCH in key:
                    ax_pglitch.plot(times, val[truth], **kwargs)
                elif names.RANK in key:
                    ax_rank.plot(times, val[truth], **kwargs)
                elif names.EFF in key:
                    eff_key = key
                elif names.FAP in key:
                    fap_key = key
                    ax_fap.plot(times, val[truth], **kwargs)

            # plot eff/fap as a special case
            ax_eff_fap.plot(times, s['data'][eff_key][truth]/s['data'][fap_key][truth], **kwargs)

            # remove label from kwargs
            kwargs.pop('label', None)

    # decorate
    ax_pglitch.set_ylabel(PGLITCH_LABEL)
    ax_loglike.set_ylabel(LOGLIKE_LABEL)
    ax_eff_fap.set_ylabel(EFF_FAP_LABEL)
    ax_fap.set_ylabel(FAP_LABEL)
    ax_rank.set_ylabel(RANK_LABEL)

    ax_rank.set_xlabel('Time [sec] relative to %.3f'%(t0))

    ax_loglike.set_yscale('symlog', linthreshy=10)
    ax_eff_fap.set_yscale('log')
    ax_fap.set_yscale('log')

    xlim = (start-t0, end-t0)
    ax_rank.set_xlim(xlim)
    for ax in [ax_pglitch, ax_loglike, ax_eff_fap, ax_fap]:
        plt.setp(ax.get_xticklabels(), visible=False)
        ax.set_xlim(xlim)

    if legend:
        ax_rank.legend(loc='best')

    # annotate
    ylims = dict((ax, ax.get_ylim()) for ax in [ax_pglitch, ax_loglike, ax_eff_fap, ax_fap, ax_rank])
    # add annotation gps
    if len(gch_gps):
        gch_gps = np.array(gch_gps)
        gch_gps = gch_gps[(start<=gch_gps)*(gch_gps<end)] # filter by only the times that will show up
        gch_gps = gch_gps-t0
        verts = np.empty((2, len(gch_gps)), dtype=float)
        gch_gps = [gch_gps]*2
        for ax, (ymin, ymax) in ylims.items():
            verts[0,:] = ymin
            verts[1,:] = ymax
            ax.plot(gch_gps, verts, color='grey', linestyle='dashed', alpha=DEFAULT_SHADE_ALPHA)
            ax.set_ylim(ymin=ymin, ymax=ymax)

    # shade the inactive segements
    if segs is not None:
        for s, e in utils.remove_segments([[start, end]], segs):
            for ax, (ymin, ymax) in ylims.items():
                ax.fill_between([s-t0, e-t0], [ymin]*2, [ymax]*2, color='grey', alpha=DEFAULT_SHADE_ALPHA)
                ax.set_ylim(ymin=ymin, ymax=ymax)

    plt.subplots_adjust(hspace=0.10, wspace=0.10)
    return fig

def dataset_corner(nicknames, datasets):
    """generate a corner plot showing the correlations between ranks
    """
    ### get the set of all gps times present
    times = set()
    for nickname in nicknames:
        times = times.union(set(datasets[nickname].times))
    times = np.array(sorted(times), dtype=float)

    ### construct an array representing the data set
    data = np.empty((len(nicknames), len(times)), dtype=float)
    data[:] = np.nan
    for j, nickname in enumerate(nicknames):
        for v in datasets[nickname]:
            data[j][v.time==times] = v.rank

    ### make a simple corner plot
    return _corner(np.transpose(data), [nickname+' '+RANK_LABEL for nickname in nicknames], linestyle='none', marker='.')

def _corner(data, labels, color=None, linestyle='none', marker='.', fig=None):
    """construct a basic corner plot
    """
    if fig is None:
        fig = plt.figure()
    else:
        plt.figure(fig.number)

    Nsamp, N = data.shape
    alpha = max(1./Nsamp, 0.10)
    ranges = []
    for i in range(N):
        m = np.min(data[:,i])
        M = np.max(data[:,i])
        d = 0.05*(M-m)
        ranges.append((m-d, M+d))

    num_bins = max(10, int(Nsamp**0.5))+1
    bins = [np.linspace(m, M, num_bins) for m, M in ranges]

    for row in range(N):
        for col in range(row+1):
            ax = plt.subplot(N, N, row*N+col+1)

            if col==row:
                if color is None:
                    ax.hist(data[:,col], bins=bins[col], histtype='step')
                else:
                    ax.hist(data[:,col], color=color, histtype='step')
            else:
                if color is None:
                    ax.plot(data[:,col], data[:,row], linestyle=linestyle, marker=marker, alpha=alpha)
                else:
                    ax.plot(data[:,col], data[:,row], color=color, linestyle=linestyle, marker=marker, alpha=alpha)
                ax.set_ylim(ranges[row])
            ax.set_xlim(ranges[col])

            if (col==0) and (row!=0):
                ax.set_ylabel(labels[row])
            else:
                plt.setp(ax.get_yticklabels(), visible=False)

            if row==N-1:
                ax.set_xlabel(labels[col])
            else:
                plt.setp(ax.get_xticklabels(), visible=False)

    plt.subplots_adjust(hspace=0.05, wspace=0.05)

    return fig

def roc(nicknames, series, datasets, legend=True, fig=None):
    """plot the ROC curves in a standard way
    """
    if fig is None:
        fig = plt.figure()
    ax = fig.gca()

    get_color = color_generator()

    xmin = 1e-3
    for nickname in nicknames:
        dataset = datasets[nickname]
        ### actually plot data
        if len(dataset):
            xmin = min(xmin, 0.1/len(dataset))

        ### directly from ranks in evaluated datasets
        color = next(get_color)

        _dataset2roc(ax, dataset, color=color, label=nickname+": "+RANK_LABEL, linestyle='dashed', alpha=DEFAULT_ALPHA, shade_alpha=DEFAULT_SHADE_ALPHA, marker='.')

        ### plot based on p(glitch) assigned by timeseries
        if series is not None:
            # update vectors to reflect the p(glitch) they were assigned
            for key in series[nickname][0]['data'].keys(): ### find the relevant key
                if 'PGLITCH' in key:
                    break
            dset = dataset.copy() ### make a copy so we don't muck up the original data
            ranks = [_series2val(series[nickname], t, key, default=0) for t in dset.times]
            dset.evaluate(ranks)
            _dataset2roc(ax, dset, color=color, label=nickname+": "+PGLITCH_LABEL, linestyle='solid', alpha=DEFAULT_ALPHA, shade_alpha=DEFAULT_SHADE_ALPHA, marker=None)

    # plot a fair coin's flip
    line = np.logspace(np.log10(xmin), 0, 1001)
    ax.plot(line, line, color='k', linestyle='dashed', alpha=DEFAULT_SHADE_ALPHA)

    ### decorate
    ax.set_xlabel('FAP')
    ax.set_ylabel('Efficiency')

    ax.set_xscale('log')

    ax.set_ylim(ymin=-0.01, ymax=1.01)
    ax.set_xlim(xmin=xmin, xmax=1.01)

    ax.legend(loc='lower right')

    plt.subplots_adjust(hspace=0.10, wspace=0.10)
    return fig

def _series2val(series, gps, key, default=None):
    for s in series:
        t = s['data']['times'] ### expect that this has been added at this point
        dt = s['deltaT']
        if len(t):
            if (t[0]<=gps) and (gps<t[-1]+dt):
                return np.interp(gps, t, s['data'][key])
    else:
        if default is not None:
            return default
        else:
            segs = segmentlist([])
            for s in series:
                t = s['data']['times'] ### expect that this has been added at this point
                dt = s['deltaT']
                segs.append(segment(t[0], t[-1]+dt))
                segs.coalesce()
            raise ValueError('could not find gps=%.3f in any of the provided series spanning:\n%s'%(gps, repr(segs)))

def _dataset2roc(ax, dataset, color=None, label=None, linestyle='solid', alpha=DEFAULT_ALPHA, shade_alpha=DEFAULT_SHADE_ALPHA, marker='o'):
    """helper function for generating an ROC curve from a dataset
    """
    ranks = np.array(sorted(set(dataset.ranks))) ### the list of all ranks observed in datasets
    gch, cln = dataset.vectors2classes()
    
    ### actually plot data
    ### directly from ranks in evaluated datasets
    eff = np.zeros_like(ranks, dtype=float)
    fap = np.zeros_like(ranks, dtype=float)
    
    for r in gch.ranks:
        eff += r >= ranks
    eff /= len(gch)
    seff = (eff*(1-eff)/len(gch))**0.5

    for r in cln.ranks:
        fap += r > ranks
    fap /= len(cln)
    sfap = (fap*(1-fap)/len(cln))**0.5

    # plot uncertainty regions
    if color is not None:
        ax.plot(fap, eff, drawstyle='steps', color=color, marker=marker, label=label, linestyle=linestyle, alpha=alpha)
    else:
        color = ax.plot(fap, eff, drawstyle='steps', marker=marker, label=label, linestyle=linestyle, alpha=alpha)[0].get_color()
    for e, se, f, sf, r in zip(eff, seff, fap, sfap, ranks):
        ax.fill_between([f-sf, f+sf], [e-se]*2, [e+se]*2, color=color, alpha=shade_alpha)

    return color

def calibration_distribs(nicknames, calibmaps, legend=True, fig=None, pdf_ymin=1e-4, pdf_ymax=1e4):
    """plot the distributions over observed ranks and the calibration KDEs
    """
    if fig is None:
        fig = plt.figure()
    ax_cum_gch = plt.subplot(2,2,1)
    ax_cum_cln = plt.subplot(2,2,2)
    ax_gch = plt.subplot(2,2,3)
    ax_cln = plt.subplot(2,2,4)

    get_color = color_generator()

    # plot histograms of ranks within dataset
    num = np.sum([len(calibmaps[nickname]) for nickname in nicknames])
    alpha = max(0.10, 0.75/num)
    shade_alpha = max(0.01, alpha/4.)
    for nickname in nicknames:
        kwargs = dict(alpha=alpha, label=nickname, color=next(get_color))
        fill_between_kwargs = dict(alpha=shade_alpha, color=kwargs['color'])
        # plot distirbutions stored within calibration maps
        for calibmap in calibmaps[nickname].values():
#            label = nickname.replace('_','\_')+': [%s, %s)'%tuple(calibmap.hash.split('_')) if labelme else None
            for ax_cum, ax, kde in [(ax_cum_cln, ax_cln, calibmap._cln_kde), (ax_cum_gch, ax_gch, calibmap._gch_kde)]:
                # control the number and placement of bins by hand so we don't end up with enormous things
                ranks = kde.ranks

                # extract data
                # cumulative distributions
                cdf = 1.-kde.cdf(ranks)
                cdf_low = 1.-kde.cdf_quantile(ranks, 0.16)
                cdf_high = 1.-kde.cdf_quantile(ranks, 0.84)

                # differential distributions
                pdf = kde.pdf(ranks)
                pdf_low = kde.pdf_quantile(ranks, 0.16)
                pdf_high = kde.pdf_quantile(ranks, 0.84)

                # actually plot
                if isinstance(kde, calibration.FixedBandwidth1DKDE): ### continuous case
                    ax_cum.plot(ranks, cdf, **kwargs)

                    kwargs.pop('label', None) ### remove this so we do not repeatedly label

                    ax_cum.hist(kde.obs, bins=ranks, histtype='step', density=True, cumulative=-1, linestyle='dashed', **kwargs)
                    ax_cum.fill_between(ranks, cdf_low, cdf_high, **fill_between_kwargs)

                    ax.plot(ranks, pdf, **kwargs)
                    ax.fill_between(ranks, pdf_low, pdf_high, **fill_between_kwargs)
                    ax.hist(kde.obs, bins=ranks, histtype='step', density=True, linestyle='dashed', **kwargs)

                else: ### discrete case
                    ax_cum.step(ranks, cdf, where='post', marker='.', **kwargs)

                    kwargs.pop('label', None) ### remove this so we do not repeatedly label

                    ax_cum.hist(kde.obs, bins=[0]+list(ranks)+[1], histtype='step', density=True, cumulative=-1, linestyle='dashed', **kwargs)
                    for r, R, l, h in zip(ranks[:-1], ranks[1:], cdf_low[:-1], cdf_high[:-1]):
                        ax_cum.fill_between([r, R], [l]*2, [h]*2, **fill_between_kwargs)
                    if len(ranks):
                        ax_cum.fill_between([ranks[-1], 1], [cdf_low[-1]]*2, [cdf_high[-1]]*2, **fill_between_kwargs)

                    ax.plot(ranks, pdf, linestyle='none', marker='.', **kwargs)
                    for r, l, h in zip(ranks, pdf_low, pdf_high):
                        ax.plot([r]*2, [l, h], **fill_between_kwargs)

    ### decorate
    ax_cum_gch.set_ylabel(CUM_GLITCH_LIKE_LABEL)
    ax_cum_cln.set_ylabel(CUM_CLEAN_LIKE_LABEL)
    ax_gch.set_ylabel(GLITCH_LIKE_LABEL)
    ax_cln.set_ylabel(CLEAN_LIKE_LABEL)

    xlim = (0., 1.)
    ylim = (0., 1.)
    for ax in [ax_cum_gch, ax_cum_cln]:
        plt.setp(ax.get_xticklabels(), visible=False)
        ax.set_xlim(xlim)
        ax.set_ylim(ylim)

    for ax in [ax_gch, ax_cln]:
        ax.set_xlabel(RANK_LABEL)
        ax.set_xlim(xlim)
        ax.set_yscale('log')
        ymin, ymax = ax.get_ylim()
        ymin = max(pdf_ymin, ymin) if pdf_ymin > 0 else ymin
        ymax = min(pdf_ymax, ymax) if pdf_ymax > 0 else ymax
        ax.set_ylim(ymin=ymin, ymax=ymax)

    for ax in [ax_cum_cln, ax_cln]:
        ax.yaxis.tick_right()
        ax.yaxis.set_label_position('right')

    if legend:
        ax_cum_cln.legend(loc='best')

    plt.subplots_adjust(hspace=0.05, wspace=0.05)
    return fig    

def _series2coverage(series, dataset, key):
    num = len(dataset)
    if num: ### there are samples to use
        nom = defaultdict(int)
        for t in dataset.times:
            nom[_series2val(series, t, key, default=1)] += 1

        nom, frc = np.transpose(np.array(list(nom.items()), dtype=float))
        order = nom.argsort()
        nom = nom[order]
        frc = np.cumsum(frc[order])/num

        return nom, frc, (frc*(1-frc)/num)**0.5
    else:
        return np.array([], dtype=float), np.array([], dtype=float), np.array([], dtype=float)

def calibration_coverage(nicknames, series, datasets, legend=True, fig=None):
    """plot the calibration coverage in a standard way
    """
    if fig is None:
        fig = plt.figure()
    ax_eff = plt.subplot(1,2,1)
    ax_fap = plt.subplot(1,2,2)

    get_color = color_generator()

    ### prepare data
    min_fap = 1e-3
    for nickname in nicknames:
        ### FIXME: predict key names instead of this thing, which could be fragile
        for key in series[nickname][0]['data'].keys(): ### find the relevant key
            if names.EFF in key:
                effkey = key
            elif names.FAP in key:
                fapkey = key

        gch, cln = datasets[nickname].vectors2classes()

        color = next(get_color)
        # plot efficiency
        obs_eff, frac_eff, s_eff = _series2coverage(series[nickname], gch, effkey)
        ax_eff.plot(obs_eff, frac_eff, linestyle='None', marker='.', color=color, alpha=DEFAULT_ALPHA, label=nickname)[0].get_color()
        for x, y, dy in zip(obs_eff, frac_eff, s_eff):
            ax_eff.plot([x]*2, [y-dy, y+dy], color=color, alpha=DEFAULT_SHADE_ALPHA)

        # plot fap
        obs_fap, frac_fap, s_fap = _series2coverage(series[nickname], cln, fapkey)
        ax_fap.plot(obs_fap, frac_fap, linestyle='None', marker='.', color=color, alpha=DEFAULT_ALPHA, label=nickname)
        for x, y, dy in zip(obs_fap, frac_fap, s_fap):
            ax_fap.plot([x]*2, [y-dy, y+dy], color=color, alpha=DEFAULT_SHADE_ALPHA)

        if np.any(obs_fap>0):
            min_fap = min(np.min(obs_fap[obs_fap>0]), min_fap)

    # plot reference lines
    x = np.logspace(np.log10(min_fap), 0, 1001)
    ax_eff.plot(x, x, color='k', alpha=DEFAULT_SHADE_ALPHA, linestyle='dashed')
    ax_fap.plot(x, x, color='k', alpha=DEFAULT_SHADE_ALPHA, linestyle='dashed')

    # decorate
    if legend:
        ax_fap.legend(loc='best')

    ax_eff.set_xlabel('Nominal Efficiency')
    ax_eff.set_ylabel('Observed Fraction')

    ax_fap.set_xlabel('Nominal FAP')
    ax_fap.set_ylabel('Observed Fraction')
    ax_fap.yaxis.tick_right()
    ax_fap.yaxis.set_label_position('right')

    lim = (0., 1.)
    ax_eff.set_xlim(lim)
    ax_eff.set_ylim(lim)

    lim = (min_fap, 1)
    ax_fap.set_xlim(lim)
    ax_fap.set_ylim(lim)
    ax_fap.set_xscale('log')
    ax_fap.set_yscale('log')

    plt.subplots_adjust(hspace=0.05, wspace=0.05)
    return fig

def featureimportance(nicknames, models, classifiers, datasets, start, end, t0, segdict=None, **kwargs):
    """delegate to models heavily here
    """
    seg = segmentlist([segment(start,end)])

    ### limit segs to cover only series if available
    if segdict is not None:
        all_segs = segmentlist([segs for nickname in nicknames
                                     for model in models[nickname].values()
                                     for segs in segdict[nickname][model.hash]])
        all_segs.coalesce()
        all_segs = utils.segments_intersection(all_segs, seg)

    figs = dict()
    tabs = dict()
    for nickname in nicknames:
        working_dataset = datasets[nickname].copy()
        working_dataset.filter(all_segs)

        f = dict()
        t = dict()
        classifier = classifiers[nickname]
        for key, model in models[nickname].items():
            classifier.model = model ### assign this so internal plumbing works out

            ### only show feature importances over time in which model was evaluated for
            if segdict is not None:
                intersection = utils.segments_intersection(seg, segdict[nickname][model.hash])
                if utils.livetime(intersection)==0:
                    raise ValueError('no overlap between target segment and segdict for '+model.hash)
                start, end = intersection.extent()

            ### make a figure
            try:
                fig = classifier.feature_importance_figure(working_dataset, start, end, t0, **kwargs)
                fig.suptitle('%s Feature Importance within [%.3f, %.3f)'%(nickname, start, end))
                f[key] = fig
            except NotImplementedError:
                f[key] = None
            ### make a table
            try:
                t[key] = classifier.feature_importance_table(working_dataset, start, end, t0)
            except NotImplementedError:
                t[key] = None
        figs[nickname] = f
        tabs[nickname] = t
    return figs, tabs

#-------------------------------------------------
### plotting utilities

def close(fig):
    """
    delegate to pyplot.close
    """
    plt.close(fig)
