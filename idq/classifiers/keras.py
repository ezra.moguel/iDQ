__description__ = "a module that houses keras-based classifier implementations"
__author__ = "Patrick Godwin (patrick.godwin@ligo.org), Kyle Rose (rosek@kenyon.edu)"
__doc__ = "\n\n".join([__description__, __author__])

#-------------------------------------------------

import logging
import warnings

import numpy as np

from ligo.segments import segmentlist

### NOTE: ignore numpy FutureWarnings from tensorflow/Keras
###       remove when not needed
warnings.filterwarnings('ignore', category=FutureWarning)
from keras.models import Model, Sequential
from keras.layers import Activation, Dense, Dropout, Flatten, Reshape
from keras.layers import LocallyConnected1D as Local1D
from keras.regularizers import l1, l2, l1_l2
from keras_pickle_wrapper import KerasPickleWrapper
from sklearn.utils import class_weight
from sklearn.model_selection import train_test_split
import tensorflow

from .. import exceptions
from .. import hookimpl
from .. import io
from .. import features
from .. import utils

from . import ClassifierModel, SupervisedClassifier
from . import DEFAULT_DT


logger = logging.getLogger('idq')

#-------------------------------------------------
### defaults

DEFAULT_COLUMN_VALUE = 0

#-------------------------------------------------
### classifier implementations

class KerasModel(ClassifierModel):
    """the model object for SupervisedKerasClassifier
    """

    def __init__(self, start, end, keras, whitener, channels, downselector, transformer, time=features.DEFAULT_TIME_NAME, segs=None):
        ClassifierModel.__init__(self, start, end, segs=segs)

        ### define stuff particular to Keras
        self._keras = keras
        self._whitener = whitener

        ### set up how features are extracted and transformed
        self._selector = features.Selector(
            channels=channels,
            time=time,
            downselector=downselector,
            transformer=transformer,
        )

    @property
    def keras(self):
        return self._keras()

    @property
    def whitener(self):
        return self._whitener

    @property
    def channels(self):
        return self._selector.channels

    @property
    def time(self):
        return self._selector.time

    @property
    def selector(self):
        return self._selector

class SupervisedKerasClassifier(SupervisedClassifier):
    """
    A base class for supervised keras classifiers
    Note: not a standalone classifier
    """
    _flavor = "keras_supervised_classifier"
    _required_kwargs = [
        'safe_channels_path',
        'window',
        'time',
        'significance',
        'layer',
        'loss',
        'optimizer',
        'metrics',
        'epochs',
        'batch_size',
    ]

    def __init__(self, *args, **kwargs):
        SupervisedClassifier.__init__(self, *args, **kwargs)

    def train(self, dataset):
        """
        Trains a model using feature data, makes predictions using the training set's class distribution
        """
        verbose = self.kwargs.get('verbose', False)

        ### sanity check training data
        num_glitch, num_clean = dataset.vectors2num()
        if num_glitch == 0 or num_clean == 0:
            raise ValueError(
                "training Keras-based classifiers not allowed with zero target or clean "
                "(random) times. this may indicate an issue with data discovery or not "
                "requesting enough time for training."
            )

        ### set random seed if specified
        random_seed = self.kwargs.get('random_state', None)
        if random_seed is not None:
            logger.info('setting Keras random seed: %d'%random_seed)
            tensorflow.random.set_seed(random_seed)

        ### create the model, passing in dataset to determine input dimensions
        self.model = self._define_model(dataset)

        ### balance out class weights if specified
        balanced = self.kwargs.get('balanced', False)
        if balanced:
            logger.info('balancing class weights for model')
            class_weights = class_weight.compute_class_weight(
                'balanced',
                classes=np.array([0, 1]),
                y=dataset.labels
            )
        else:
            class_weights = None

        ### configure feature selection and load
        logger.info('generating dataset')
        dataset.configure(self.model.selector)
        dataset.load_data()

        ### train whitener + whiten
        logger.info('whitening data')
        self.model.whitener.train(dataset.features.as_unstructured(), dataset.features.colnames)
        data = self.model.whitener.whiten(dataset.features.as_unstructured(), dataset.features.colnames)

        ### if using validation data, shuffle the training set
        if 'validation_split' in self.kwargs:
            logger.info('shuffling training data based on validation split')
            train_data, test_data, train_labels, test_labels = train_test_split(
                data,
                dataset.labels,
                test_size=self.kwargs['validation_split'],
                shuffle=True
            )
            validation_data = (test_data, test_labels)
        else:
            train_data, train_labels = data, dataset.labels
            validation_data = None

        ### train model
        logger.info('training model')
        self.model.keras.fit(
            train_data,
            train_labels,
            epochs=self.kwargs['epochs'],
            batch_size=self.kwargs['batch_size'],
            class_weight=class_weights,
            validation_data=validation_data,
            verbose=1,
        )

        return self.model

    def evaluate(self, dataset):
        """
        Applies a supervised keras model to feature data.
        """
        if not self.is_trained:
            raise exceptions.UntrainedError('%s does not have an internal model'%self.flavor)

        ### configure feature selection and load
        logger.info('generating dataset')
        dataset.configure(self.model.selector)
        dataset.load_data()

        logger.info('evaluating model')
        whitened = self.model.whitener.whiten(dataset.features.as_unstructured(), dataset.features.colnames)
        ranks = self.model.keras.predict(whitened, verbose=1)
        return dataset.evaluate(ranks.flatten(), hashes=self.model.hash)

    def timeseries(self, dataset_factory, dt=DEFAULT_DT, segs=None):
        """
        Generate a time series of predictions based on predicted model probabilities.
        """
        ### check if model has been trained
        if not self.is_trained:
            raise exceptions.UntrainedError('%s does not have an internal model'%self.flavor)

        if segs is None:
            segs = dataset_factory.classifier_data.segs

        ranks = []
        for seg in segs:
            dataset = dataset_factory.unlabeled(dt=dt, segs=segmentlist([seg]))
            if len(dataset): ### append ranks if dataset isn't empty
                ranks.append( (self.evaluate(dataset).ranks, dataset.times[0], dt) )
        return ranks

    def feature_importance(self):
        """
        return a ranked list of important features within the trained model

        WARNING: NOT IMPLEMENTED YET!
        """
        raise NotImplementedError

    def _define_model(self, dataset):
        """
        Defines and returns a keras classifier model, using keyword arguments as necessary.
        Used internally
        """
        ### set defaults for missing values if not set
        self.kwargs['default'] = self.kwargs.get('default', DEFAULT_COLUMN_VALUE)
        self.kwargs['default_delta_time'] = self.kwargs.get('default_delta_time', -self.kwargs['window'])

        ### set quantities used for calculating layer sizes
        channels = io.path2channels(self.kwargs['safe_channels_path'])
        self.num_channels = len(channels)
        self.num_columns = len(dataset._dataloader.columns)
        self.num_features = self.num_channels * self.num_columns

        return KerasModel(
            dataset.start,
            dataset.end,
            self._define_keras(),
            features.Whitener(),
            channels=channels,
            downselector=features.DownselectLoudest(**self.kwargs),
            transformer=features.DeltaTimeTransformer(**self.kwargs),
            time=self.kwargs['time'],
            segs=dataset.segs,
        )

    def _define_keras(self, *args, **kwargs):
        return KerasPickleWrapper(Sequential())

    def _add_layer(self, model, layer, input_dim=None, **kwargs):
        """
        given a config-formatted set of layer inputs, generate a Keras layer
        """
        if layer['type'] == 'Dense':
            layer_kwargs = {'input_dim': input_dim} if input_dim else {}
            if 'regularizer_type' in layer:
                regularizer = eval(layer['regularizer_type'])(layer['regularizer_value'])
                layer_kwargs.update({'kernel_regularizer': regularizer, 'bias_regularizer': regularizer})
            units = self._process_size(layer['units'])
            layer_kwargs.update({'activation': layer['activation']})
            model.add(Dense(units, **layer_kwargs))

        elif layer['type'] == 'Dropout':
            layer_kwargs = {'input_dim': input_dim} if input_dim else {}
            model.add(Dropout(layer['rate'], **layer_kwargs))

        elif layer['type'] == 'Local1D':
            layer_kwargs = {}
            ### reshape input layer since Local1D layer expects 3-dim data
            if input_dim:
                model.add(Reshape(input_shape=(input_dim,), target_shape=(input_dim,1)))
            else:
                input_shape = model.layers[-1].output_shape
                if not len(input_shape) == 1:
                    raise ValueError('only 1D input shape allowed for Local1D')
                model.add(Reshape(input_shape=(input_shape[0],), target_shape=(input_shape[0], 1)))

            ### extract layer config
            if 'regularizer_type' in layer:
                regularizer = eval(layer['regularizer_type'])(layer['regularizer_value'])
                layer_kwargs.update({'kernel_regularizer': regularizer, 'bias_regularizer': regularizer})
            kernel_size = self._process_size(layer['kernel_size'])
            strides = self._process_size(layer['strides'])
            layer_kwargs.update({'strides': strides, 'activation': layer['activation']})
            model.add(Local1D(layer['filters'], kernel_size, **layer_kwargs))
            model.add(Flatten())

        else:
            raise ValueError('layer=%s not understood'%layer[0])

    def _process_sizes(self, *sizes):
        return tuple([self._process_size(size) for size in sizes])

    def _process_size(self, size):
        """
        parse layer sizes from config with special handling for Nchan, Ncol, Ntotal.
        """
        if isinstance(size, int):
            return size
        elif 'Ncol' in size:
           size = size.replace('Ncol', '')
           if size:
               return int(float(size) * self.num_columns)
           else:
               return self.num_columns
        elif 'Nchan' in size:
           size = size.replace('Nchan', '')
           if size:
               return int(float(size) * self.num_channels)
           else:
               return self.num_channels
        elif 'Ntotal' in size:
           size = size.replace('Ntotal', '')
           if size:
               return int(float(size) * self.num_features)
           else:
               return self.num_features
        else:
            raise ValueError(f"Layer size {size} not recognized as valid")


class NeuralNetwork(SupervisedKerasClassifier):
    """
    A neural network based on Keras
    """
    _flavor = "neural_network"

    def _define_keras(self, **kwargs):
        model = Sequential()

        ### generate layers sequentially
        layers = self.kwargs['layer']
        self._add_layer(model, layers[0], input_dim=self.num_features)
        for layer in layers[1:]:
            self._add_layer(model, layer)

        ### compile the model
        metrics = self.kwargs['metrics'].strip().split()
        model.compile(loss=self.kwargs['loss'], optimizer=self.kwargs['optimizer'], metrics=metrics)

        return KerasPickleWrapper(model)


#-------------------------------------------------
### plugin implementations

@hookimpl
def get_classifiers():
    return {
        "keras:dnn": NeuralNetwork,
    }
