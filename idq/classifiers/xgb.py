__description__ = "a module that houses xgboost-based classifier implementations"
__author__ = "Patrick Godwin (patrick.godwin@ligo.org)"
__doc__ = "\n\n".join([__description__, __author__])

#-------------------------------------------------

from xgboost import XGBClassifier

from .. import hookimpl
from .sklearn import SupervisedSklearnClassifier


#-------------------------------------------------
### classifier implementations

class XGBTree(SupervisedSklearnClassifier):
    """
    A gradient-boosted tree classifier based on xgboost.

    * `XGBoost Intro <https://xgboost.readthedocs.io/en/latest/tutorials/model.html>`_.

    * `XGBoost Hyperparameter Guide <https://xgboost.readthedocs.io/en/latest/parameter.html>`_.

    * `XGBoost API <https://xgboost.readthedocs.io/en/latest/python/python_api.html#module-xgboost.sklearn>`_.

    """
    _flavor = "xgb_tree"

    def classifier(self):
        """
        Creates an extreme gradient-boosted tree classifier.
        """
        return [('classifier', XGBClassifier())]


#-------------------------------------------------
### plugin implementations

@hookimpl
def get_classifiers():
    return {
        "xgboost:xgb_tree": XGBTree,
    }
