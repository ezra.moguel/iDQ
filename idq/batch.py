__description__ = "a module that holds high-level scheduling utilities for batch workflows"
__author__ = "Reed Essick (reed.essick@ligo.org), Patrick Godwin (patrick.godwin@ligo.org)"
__doc__ = "\n\n".join([__description__, __author__])

#---------------------------------------------------------------------------------------------------

from collections import defaultdict
import logging
import multiprocessing as mp
import os

import numpy as np

from ligo.segments import segment, segmentlist

from . import calibration
from . import classifiers
from . import condor
from . import configparser
from . import exceptions
from . import factories
from . import logs
from . import names
from . import plots
from . import reports
from . import utils
from .io.reporters.pkl import PickleReporter


logger = logging.getLogger('idq')
batch_logger = logging.getLogger('idq-batch')

#---------------------------------------------------------------------------------------------------

DEFAULT_BATCH_NUM_BINS = 1
DEFAULT_BATCH_WORKFLOW = 'block'
DEFAULT_BATCH_LOG_LEVEL = logs.DEFAULT_LOG_LEVEL
DEFAULT_INITIAL_LOOKBACK = 3600 # one hour
DEFAULT_NUM_SEGS_PER_BIN = 1

#---------------------------------------------------------------------------------------------------
### batch processes

def condor_batch(gps_start, gps_end, batchdir, nickname, verbose=False):
    '''
    run the training process
    '''
    ### FIXME: record progress in a logger somewhere!

    ### read in the package prepared by idq-train or idq-straming_train
    diskreporter = PickleReporter(batchdir, gps_start, gps_end)
    args, kwargs = diskreporter.retrieve(names.nickname2condorname(nickname))

    ### execute batch
    _batch(*args, **kwargs)


def _batch(
        gps_start,
        gps_end,
        segments,
        exclude_in_training,
        exclude_in_else,
        tempdir,
        temppath,
        finaldir,
        finalpath,
        tag,
        trainreporter_kwargs,
        evaluatereporter_kwargs,
        calibratereporter_kwargs,
        nicknames,
        verbose=False,
        skip_timeseries=False,
        skip_report=False,
    ):
    """
    a helper function for forked workflow via multiprocessing
    """
    ### set up reporters
    reporter_factory = factories.ReporterFactory()

    logdir = names.tag2logdir(tag, rootdir=tempdir)
    trainreporter = reporter_factory(names.tag2traindir(tag, rootdir=tempdir), gps_start, gps_end, **trainreporter_kwargs)
    evaluatereporter = reporter_factory(names.tag2evaluatedir(tag, rootdir=tempdir), gps_start, gps_end, **evaluatereporter_kwargs)
    calibratereporter = reporter_factory(names.tag2calibratedir(tag, rootdir=tempdir), gps_start, gps_end, **calibratereporter_kwargs)

    final_logdir = names.tag2logdir(tag, rootdir=finaldir)
    final_trainreporter = reporter_factory(names.tag2traindir(tag, rootdir=finaldir), gps_start, gps_end, **trainreporter_kwargs)
    final_evaluatereporter = reporter_factory(names.tag2evaluatedir(tag, rootdir=finaldir), gps_start, gps_end, **evaluatereporter_kwargs)
    final_calibratereporter = reporter_factory(names.tag2calibratedir(tag, rootdir=finaldir), gps_start, gps_end, **calibratereporter_kwargs)

    ### run train job, dropping results in the temporary directory
    batch_logger.info('--- TRAIN : %s ---'%logs.get_logpath(logdir, names.tag2train_logname(tag)))
    train(gps_start, gps_end, temppath, verbose=verbose, segments=segments, exclude=exclude_in_training)

    ### run evaluate job, dropping results in temporary directory
    batch_logger.info('--- EVALUATE : %s ---'%logs.get_logpath(logdir, names.tag2evaluate_logname(tag)))
    evaluate(gps_start, gps_end, temppath, verbose=verbose, segments=segments, exclude=exclude_in_else)

    ### run calibrate job, dropping results in temporary directory
    batch_logger.info('--- CALIBRATE : %s ---'%logs.get_logpath(logdir, names.tag2calibrate_logname(tag)))
    calibrate(gps_start, gps_end, temppath, verbose=verbose)

    ### copy models, datasets, calibration maps into final locations
    # copy the resulting models into the appropriate places within evaluate-dir
    batch_logger.info('--- COPYING RESULTS ---')
    segments = utils.remove_segments(segments, exclude_in_else) ### the segments into which we need to copy the models, etc.
                                                                ### we pass in "segments" as an argument to avoid re-computing scisegs from segdb
    for nickname in nicknames:
        train_topic = names.nickname2train_topic(nickname)
        evaluate_topic = names.nickname2evaluate_topic(nickname)
        calibrate_topic = names.nickname2calibrate_topic(nickname)
        
        model = trainreporter.retrieve(train_topic) ### only do I/O once
        dataset = evaluatereporter.retrieve(evaluate_topic)
        calib = calibratereporter.retrieve(calibrate_topic)

        for start, end in segments: # iterate through segments on which we want to have final evaluations
            # copy model
            batch_logger.debug('copying trained model for %s within [%.3f, %.3f) to final location'%(nickname, start, end))
            model.hash = names.start_end2hash(start, end) # update hash to point at new model
            final_trainreporter.start = start
            final_trainreporter.end = end
            final_trainreporter.report(train_topic, model, preferred=True)

            # copy calibration maps
            batch_logger.debug('copying calibration map for %s within [%.3f, %.3f) to final location'%(nickname, start, end))
            calib.hash = names.start_end2hash(start, end) # update hash to point at new calib map
            final_calibratereporter.start = start
            final_calibratereporter.end = end
            final_calibratereporter.report(calibrate_topic, calib, preferred=True)

            # copy (subsets) of quivers
            batch_logger.debug('copying (subset) of evaluated datasets for %s within [%.3f, %.3f) to final location'%(nickname, start, end))
            nsegments = utils.remove_segments(segments, segmentlist([segment(start, end)])) ### the segments that we want to keep in the quiver
            subset = dataset.filter(segs=nsegments) ### removes and returns the subset that does not fall within nsegment
            subset.hash = names.start_end2hash(start, end) # update hash to point at new model
            subset.start = start # update this to reflect just the one segment
            subset.end = end

            final_evaluatereporter.start = start ### report
            final_evaluatereporter.end = end
            final_evaluatereporter.report(evaluate_topic, subset, preferred=True)

    ### run timeseries job over each 1-bin stretch separately
    if skip_timeseries:
        batch_logger.info('--- TIMESERIES : skipping ---')
    else:
        batch_logger.info('--- TIMESERIES : %s ---'%logs.get_logpath(logdir, names.tag2timeseries_logname(tag)))
        for start, end in segments: ### run a separate timeseries job over each segment, which will pick up the model and calibmap copied for that segment
            segs = utils.segments_intersection(segments, segmentlist([segment(start, end)]))
            timeseries(start, end, finalpath, verbose=verbose, segments=segs)

    ### run report job over each 1-bin stretch separately
    if skip_report:
        batch_logger.info('--- REPORT : skipping ---')
    else:
        batch_logger.info('--- REPORT : %s ---'%logs.get_logpath(logdir, names.tag2report_logname(tag)))
        report(gps_start, gps_end, finalpath, verbose=verbose, segments=segments, skip_timeseries=skip_timeseries) ### just do this for EVERYTHING once instead of splitting by segments


def batch(
        gps_start,
        gps_end,
        config_path,
        verbose=False,
        num_bins=DEFAULT_BATCH_NUM_BINS,
        batch_workflow=DEFAULT_BATCH_WORKFLOW,
        batch_log_level=DEFAULT_BATCH_LOG_LEVEL,
        skip_timeseries=False,
        skip_report=False,
        persist=False,
        block=False,
        monitoring_cadence=utils.DEFAULT_MONITORING_CADENCE,
        causal=False,
        num_segs_per_bin=DEFAULT_NUM_SEGS_PER_BIN,
        initial_lookback=DEFAULT_INITIAL_LOOKBACK,
    ):
    '''
    launch and manage all batch processes
    should manage round-robin training/evaluation automatically so users really just have to "point and click"
    '''
    config_path = os.path.abspath(os.path.realpath(config_path))
    config = configparser.path2config(config_path)

    #---------------------------------------------
    ### set up logging
    #---------------------------------------------
    tag = config.tag
    rootdir = config.rootdir
    logdir = names.tag2logdir(tag, rootdir=rootdir)

    logs.configure_logger(
        batch_logger,
        names.tag2batch_logname(tag),
        log_level=batch_log_level,
        rootdir=logdir,
        verbose=verbose
    )
    batch_logger.info( "using config : %s"%config_path )
    batch_logger.info("writing logs to : %s"%logdir)

    #---------------------------------------------
    ### pull out a things that will be used throughout all batch runs
    #---------------------------------------------

    trainreporter_kwargs = {**config.train["reporting"], "group": names.tag2group(tag, 'train')}
    evaluatereporter_kwargs = {**config.evaluate["reporting"], "group": names.tag2group(tag, 'evaluate')}
    calibratereporter_kwargs = {**config.calibrate["reporting"], "group": names.tag2group(tag, 'calibrate')}

    batchdir = names.tag2batchdir(tag, rootdir=rootdir)
    nicknames = config.classifiers

    exclude_in_train = []
    exclude_in_else = []
    tempdirs = []
    temppaths = []

    #---------------------------------------------
    ### use num_bins to partition time into separate stretches
    #---------------------------------------------

    if num_bins > 1: ### automatically divide up segments for cross validation
        if causal:
            batch_logger.info('causal round-robin training with %d bins within [%.3f, %.3f)'%(num_bins, gps_start, gps_end))
        else:
            batch_logger.info('round-robin training with %d bins and %d segments per bin within [%.3f, %.3f)'%(num_bins, num_segs_per_bin, gps_start, gps_end))

        # we check to make sure that all the segdb decisions are consistent between train, evaluate, and timeseries jobs
        train_ignore_segdb = config.train.get("ignore_segdb", False)
        evaluate_ignore_segdb = config.evaluate.get("ignore_segdb", False)
        ignore_segdb = [train_ignore_segdb, evaluate_ignore_segdb]
        if not skip_timeseries:
            ignore_segdb.append(config.timeseries.get("ignore_segdb", False))
        if not skip_report:
            ignore_segdb.append(config.report.get("ignore_segdb", False))

        if all(ignore_segdb) != any(ignore_segdb):
            raise exceptions.ConfigurationInconsistency('we must supply the same ignore_segdb option within all data discovery sections for batch jobs with num_bins>1')

        ### grab segments
        segs = load_record_segments(
            gps_start,
            gps_end,
            config.train,
            config.segments,
            batch_logger
        )

        if causal:
            batch_logger.info("processing initial lookback for causal workflow")
            initial_segs = load_record_segments(
                gps_start-initial_lookback,
                gps_start,
                config.train,
                config.segments,
                batch_logger
            )
        else:
            initial_segs = segmentlist([])

        livetime = utils.livetime(initial_segs) + utils.livetime(segs)

        ### dsplit up the data into bins
        batch_logger.info('dividing bins equally based on livetime:')
        if causal:
            folds = utils.segments_causal_kfold(segs, num_bins, initial_segs=initial_segs)
            gps_start -= initial_lookback ### include this in the call to _batch
        else:
            folds = utils.segments_checkerboard(segs, num_bins, num_segs_per_bin)
        segs = utils.segments_union(segs, initial_segs) ### NOTE: we only take the union AFTER we divide up the analysis time into folds
                                                        ### this is done to simplify the logic in the following loop, which defines segments
                                                        ### that should be excluded based on "segs", which therefore must include "initial_segs"
                                                        ### for causal training

        # assign data into bins
        for i, (trainsegs, elsesegs) in enumerate(folds, 1):
            ### format batch, exclude segments
            ### we do this backflip to make sure we only pick up the right parts for causal batch jobs
            exclude_in_train.append( utils.remove_segments(segs, trainsegs) ) ### exclude everything that is not in trainsegs
            exclude_in_else.append( utils.remove_segments(segs, elsesegs) )   ### exclude everything that is not in elsesegs
            batch_logger.info('    bin %d -- train livetime: %.3f sec, else livetime: %.3f sec'%\
                (i, livetime-utils.livetime(exclude_in_train[-1]), livetime-utils.livetime(exclude_in_else[-1])))

            ### set up temporary paths
            tempdir = os.path.join(batchdir, 'train_%d-%d-%d'%(i, gps_start, gps_end))
            if not os.path.exists(tempdir):
                os.makedirs(tempdir)
            temppath = os.path.join(tempdir, 'train_%d-%d-%d.toml'%(i, gps_start, gps_end))

            batch_logger.debug('writing temporary config to: '+temppath)
            config.dump(temppath, rootdir=tempdir)

            # record stuff
            tempdirs.append(tempdir)
            temppaths.append(temppath)

    else: # num_bins == 1, so train and evaluate on the same stretch
        bin_segment = segment(gps_start, gps_end)
        segs = segmentlist([bin_segment])
        batch_logger.info('training with 1 bin within [%.3f, %.3f)'%bin_segment)
        batch_logger.warn('You are training and evaluating with a single bin. Be careful! This can produce artificially inflated performance estimates.')

        # record stuff
        exclude_in_train.append(segmentlist([]))
        exclude_in_else.append(segmentlist([]))
        tempdirs.append(rootdir)
        temppaths.append(config_path)

    #---

    if batch_workflow=='block':
        batch_logger.info('processing segments sequentially')

        for eit, eie, tempdir, temppath in zip(exclude_in_train, exclude_in_else, tempdirs, temppaths):
            _batch(
                gps_start,
                gps_end,
                segs,
                eit,
                eie,
                tempdir,
                temppath,
                rootdir,
                config_path,
                tag,
                trainreporter_kwargs,
                evaluatereporter_kwargs,
                calibratereporter_kwargs,
                nicknames,
                verbose=verbose,
                skip_timeseries=skip_timeseries,
                skip_report=skip_report,
            )

    #---

    elif batch_workflow=='fork':
        batch_logger.info( 'processing segments in parallel via multiprocessing' )
        workers = []

        try:
            workers = []
            kwargs = {'verbose':verbose, 'skip_timeseries':skip_timeseries, 'skip_report':skip_report}
            for eit, eie, tempdir, temppath in zip(exclude_in_train, exclude_in_else, tempdirs, temppaths):
                args = (
                    gps_start,
                    gps_end,
                    segs,
                    eit,
                    eie,
                    tempdir,
                    temppath,
                    rootdir,
                    config_path,
                    tag,
                    trainreporter_kwargs,
                    evaluatereporter_kwargs,
                    calibratereporter_kwargs,
                    nicknames,
                )
                proc = mp.Process(target=_batch, args=args, kwargs=kwargs)
                proc.start()
                workers.append( proc )

            ### monitor forked jobs, cleaning up as they finish
            if persist:
                manager = utils.CadenceManager(utils.current_gps_time(), monitoring_cadence, delay=0)
                while workers:
                    proc = workers.pop()
                    if proc.is_alive(): ### still going
                        workers.append( proc ) ### add it back in

                    else: ### process is done
                        if proc.exitcode: ### something went wrong with this process!
                            raise RuntimeError('non-zero returncode for %s (pid=%d)'%(proc.name, proc.pid) )

                        proc.terminate() ### make sure we clean up the process

                    manager.wait()

        except Exception as e:
            while workers:
                proc = workers.pop()
                proc.terminate()
            raise e

    #---

    elif batch_workflow=='condor':
        batch_logger.info( 'processing segments in parallel via condor' )

        diskreporter = PickleReporter(batchdir, gps_start, gps_end)
        batchnames = []
        kwargs = {'verbose':verbose, 'skip_timeseries':skip_timeseries, 'skip_report':skip_report}
        for ind, (eit, eie, tempdir, temppath) in enumerate(zip(exclude_in_train, exclude_in_else, tempdirs, temppaths)):
            ### prepare package for idq-condor_batch
            args = (
                gps_start,
                gps_end,
                segs,
                eit,
                eie,
                tempdir,
                temppath,
                rootdir,
                config_path,
                tag,
                trainreporter_kwargs,
                evaluatereporter_kwargs,
                calibratereporter_kwargs,
                nicknames,
            )
            # write to disk
            batchname = names.bin2batchname(ind)
            path = diskreporter.report(names.nickname2condorname(batchname), (args, kwargs))
            batch_logger.debug('blob written to: '+path)
            batchnames.append(batchname)

        dag_path = condor.batch_dag(
            gps_start,
            gps_end,
            batchdir,
            batchnames,
            **config.condor
        )
        batch_logger.info( 'dag written to '+dag_path )

        exitcode = condor.submit_dag(dag_path, block=block)
        if exitcode:
            raise RuntimeError('non-zero returncode for dag '+dag_path )

        if persist:
            raise NotImplementedError('we currently cannot monitor condor for completion of batch jobs...')

    else:
        raise ValueError('batch_workflow=%s not understood'%batch_workflow)


def condor_train(gps_start, gps_end, traindir, nickname, verbose=False):
    '''
    run the training process
    '''
    ### FIXME: record progress in a logger somewhere!

    ### read in the package prepared by idq-train or idq-straming_train
    diskreporter = PickleReporter(traindir, gps_start, gps_end)
    blob = diskreporter.retrieve(names.nickname2condorname(nickname))

    ### extract the objects we need
    classifier = blob['classifier']  ### FIXME don't hard code these names, but look them up in names.py?
    args = diskreporter.retrieve(blob['args_nickname'])
    reporter = diskreporter.retrieve(blob['reporter_nickname'])

    ### execute training
    model = classifier.train(*args)

    ### report the model
    reporter.start, reporter.end = model.start, model.end ### update this to match the model's provenance
    reporter.report(names.nickname2train_topic(classifier.nickname), model, preferred=True)


def _train_and_report(conn, classifier, reporter, *args):
    """
    a helper function for forked workflow via multiprocessing
    """
    model = classifier.train(*args)
    reporter.start, reporter.end = model.start, model.end ### update this to match the model's provenance
    path = reporter.report(names.nickname2train_topic(classifier.nickname), model, preferred=True) ### report the final result
    conn.send(path)


def train(gps_start, gps_end, config_path, verbose=False, segments=None, exclude=None):
    '''
    run the training process
    '''
    ### read in config
    config_path = os.path.abspath(os.path.realpath(config_path))
    config = configparser.path2config(config_path)

    ### define factories to generate objects
    reporter_factory = factories.ReporterFactory()
    classifier_factory = factories.ClassifierFactory()

    #-----------------------------------------
    ### extract common parameters
    #-----------------------------------------
    tag = config.tag

    rootdir = os.path.abspath(os.path.realpath(config.rootdir))
    traindir = names.tag2traindir(tag, rootdir) ### used to store results
    logdir = names.tag2logdir(tag, rootdir=rootdir)

    for directory in [rootdir, traindir, logdir]:
        if not os.path.exists(directory):
            os.makedirs(directory)

    #-----------------------------------------
    ### set up logging
    #-----------------------------------------
    logs.configure_logger(
        logger,
        names.tag2train_logname(tag),
        log_level=config.train['log_level'],
        rootdir=logdir,
        verbose=verbose
    )
    logger.info( "using config : %s"%config_path )
    logger.info("writing logs to : %s"%logdir)

    #-----------------------------------------
    ### set up how we record results
    #-----------------------------------------
    trainreporter = reporter_factory(traindir, gps_start, gps_end, group=names.tag2group(tag, 'train'), **config.train["reporting"])

    ### other reporters used to record intermediate data products
    picklereporter = PickleReporter(traindir, gps_start, gps_end)
    segmentreporter = reporter_factory(traindir, gps_start, gps_end, flavor="segment")
    gpstimesreporter = reporter_factory(traindir, gps_start, gps_end, flavor="span")

    #-----------------------------------------
    ### set a random seed if provided
    #-----------------------------------------
    seed = config.samples.get('random_seed', None)
    if seed is not None:
        logger.info('setting random seed: %d'%seed )
        np.random.seed(seed)

    #-----------------------------------------
    ### figure out which classifiers we'll run
    #-----------------------------------------
    logger.info('using classifiers: %s' % ', '.join(config.classifiers))
    klassifiers = {} ### funny spelling to avoid conflict with module named "classifiers"
    for nickname in config.classifiers:
        items = config.classifier_map[nickname]
        logger.debug( nickname +' -> ' + ' '.join('%s:%s'%(k, v) for k, v in items.items()))

        ### instantiate the object and add it to the list
        klassifiers[nickname] = classifier_factory(nickname, rootdir=traindir, **items)

    #-----------------------------------------
    ### figure out how we'll read data
    #-----------------------------------------

    ### grab segments
    segs = load_record_segments(
        gps_start,
        gps_end,
        config.train,
        config.segments,
        logger,
        segments=segments,
        exclude=exclude,
        reporter=segmentreporter
    )

    ### grab data
    dataloader, time = load_record_dataloader(
        gps_start,
        gps_end,
        segs,
        config.features,
        logger,
        reporter=picklereporter,
        group=names.tag2group(tag, 'train')
    )

    ### define truth labels for samples
    target_times, random_times, clean_segs = find_record_times(
        dataloader,
        time,
        config.train['random_rate'],
        config.samples,
        logger,
        timesreporter=gpstimesreporter,
        segmentreporter=segmentreporter
    )

    # generate the dataset
    logger.info( 'building dataset' )
    dataset = factories.DatasetFactory(dataloader).labeled(target_times, random_times)

    # record dataset
    datasetreporter = reporter_factory(traindir, gps_start, gps_end, flavor="dataset")
    path = datasetreporter.report('dataset', dataset)
    logger.debug( 'dataset written to '+path )

    #-----------------------------------------
    ### actually run jobs

    workflow = config.train['workflow']

    ### run in series
    if workflow == 'block':
        logger.info( 'training classifiers sequentially' )

        for nickname, classifier in klassifiers.items():
            logger.info( 'training %s with dataset'%nickname )
            model = classifier.train(dataset)

            trainreporter.start, trainreporter.end = model.start, model.end ### update this to match the model's provenance
            logger.info('reporting model')
            path = trainreporter.report(names.nickname2train_topic(nickname), model, preferred=True) ### report the resulting model
            logger.debug( 'model for %s written to %s'%(nickname, path) )

    ### run in parallel on the same node
    elif workflow == 'fork':
        logger.info( 'training classifiers in parallel via multiprocessing' )

        ### set up args to be passed for all the jobs
        train_args = {}
        for nickname, classifier in klassifiers.items():
            train_args[nickname] = (classifier, trainreporter, dataset)

        monitoring_cadence = config.train.get('monitoring_cadence', utils.DEFAULT_MONITORING_CADENCE)
        start_message = 'training %s'
        end_message = 'model for %s written to %s'

        ### monitor forked jobs, cleaning up as they finish
        _fork_and_monitor(_train_and_report, train_args, logger, start_message, end_message, monitoring_cadence)

    ### submit to condor
    elif workflow == 'condor':
        logger.info( 'training classifiers in parallel via condor' )

        ### set up blob which we'll write to disk for each classifier
        blob = {
            'classifier':None,
            'args_nickname':None,
            'reporter_nickname':None,
        }

        ### write reporter to disk
        blob['reporter_nickname'] = 'reporter' ### FIXME: don't hard-code nickname? look it up in names.py?
        path = picklereporter.report(blob['reporter_nickname'], trainreporter)
        logger.debug( 'reporter written to '+path )

        ### write dataset to disk
        blob['args_nickname'] = 'trainWithDataset' ### FIXME: don't hard-code nickname? look it up in names.py?
        path = picklereporter.report(blob['args_nickname'], [dataset])
        logger.debug( 'dataset written to '+path )

        ### iterate through classifiers, writing a blob for each to disk
        for nickname, classifier in klassifiers.items():
            blob['classifier'] = classifier
            path = picklereporter.report(names.nickname2condorname(nickname), blob)
            logger.debug( 'blob for %s written to %s'%(nickname, path) )

        ### define sub, dag files
        dag_path = condor.train_dag(
            gps_start,
            gps_end,
            traindir,
            config.classifiers,
            **config.condor
        )
        logger.info( 'dag written to '+dag_path )

        exitcode = condor.submit_dag(dag_path, block=True) ### blocks until this is done
        if exitcode:
            raise RuntimeError('non-zero returncode for dag '+dag_path )

    else:
        raise ValueError('workflow=%s not understood'%(workflow) )


def condor_evaluate(gps_start, gps_end, evaluatedir, nickname, verbose=False):
    '''
    calibrate the classifier predictions based on historical data
    '''
    ### FIXME: record progress in a logger somewhere!

    ### read in the package prepared by idq-evaluate or idq-streaming_evaluate
    diskreporter = PickleReporter(evaluatedir, gps_start, gps_end)
    blob = diskreporter.retrieve(names.nickname2condorname(nickname))

    ### extract the objects we need
    classifier = blob['classifier']  ### FIXME don't hard code these names, but look them up in names.py?
    args = diskreporter.retrieve(blob['args_nickname'])
    reporter = diskreporter.retrieve(blob['reporter_nickname'])

    ### evaluate labeled dataset for determining classifier performance
    dataset = classifier.evaluate(*args)

    ### report
    reporter.start, reporter.end = dataset.start, dataset.end ### manage provanance
    path = reporter.report(names.nickname2evaluate_topic(classifier.nickname), dataset, preferred=True)


def _evaluate_and_report(conn, classifier, reporter, *args):
    '''
    a helper function for forked workflow via multiprocessing
    '''
    dataset = classifier.evaluate(*args)
    reporter.start, reporter.end = dataset.start, dataset.end ### manage provenance
    path = reporter.report(names.nickname2evaluate_topic(classifier.nickname), dataset, preferred=True)
    conn.send(path)


def evaluate(gps_start, gps_end, config_path, verbose=False, segments=None, exclude=None, preferred=False):
    '''
    evaluate the data using classifiers
    '''
    ### read in config
    config_path = os.path.abspath(os.path.realpath(config_path))
    config = configparser.path2config(config_path)

    ### define factories to generate objects
    reporter_factory = factories.ReporterFactory()
    classifier_factory = factories.ClassifierFactory()

    #-----------------------------------------
    ### extract common parameters
    #-----------------------------------------
    tag = config.tag

    rootdir = os.path.abspath(os.path.realpath(config.rootdir))
    evaluatedir = names.tag2evaluatedir(tag, rootdir=rootdir)
    traindir = names.tag2traindir(tag, rootdir=rootdir)
    logdir = names.tag2logdir(tag, rootdir=rootdir)

    for directory in [rootdir, traindir, evaluatedir, logdir]:
        if not os.path.exists(directory):
            os.makedirs(directory)

    #-----------------------------------------
    ### set up logging
    #-----------------------------------------
    logs.configure_logger(
        logger,
        names.tag2evaluate_logname(tag),
        log_level=config.evaluate['log_level'],
        rootdir=logdir,
        verbose=verbose,
    )
    logger.info( "using config : %s"%config_path )
    logger.info("writing logs to : %s"%logdir)

    #-----------------------------------------
    ### set up how we record results
    #-----------------------------------------
    evaluatereporter = reporter_factory(evaluatedir, gps_start, gps_end, group=names.tag2group(tag, 'evaluate'), **config.evaluate["reporting"])
    trainreporter = reporter_factory(traindir, gps_start, gps_end, group=names.tag2group(tag, 'evaluate'), **config.train["reporting"])

    ### other reporters used to record intermediate data products
    picklereporter = PickleReporter(evaluatedir, gps_start, gps_end)
    segmentreporter = reporter_factory(evaluatedir, gps_start, gps_end, flavor="segment")
    gpstimesreporter = reporter_factory(evaluatedir, gps_start, gps_end, flavor="span")
    datasetreporter = reporter_factory(evaluatedir, gps_start, gps_end, flavor="dataset")

    #-----------------------------------------
    ### set a random seed if provided
    #-----------------------------------------
    seed = config.samples.get('random_seed', None)
    if seed is not None:
        logger.info('setting random seed: %d'%seed )
        np.random.seed(seed)

    #-----------------------------------------
    ### figure out which classifiers we'll run
    #-----------------------------------------
    logger.info('using classifiers: %s' % ', '.join(config.classifiers))
    klassifiers = {} ### funny spelling to avoid conflict with module named "classifiers"
    for nickname in config.classifiers:
        items = config.classifier_map[nickname]
        logger.debug( nickname +' -> ' + ' '.join('%s:%s'%(k, v) for k, v in items.items()))

        ### actuall instantiate the object and add it to the list
        klassifiers[nickname] = classifier_factory(nickname, rootdir=evaluatedir, **items)

    #-----------------------------------------
    ### figure out how we'll read data
    #-----------------------------------------

    ### grab segments
    segs = load_record_segments(
        gps_start,
        gps_end,
        config.evaluate,
        config.segments,
        logger,
        segments=segments,
        exclude=exclude,
        reporter=segmentreporter
    )

    ### grab data
    dataloader, time = load_record_dataloader(
        gps_start,
        gps_end,
        segs,
        config.features,
        logger,
        reporter=picklereporter,
        group=names.tag2group(tag, 'evaluate')
    )

    #-----------------------------------------
    ### generate dataset for evaluation
    #-----------------------------------------

    ### define truth labels for samples
    target_times, random_times, clean_segs = find_record_times(
        dataloader,
        time,
        config.evaluate['random_rate'],
        config.samples,
        logger,
        timesreporter=gpstimesreporter,
        segmentreporter=segmentreporter
    )

    ### genrate the dataset
    logger.info( 'building dataset' )
    dataset = factories.DatasetFactory(dataloader).labeled(target_times, random_times)

    # record dataset
    path = datasetreporter.report('dataset', dataset)
    logger.debug( 'dataset written to '+path )

    #-----------------------------------------
    ### retrieve trained models
    for nickname, classifier in klassifiers.items():
        logger.info( 'retrieving model for '+nickname )
        classifier.model = trainreporter.retrieve(names.nickname2train_topic(nickname), preferred=preferred)

    #-----------------------------------------
    ### actually run jobs

    workflow = config.evaluate['workflow']
    if workflow == 'block':
        logger.info( 'evaluating dataset with trained classifiers sequentially' )

        for nickname, classifier in klassifiers.items():

            ### evaluate dataset and save probabilities to disk
            logger.info( 'evaluating dataset with %s'%nickname )
            dataset = classifier.evaluate(dataset)

            evaluatereporter.start, evaluatereporter.end = dataset.start, dataset.end ### manage provenance
            logger.info('reporting dataset')
            path = evaluatereporter.report(names.nickname2evaluate_topic(nickname), dataset, preferred=True)
            logger.debug( 'dataset written to %s'%path )

    elif workflow == 'fork':
        logger.info( 'evaluating dataset in parallel via multiprocessing' )

        ### set up args to be passed for all the jobs
        evaluate_args = {}
        for nickname, classifier in klassifiers.items():
            evaluate_args[nickname] = (classifier, evaluatereporter, dataset)

        monitoring_cadence = config.evaluate.get('monitoring_cadence', utils.DEFAULT_MONITORING_CADENCE)
        start_message = 'evaluating dataset with %s'
        end_message = 'evaluated dataset for %s written to %s'

        ### monitor forked jobs, cleaning up as they finish
        _fork_and_monitor(_evaluate_and_report, evaluate_args, logger, start_message, end_message, monitoring_cadence)

    elif workflow == 'condor':
        logger.info( 'evaluating dataset in parallel via condor' )

        ### set up blob which we'll write to disk for each classifier
        blob = {
            'classifier':None,
            'args_nickname':None,
            'reporter_nickname':None,
        }

        ### write reporter to disk
        blob['reporter_nickname'] = 'reporter' ### FIXME: don't hard-code nickname? look it up in names.py?
        path = picklereporter.report(blob['reporter_nickname'], evaluatereporter)
        logger.debug( 'reporter written to '+path )

        ### iterate through classifiers, writing a blob for each to disk
        blob['args_nickname'] = 'evaluate' ### FIXME: don't hard-code nickname? look it up in names.py?
        path = picklereporter.report(blob['args_nickname'], [dataset])
        logger.debug( 'arguments written to '+path )

        ### write blobs for each classifier to disk
        for nickname, classifier in klassifiers.items():
            nickname = classifier.nickname
            blob['classifier'] = classifier
            path = picklereporter.report(names.nickname2condorname(nickname), blob)
            logger.debug( 'blob for %s written to %s'%(nickname, path) )

        ### define sub, dag files
        dag_path = condor.evaluate_dag(
            gps_start,
            gps_end,
            evaluatedir,
            config.classifiers,
            **config.condor
        )
        logger.info( 'dag written to '+dag_path )

        exitcode = condor.submit_dag(dag_path, block=True) ### blocks until this is done
        if exitcode:
            raise RuntimeError('non-zero returncode for dag '+dag_path )

    else:
        raise ValueError('workflow=%s not understood'%(workflow) )


def condor_calibrate(gps_start, gps_end, calibratedir, nickname, verbose=False):
    '''
    calibrate the classifier predictions based on historical data
    '''
    ### FIXME: record progress in a logger somewhere!

    ### read in the package prepared by idq-calibrate
    diskreporter = PickleReporter(calibratedir, gps_start, gps_end)
    blob = diskreporter.retrieve(names.nickname2condorname(nickname))

    ### extract the objects we need
    classifier = blob['classifier']  ### FIXME don't hard code these names, but look them up in names.py?
    args = diskreporter.retrieve(blob['args_nickname'])
    calibrate_kwargs = args[-1]
    args = args[:-1]
    reporter = diskreporter.retrieve(blob['reporter_nickname'])

    ### evaluate dataset and save probabilities to disk
    calibmap = classifier.calibrate(*args, **calibrate_kwargs)

    ### report
    reporter.start, reporter.end = calibmap.start, calibmap.end ### manage provenance
    path = reporter.report(names.nickname2calibrate_topic(nickname), calibmap, preferred=True)


def _calibrate_and_report(conn, classifier, reporter, *args):
    '''
    a helper function for forked workflow via multiprocessing
    '''
    calibrate_kwargs = args[-1]
    args = args[:-1]
    calibmap = classifier.calibrate(*args, **calibrate_kwargs)
    reporter.start, reporter.end = calibmap.start, calibmap.end ### manage provenance
    path = reporter.report(names.nickname2calibrate_topic(classifier.nickname), calibmap, preferred=True) ### report the final result
    conn.send(path)


def calibrate(gps_start, gps_end, config_path, verbose=False, preferred=False):
    '''
    calibrate the classifier predictions based on historical data
    '''
    ### read in config
    config_path = os.path.abspath(os.path.realpath(config_path))
    config = configparser.path2config(config_path)

    ### define factories to generate objects
    reporter_factory = factories.ReporterFactory()
    classifier_factory = factories.ClassifierFactory()

    #-----------------------------------------
    ### extract common parameters
    #-----------------------------------------
    tag = config.tag

    rootdir = os.path.abspath(os.path.realpath(config.rootdir))
    evaluatedir = names.tag2evaluatedir(tag, rootdir=rootdir)
    calibratedir = names.tag2calibratedir(tag, rootdir=rootdir)
    logdir = names.tag2logdir(tag, rootdir=rootdir)

    for directory in [rootdir, evaluatedir, calibratedir, logdir]:
        if not os.path.exists(directory):
            os.makedirs(directory)

    #-----------------------------------------
    ### set up logging
    #-----------------------------------------
    logs.configure_logger(
        logger,
        names.tag2calibrate_logname(tag),
        log_level=config.calibrate['log_level'],
        rootdir=logdir,
        verbose=verbose,
    )
    logger.info( "using config : %s"%config_path )
    logger.info("writing logs to : %s"%logdir)

    #-----------------------------------------
    ### set up how we record results
    #-----------------------------------------
    evaluatereporter = reporter_factory(evaluatedir, gps_start, gps_end, group=names.tag2group(tag, 'calibrate'), **config.evaluate["reporting"])
    calibratereporter = reporter_factory(calibratedir, gps_start, gps_end, group=names.tag2group(tag, 'calibrate'), **config.calibrate["reporting"])

    ### other reporters used to record intermediate data products
    picklereporter = PickleReporter(calibratedir, gps_start, gps_end)
    segmentreporter = reporter_factory(calibratedir, gps_start, gps_end, flavor="segment")
    cleansegsreporter = reporter_factory(evaluatedir, gps_start, gps_end, flavor="segment")

    #-----------------------------------------
    ### set a random seed if provided
    #-----------------------------------------
    seed = config.samples.get('random_seed', None)
    if seed is not None:
        logger.info('setting random seed: %d'%seed )
        np.random.seed(seed)

    #-----------------------------------------
    ### figure out which classifiers we'll run
    #-----------------------------------------
    logger.info('using classifiers: %s' % ', '.join(config.classifiers))
    klassifiers = {} ### funny spelling to avoid conflict with module named "classifiers"
    for nickname in config.classifiers:
        items = config.classifier_map[nickname]
        logger.debug( nickname +' -> ' + ' '.join('%s:%s'%(k, v) for (k, v) in items.items()))

        ### instantiate the object and add it to the list
        klassifiers[nickname] = classifier_factory(nickname, rootdir=calibratedir, **items)

    #-----------------------------------------
    # extract basic parameters for calibration
    #-----------------------------------------
    calibrate_kwargs = config.calibrate

    #-----------------------------------------
    ### actually run jobs

    workflow = config.calibrate['workflow']

    if workflow == 'block':
        logger.info( 'calibrating classifiers with evaluated datasets sequentially' )

        for nickname, classifier in klassifiers.items():

            logger.info('retrieving evaluated dataset')
            dataset = evaluatereporter.retrieve(names.nickname2evaluate_topic(nickname), preferred=preferred)

            logger.info('retrieving clean segments')
            clean_segs = cleansegsreporter.retrieve('cleanSegments', preferred=preferred)

            ### evaluate dataset and save probabilities to disk
            logger.info( 'calibrating evaluations with %s'%nickname )
            calibmap = classifier.calibrate(dataset, clean_segs=clean_segs, **calibrate_kwargs)

            calibratereporter.start, calibratereporter.end = calibmap.start, calibmap.end ### manage provenance
            logger.info('reporting calibration')
            path = calibratereporter.report(names.nickname2calibrate_topic(nickname), calibmap, preferred=True)
            logger.debug( 'calibration written to %s'%path )

    elif workflow == 'fork':
        logger.info( 'calibrating in parallel via multiprocessing' )

        ### set up args to be passed for all the jobs
        calibrate_args = {}
        for nickname, classifier in klassifiers.items():

            logger.info('retrieving evaluated dataset')
            dataset = evaluatereporter.retrieve(names.nickname2evaluate_topic(nickname), preferred=preferred)

            logger.info('retrieving clean segments')
            clean_segs = cleansegsreporter.retrieve('cleanSegments', preferred=preferred)

            all_kwargs = {'clean_segs': clean_segs}
            all_kwargs.update(calibrate_kwargs)
            calibrate_args[nickname] = (classifier, calibratereporter, dataset, all_kwargs)

        monitoring_cadence = config.calibrate.get('monitoring_cadence', utils.DEFAULT_MONITORING_CADENCE)
        start_message = 'calibrating evaluations with %s'
        end_message = 'calibration for %s written to %s'

        ### monitor forked jobs, cleaning up as they finish
        _fork_and_monitor(_calibrate_and_report, calibrate_args, logger, start_message, end_message, monitoring_cadence)

    elif workflow == 'condor':
        logger.info( 'calibrating in parallel via condor' )

        ### set up blob which we'll write to disk for each classifier
        blob = {
            'classifier':None,
            'args_nickname':None,
            'reporter_nickname':None,
        }

        ### write reporter to disk
        blob['reporter_nickname'] = 'reporter' ### FIXME: don't hard-code nickname? look it up in names.py?
        path = picklereporter.report(blob['reporter_nickname'], calibratereporter)
        logger.debug( 'reporter written to '+path )


        ### write blobs for each classifier to disk
        for nickname, classifier in klassifiers.items():
            nickname = classifier.nickname

            logger.info('retrieving evaluated dataset')
            dataset = evaluatereporter.retrieve(names.nickname2evaluate_topic(nickname))

            logger.info('retrieving clean segments')
            clean_segs = cleansegsreporter.retrieve('cleanSegments', preferred=preferred)

            ### iterate through classifiers, writing a blob for each to disk
            blob['args_nickname'] = 'calibrate' ### FIXME: don't hard-code nickname? look it up in names.py?
            all_kwargs = {'clean_segs': clean_segs}
            all_kwargs.update(calibrate_kwargs)
            path = picklereporter.report(blob['args_nickname'], [dataset, all_kwargs])
            logger.debug( 'arguments written to '+path )

            blob['classifier'] = classifier
            path = picklereporter.report(names.nickname2condorname(nickname), blob)
            logger.debug( 'blob for %s written to %s'%(nickname, path) )

        ### define sub, dag files
        dag_path = condor.calibrate_dag(
            gps_start,
            gps_end,
            calibratedir,
            config.classifiers,
            **config.condor
        )
        logger.info( 'dag written to '+dag_path )

        exitcode = condor.submit_dag(dag_path, block=True) ### blocks until this is done
        if exitcode:
            raise RuntimeError('non-zero returncode for dag '+dag_path )

    else:
        raise ValueError('workflow=%s not understood'%(workflow) )


def report_timeseries(reporter, nickname, seriesdicts, preferred=False):
    paths = []
    for s in seriesdicts:
        reporter.start = s['t0']
        reporter.end = s['t0'] + s['deltaT']*len(list(s['data'].values())[0]) ### just grab one series
        paths.append(reporter.report(nickname, s, preferred=preferred))
    return paths


def condor_timeseries(gps_start, gps_end, timeseriesdir, nickname, verbose=False):
    '''
    calibrate the classifier predictions based on historical data
    '''
    ### FIXME: record progress in a logger somewhere!

    ### read in the package prepared by idq-calibrate
    diskreporter = PickleReporter(timeseriesdir, gps_start, gps_end)
    blob = diskreporter.retrieve(names.nickname2condorname(nickname))

    ### extract the objects we need
    classifier = blob['classifier']  ### FIXME don't hard code these names, but look them up in names.py?
    instrument, args, template = diskreporter.retrieve(blob['args_nickname'])
    reporter = diskreporter.retrieve(blob['reporter_nickname'])

    ### generate timeseries
    series = classifier.timeseries(*args)
    ans = calibration.series2dict(series, classifier.model, classifier.calibration_map, template)

    ### report
    report_timeseries(reporter, names.nickname2timeseries_topic(instrument, nickname), ans, preferred=True)


def _timeseries_and_report(conn, classifier, reporter, instrument, args, template):
    '''
    a helper function for forked workflow via multiprocessing
    '''
    series = classifier.timeseries(*args)
    ans = calibration.series2dict(series, classifier.model, classifier.calibration_map, template)
    paths = report_timeseries(reporter, names.nickname2timeseries_topic(instrument, classifier.nickname), ans, preferred=True)
    conn.send(paths)


def timeseries(gps_start, gps_end, config_path, verbose=False, segments=None, exclude=None, preferred=False):
    '''
    evaluate the data using classifiers
    '''
    ### read in config
    config_path = os.path.abspath(os.path.realpath(config_path))
    config = configparser.path2config(config_path)

    ### define factories to generate objects
    reporter_factory = factories.ReporterFactory()
    classifier_factory = factories.ClassifierFactory()

    #-----------------------------------------
    ### extract common parameters
    #-----------------------------------------
    tag = config.tag

    rootdir = os.path.abspath(os.path.realpath(config.rootdir))
    timeseriesdir = names.tag2timeseriesdir(tag, rootdir=rootdir)
    calibratedir = names.tag2calibratedir(tag, rootdir=rootdir)
    traindir = names.tag2traindir(tag, rootdir=rootdir)
    logdir = names.tag2logdir(tag, rootdir=rootdir)

    for directory in [rootdir, traindir, calibratedir, timeseriesdir, logdir]:
        if not os.path.exists(directory):
            os.makedirs(directory)

    ### extract information necessary for channel names in timeseries files
    instrument = config.instrument

    target_bounds = configparser.config2bounds(config.samples['target_bounds'])
    frequency = config.features['frequency']
    assert frequency in target_bounds, 'must specify a frequency range (called %s) within target_bounds'%frequency
    freq_min, freq_max = target_bounds[frequency]
    assert isinstance(freq_min, int) and isinstance(freq_max, int), 'frequency bounds must be integers!'

    #-----------------------------------------
    ### set up logging
    #-----------------------------------------
    logs.configure_logger(
        logger,
        names.tag2timeseries_logname(tag),
        log_level=config.timeseries['log_level'],
        rootdir=logdir,
        verbose=verbose,
    )
    logger.info( "using config : %s"%config_path )
    logger.info("writing logs to : %s"%logdir)

    #-----------------------------------------
    ### set up how we record results
    #-----------------------------------------
    trainreporter = reporter_factory(traindir, gps_start, gps_end, group=names.tag2group(tag, 'timeseries'), **config.train["reporting"])
    calibratereporter = reporter_factory(calibratedir, gps_start, gps_end, group=names.tag2group(tag, 'timeseries'), **config.calibrate["reporting"])
    timeseriesreporter = reporter_factory(timeseriesdir, gps_start, gps_end, group=names.tag2group(tag, 'timeseries'), **config.timeseries["reporting"])

    ### other reporters used to record intermediate data products
    picklereporter = PickleReporter(timeseriesdir, gps_start, gps_end)
    segmentreporter = reporter_factory(timeseriesdir, gps_start, gps_end, flavor="segment")

    #-----------------------------------------
    ### set a random seed if provided
    #-----------------------------------------
    seed = config.samples.get('random_seed', None)
    if seed is not None:
        logger.info('setting random seed: %d'%seed )
        np.random.seed(seed)

    #-----------------------------------------
    ### figure out which classifiers we'll run
    #-----------------------------------------
    logger.info('using classifiers: %s' % ', '.join(config.classifiers))
    klassifiers = {} ### funny spelling to avoid conflict with module named "classifiers"
    for nickname in config.classifiers:
        items = config.classifier_map[nickname]
        logger.debug( nickname +' -> ' + ' '.join('%s:%s'%(k, v) for k, v in items.items()))

        ### actuall instantiate the object and add it to the list
        klassifiers[nickname] = classifier_factory(nickname, rootdir=timeseriesdir, **items)

    #-----------------------------------------
    ### figure out how we'll read data
    #-----------------------------------------

    ### grab segments
    segs = load_record_segments(
        gps_start,
        gps_end,
        config.timeseries,
        config.segments,
        logger,
        segments=segments,
        exclude=exclude,
        reporter=segmentreporter
    )

    ### grab data
    dataloader, _ = load_record_dataloader(
        gps_start,
        gps_end,
        segs,
        config.features,
        logger,
        reporter=picklereporter,
        group=names.tag2group(tag, 'timeseries')
    )
    dataset_factory = factories.DatasetFactory(dataloader)

    ### figure out timeseres sampling rate -> dt
    srate = config.timeseries['srate']
    logger.info('generating timeseries sampled at %.3f Hz'%srate)
    dt = 1./srate ### we pass dt to classifier.timeseries so we only have to do division once. Saves, like, a handful of cycles

    #-----------------------------------------
    ### load in model and calibration map
    for nickname, classifier in klassifiers.items():
        ### set model
        logger.info('retrieving %s classifier'%nickname)
        classifier.model = trainreporter.retrieve(names.nickname2train_topic(nickname), preferred=preferred)

        ### set calibration map
        logger.info('retrieving %s calibration_map'%nickname)
        classifier.calibration_map = calibratereporter.retrieve(names.nickname2calibrate_topic(nickname), preferred=preferred)

    #-----------------------------------------
    ### actually run jobs

    workflow = config.timeseries['workflow']

    ### split up timeseries generation into strides is specified
    max_stride = config.timeseries.get('stride', None)
    if max_stride is not None:
        strides = sorted(utils.split_segments_by_stride(gps_start, gps_end, segs, max_stride))
    else:
        strides = [segment(gps_start, gps_end)]

    logger.info( 'generating timeseries' )
    for stride in strides:

        ### update report times
        timeseriesreporter.start, timeseriesreporter.end = stride

        logger.info('--- timeseries stride: [%.3f, %.3f) ---'%stride )
        stride_segs = utils.segments_intersection(segmentlist([stride]), segs)
        lvtm = utils.livetime(stride_segs)

        ### create timeseries if stride livetime is nonzero
        if lvtm:

            if workflow == 'block':
                logger.info( 'generating timeseries sequentially' )
                for nickname, classifier in klassifiers.items():

                    ### generate series
                    logger.info('generating timeseries for %s'%nickname)
                    series = classifier.timeseries(dataset_factory, dt=dt, segs=stride_segs)

                    ### calibrate
                    logger.info('calibrating timeseries for %s'%nickname)
                    channel_name = names.channel_name_template(instrument, nickname, freq_min, freq_max)
                    ans = calibration.series2dict(series, classifier.model, classifier.calibration_map, channel_name)

                    ### report
                    logger.info('reporting probabilities')
                    for path in report_timeseries(timeseriesreporter, names.nickname2timeseries_topic(instrument, nickname), ans, preferred=True):
                        logger.debug( 'probabilities written to %s'%path )

            elif workflow == 'fork':
                logger.info( 'generating timeseries in parallel via multiprocessing' )

                ### set up args to be passed for all the jobs
                timeseries_args = {}
                series_args = (dataset_factory, dt, stride_segs)
                for nickname, classifier in klassifiers.items():
                    channel_name = names.channel_name_template(instrument, nickname, freq_min, freq_max)
                    timeseries_args[nickname] = (classifier, timeseriesreporter, instrument, series_args, channel_name)

                monitoring_cadence = config.evaluate.get('monitoring_cadence', utils.DEFAULT_MONITORING_CADENCE)
                start_message = 'generating timeseries for %s'
                end_message = 'timeseries for %s written to %s'

                ### monitor forked jobs, cleaning up as they finish
                _fork_and_monitor(_timeseries_and_report, timeseries_args, logger, start_message, end_message, monitoring_cadence)

            elif workflow == 'condor':
                logger.info( 'generating timeseries in parallel via condor' )

                ### set up blob which we'll write to disk for each classifier
                blob = {
                    'classifier':None,
                    'args_nickname':None,
                    'reporter_nickname':None,
                }

                ### write reporter to disk
                blob['reporter_nickname'] = 'reporter' ### FIXME: don't hard-code nickname? look it up in names.py?
                path = picklereporter.report(blob['reporter_nickname'], timeseriesreporter)
                logger.debug( 'reporter written to '+path )

                ### iterate through classifiers, writing a blob for each to disk
                blob['args_nickname'] = 'timeseries' ### FIXME: don't hard-code nickname? look it up in names.py?
                series_args = (dataset_factory, dt, stride_segs)
                channel_name = names.channel_name_template(instrument, nickname, freq_min, freq_max)
                path = picklereporter.report(blob['args_nickname'], [instrument, series_args, channel_name])
                logger.debug( 'arguments written to '+path )

                ### write blobs for each classifier to disk
                for nickname, classifier in klassifiers.items():
                    nickname = classifier.nickname
                    blob['classifier'] = classifier
                    path = picklereporter.report(names.nickname2condorname(nickname), blob)
                    logger.debug( 'blob for %s written to %s'%(nickname, path) )

                ### define sub, dag files
                dag_path = condor.timeseries_dag(
                    gps_start,
                    gps_end,
                    timeseriesdir,
                    config.classifiers,
                    **config.condor
                )
                logger.info( 'dag written to '+dag_path )

                exitcode = condor.submit_dag(dag_path, block=True) ### blocks until this is done
                if exitcode:
                    raise RuntimeError('non-zero returncode for dag '+dag_path )

            else:
                raise ValueError('workflow=%s not understood'%(workflow) )

        else:
            logger.info('no identified times, skipping timeseries')


def report(gps_start, gps_end, config_path, reportdir=None, zoom_start=None, zoom_end=None, verbose=False, segments=None, **kwargs):
    """
    generate a report of the classifier predictions in this period
    we allow reportdir to be specified directly here in case you want to make a summary of a job running in a directory you don't own
    """
    ### read in config
    config_path = os.path.abspath(os.path.realpath(config_path))
    config = configparser.path2config(config_path)

    #-----------------------------------------
    ### extract common parameters
    #-----------------------------------------
    tag = config.tag
    rootdir = os.path.abspath(os.path.realpath(config.rootdir))

    if reportdir is None:
        reportdir = names.tag2reportdir(tag, rootdir=rootdir)
        logdir = names.tag2logdir(tag, rootdir=rootdir)
    else:
        logdir = reportdir ### write log into the output directory

    nicknames = config.classifiers

    # hard code some defaults here...
    log_level = config.report['log_level']

    ignore_segdb = config.report.get('ignore_segdb', False)
    legend = config.report.get('legend', True)
    annotate_gch = config.report.get('annotate_gch', False)
    overview_only = config.report.get('overview_only', False)
    single_calib_plots = config.report.get('single_calib_plots', True)

    ### check various locations to see whether to skip timeseries
    if 'skip_timeseries' in config.report:
        skip_timeseries = config.report['skip_timeseries']
    elif 'skip_timeseries' in kwargs:
        skip_timeseries = kwargs['skip_timeseries']
    else:
        skip_timeseries = False

    #-----------------------------------------
    ### set up object and generate report
    #-----------------------------------------

    report_obj = reports.Report(config_path, gps_start, gps_end, t0=gps_start, zoom_start=zoom_start, zoom_end=zoom_end)
    report_obj.report(
        nicknames,
        reportdir=reportdir,
        logdir=logdir,
        ignore_segdb=ignore_segdb,
        skip_timeseries=skip_timeseries,
        overview_only=overview_only,
        annotate_gch_gps=annotate_gch,
        single_calib_plots=single_calib_plots,
        legend=legend,
        verbose=verbose,
        segments = segments,  
        log_level=log_level,
    )


def find_record_times(dataloader, time, random_rate, samples_config, logger, timesreporter=None, segmentreporter=None):
    """
    find truth labels (target and random times) for samples, optionally record to disk
    """
    #---
    # NOTE: if we switch to continuous weighting of samples instead of discrete labels, this whole block will need to be re-written...
    #---
    target_channel = samples_config['target_channel']
    target_bounds = configparser.config2bounds(samples_config['target_bounds'])

    dirty_bounds = configparser.config2bounds(samples_config['dirty_bounds'])
    dirty_window = samples_config['dirty_window']

    logger.info( 'identifying target times' )
    target_times = dataloader.target_times(time, target_channel, target_bounds)
    if len(target_times) == 0:
        logger.warning(
            "identified 0 target times, this may indicate data discovery issues. "
            "in addition, some classifiers do not support training without any target times."
        )
    else:
        logger.info( 'identified %d target_times'%len(target_times) )

    # record target times
    if timesreporter:
        path = timesreporter.report('targetTimes', target_times)
        logger.debug( 'target_times written to '+path )

    ### draw random times
    random_times, clean_segs = dataloader.random_times(time, target_channel, dirty_bounds, dirty_window, random_rate)
    logger.info( 'identified %d random times'%len(random_times) )

    # record random times
    if timesreporter:
        path = timesreporter.report('randomTimes', random_times)
        logger.debug( 'random_times written to '+path )

    ### record clean segments
    if segmentreporter:
        path = segmentreporter.report('cleanSegments', clean_segs)
        logger.debug( 'clean segments written to '+path )

    return target_times, random_times, clean_segs


def load_record_dataloader(start, end, segments, config, logger, reporter=None, group=None):
    """
    load classifier data and optionally write to disk
    """
    dataloader_factory = factories.DataLoaderFactory()

    # set up options
    items = config.copy()
    logger.info('using feature backend: %s' % config['flavor'])
    logger.debug( 'dataloader -> '+' '.join('%s:%s'%(k, v) for k, v in config.items()))
    time = items.pop('time') ### extract the name of the column we use as time
    if group:
        items = {**items, "group": group}

    # set up dataloader
    dataloader = dataloader_factory(start, end, segs=segments, **items)

    # record dataloader
    if reporter:
        path = reporter.report('classifierData', dataloader)
        logger.debug( 'dataloader written to '+path )

    return dataloader, time


def load_record_segments(start, end, job_config, seg_config, logger, segments=None, exclude=None, reporter=None):
    """
    load in segments and optionally write to disk
    """
    if not exclude:
        exclude = segmentlist([])

    if job_config.get('ignore_segdb', False):
        logger.info("ignoring segdb")
        segs = segmentlist([segment(start, end)])

    elif segments is not None: ### segments provided, no need to query segdb
        logger.info("segments provided ahead of time, skipping query to segdb")
        segs = segmentlist(segments)

    else:
        segdb_intersect = seg_config["intersect"].split() if "intersect" in seg_config else []
        segdb_exclude = config.segments["exclude"].split() if "exclude" in seg_config else []
        segdb_url = seg_config["segdb_url"]

        logger.info("querying %s within [%.3f, %.3f) for: intersect=%s ; exclude=%s"%(segdb_url, start, end, ','.join(segdb_intersect), ','.join(segdb_exclude)))

        segs = utils.segdb2segs(
            start,
            end,
            intersect=segdb_intersect,
            exclude=segdb_exclude,
            segdb_url=segdb_url,
            logger=logger,
         )

    ### remove any times specified on the command line
    logger.info('excluding segments : '+str(exclude))
    segs = utils.remove_segments(segs, exclude)

    logger.info('retained %.3f sec of livetime'%utils.livetime(segs))

    ### record segments
    if reporter:
        path = reporter.report('segments', segs)
        logger.debug('segments written to '+path)

    return segs


def _fork_and_monitor(func, job_args, logger, start_template, end_template, monitoring_cadence=utils.DEFAULT_MONITORING_CADENCE):
    """
    forks jobs via multiprocessing and monitors them until completion
    """
    ### FIXME: do we want to specify a maximum number of processes? Currently we just spawn all of them...could be bad...
    workers = []
    try:
        ### set up all the jobs
        for job_name, args in job_args.items():
            logger.info( start_template % job_name )
            conn1, conn2 = mp.Pipe()
            proc_args = (conn1, *args)
            proc = mp.Process(target=func, args=proc_args, name=job_name)
            proc.start()
            conn1.close()
            workers.append((proc, conn2, args))

        ### monitor forked jobs, cleaning up as they finish
        manager = utils.CadenceManager(utils.current_gps_time(), monitoring_cadence, delay=0)
        while workers:
            proc, conn2, args = workers.pop()
            if proc.is_alive(): ### still going
                workers.append((proc, conn2, args)) ### add it back in

            else: ### process is done
                if proc.exitcode: ### something went wrong with this process!
                    raise RuntimeError('non-zero returncode for %s (pid=%d)'%(proc.name, proc.pid) )

                proc.terminate() ### make sure we clean up the process
                logger.debug( end_template %(proc.name, conn2.recv()) )
                conn2.close()

            manager.wait()

    except Exception as e:
        while workers:
            proc, conn2, _ = workers.pop(0)
            proc.terminate()
            conn2.close()
        raise e
