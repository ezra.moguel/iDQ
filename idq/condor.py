__description__ = "a python module housing utilities specifically for interfacing with condor"
__author__ = "Reed Essick (reed.essick@ligo.org)"
__doc__ = "\n\n".join([__description__, __author__])

#-------------------------------------------------

import os
import subprocess as sp
from distutils.spawn import find_executable

from . import names

#-------------------------------------------------

BATCH_SUB_TEMPLATE = '''\
universe = %(universe)s
getenv = true
executable = %(executable)s
arguments = "%(gps_start)d %(gps_end)d %(jobdir)s $(nickname)"
accounting_group =  %(accounting_group)s
accounting_group_user = %(accounting_group_user)s
log = %(outdir)s/$(condorname)-$(cluster).log
error = %(outdir)s/$(condorname)-$(cluster).err
output = %(outdir)s/$(condorname)-$(cluster).out
want_graceful_removal = true
kill_sig = 15
%(extra_kwargs)s
queue 1'''

BATCH_DAG_TEMPLATE = '''\
JOB   %(nickname)s %(sub_path)s
VARS  %(nickname)s nickname="%(nickname)s" condorname="%(condorname)s"
RETRY %(nickname)s %(retry)d'''

STREAM_SUB_TEMPLATE = '''\
universe = %(universe)s
getenv = true
executable = %(executable)s
arguments = "%(gps_start)d %(gps_end)d %(config_path)s"
accounting_group =  %(accounting_group)s
accounting_group_user = %(accounting_group_user)s
log = %(outdir)s/$(condorname)-$(cluster).log
error = %(outdir)s/$(condorname)-$(cluster).err
output = %(outdir)s/$(condorname)-$(cluster).out
want_graceful_removal = true
kill_sig = 15
%(extra_kwargs)s
queue 1'''

STREAM_DAG_TEMPLATE = '''\
JOB   %(job_type)s %(sub_path)s
RETRY %(job_type)s %(retry)d'''

DEFAULT_ACCOUNTING_GROUP = 'not.real'
DEFAULT_ACCOUNTING_GROUP_USER = 'albert.einstein'
DEFAULT_REQUEST_CPUS = 1
DEFAULT_REQUEST_MEMORY = 8192
DEFAULT_UNIVERSE = 'vanilla'
DEFAULT_RETRY = 0

#-------------------------------------------------
### general things

def which(prog):
    """!
    Like the which program to find the path to an executable
    """
    out = find_executable(prog)
    if out==None:
        raise RuntimeError("could not find %s in your path, have you built the proper software and source the proper env. scripts?"%(prog))
    return out

def submit_dag(dag_path, block=True):
    """
    submit the condor_dag and monitor it
    return with the dag's returncode
    """
    proc = sp.Popen(['condor_submit_dag', dag_path])
    proc.wait()
    returncode = proc.returncode
    if returncode:
        raise RuntimeError('failed to submit '+dag_path)

    if block:
        proc = sp.Popen(['condor_wait', dag_path+'.dagman.log']) ### FIXME: is this hard-coded naming convention too fragile?
        proc.wait()
        returncode = proc.returncode

    return returncode ### note, if block=False, this will only ever return 0 (otherwise it raises an error first)

#-------------------------------------------------
### condor job generation utilities

def write_batch_sub(job_type, gps_start, gps_end, jobdir, **kwargs):
    """
    writes the sub file and returns its path
    delegates to batch_sub_str
    """
    outdir = names.start_end2dir(gps_start, gps_end, rootdir=jobdir)
    if not os.path.exists(outdir):
        try:
            os.makedirs(outdir)
        except OSError: ### handle possible race conditions...
            pass
    sub_path = os.path.join(outdir, names.start_end2path('condor%s'%job_type, gps_start, gps_end, suffix='sub'))
    with open(sub_path, 'w') as file_obj:
        file_obj.write(batch_sub_str(job_type, gps_start, gps_end, jobdir, outdir, **kwargs))

    return sub_path    

def batch_sub_str(
        job_type,
        gps_start,
        gps_end,
        jobdir,
        outdir,
        accounting_group=DEFAULT_ACCOUNTING_GROUP,
        accounting_group_user=DEFAULT_ACCOUNTING_GROUP_USER,
        universe=DEFAULT_UNIVERSE,
        **extra_kwargs
    ):
    """
    write the string representing the sub file for a condor job
    """
    return BATCH_SUB_TEMPLATE%dict(
        gps_start=gps_start,
        gps_end=gps_end,
        jobdir=jobdir,
        accounting_group=accounting_group,
        accounting_group_user=accounting_group_user,
        outdir=outdir,
        universe=universe,
        executable=which("idq-condor_%s"%job_type),
        extra_kwargs='\n'.join('%s = %s'%item for item in extra_kwargs.items())
    )

#---

def write_batch_dag(job_type, gps_start, gps_end, jobdir, nicknames, retry=DEFAULT_RETRY, **sub_kwargs):
    """
    writes sub file and dag file for a condor job
    delegates to batch_sub and batch_dag_str
    """
    ### write sub
    sub_path = write_batch_sub(job_type, gps_start, gps_end, jobdir, **sub_kwargs)

    ### write dag
    dag_path = sub_path[:-3]+"dag" ### this might be fragile...
    with open(dag_path, 'w') as file_obj:
        file_obj.write(batch_dag_str(sub_path, nicknames, retry=retry))

    return dag_path

def batch_dag_str(sub_path, nicknames, retry=DEFAULT_RETRY):
    '''
    write the string representing the dag for a condor job
    '''
    kwargs = {
        'sub_path':sub_path,
        'retry':retry,
    }

    for key in ['nickname', 'condorname']:
        if key in kwargs: ### do this just to be safe...
            kwargs.pop(key)

    return '\n'.join(BATCH_DAG_TEMPLATE%dict(nickname=nickname, condorname=names.nickname2condorname(nickname), **kwargs) for nickname in nicknames)

#---

def train_dag(gps_start, gps_end, jobdir, nicknames, retry=DEFAULT_RETRY, **sub_kwargs):
    return write_batch_dag('train', gps_start, gps_end, jobdir, nicknames, retry=retry, **sub_kwargs)

def evaluate_dag(gps_start, gps_end, jobdir, nicknames, retry=DEFAULT_RETRY, **sub_kwargs):
    return write_batch_dag('evaluate', gps_start, gps_end, jobdir, nicknames, retry=retry, **sub_kwargs)

def calibrate_dag(gps_start, gps_end, jobdir, nicknames, retry=DEFAULT_RETRY, **sub_kwargs):
    return write_batch_dag('calibrate', gps_start, gps_end, jobdir, nicknames, retry=retry, **sub_kwargs)

def timeseries_dag(gps_start, gps_end, jobdir, nicknames, retry=DEFAULT_RETRY, **sub_kwargs):
    return write_batch_dag('timeseries', gps_start, gps_end, jobdir, nicknames, retry=retry, **sub_kwargs)

def batch_dag(gps_start, gps_end, jobdir, nicknames, retry=DEFAULT_RETRY, **sub_kwargs):
    return write_batch_dag('batch', gps_start, gps_end, jobdir, nicknames, retry=retry, **sub_kwargs)

#---

def write_stream_sub(job_type, gps_start, gps_end, config_path, jobdir, **kwargs):
    """
    writes the sub file and returns its path
    delegates to stream_sub_str
    """
    outdir = names.start_end2dir(gps_start, gps_end, rootdir=jobdir)
    if not os.path.exists(outdir):
        try:
            os.makedirs(outdir)
        except OSError: ### handle possible race conditions...
            pass
    sub_path = os.path.join(outdir, names.start_end2path('condorstream%s'%job_type, gps_start, gps_end, suffix='sub'))
    with open(sub_path, 'w') as file_obj:
        file_obj.write(stream_sub_str(job_type, gps_start, config_path, gps_end, jobdir, outdir, **kwargs))

    return sub_path

def stream_sub_str(
        job_type,
        gps_start,
        gps_end,
        config_path,
        outdir,
        accounting_group=DEFAULT_ACCOUNTING_GROUP,
        accounting_group_user=DEFAULT_ACCOUNTING_GROUP_USER,
        universe=DEFAULT_UNIVERSE,
        **extra_kwargs
    ):
    """
    write the string representing the sub file for a condor job
    """
    return STREAM_SUB_TEMPLATE%dict(
        gps_start=gps_start,
        gps_end=gps_end,
        config_path=os.path.abspath(os.path.realpath(config_path)),
        accounting_group=accounting_group,
        accounting_group_user=accounting_group_user,
        outdir=outdir,
        universe=universe,
        executable=which("idq-streaming_%s"%job_type),
        extra_kwargs='\n'.join('%s = %s'%item for item in extra_kwargs.items())
    )

JOB_TYPES = ['train', 'evaluate', 'calibrate', 'timeseries', 'report']
def stream_dag(gps_start, gps_end, config_path, jobdir, skip_report=False, retry=DEFAULT_RETRY, **sub_kwargs):
    outdir = names.start_end2dir(gps_start, gps_end, rootdir=jobdir)
    if not os.path.exists(outdir):
        try:
            os.makedirs(outdir)
        except OSError: ### handle possible race conditions...
            pass
    dag_path = os.path.join(outdir, names.start_end2path('condorstream', gps_start, gps_end, suffix='sub'))

    with open(dag_path, 'r') as file_obj:
        for job_type in JOB_TYPES:
            if (not skip_report) or (job_type!='report'):
                sub_path = write_stream_sub(job_type, gps_start, gps_end, config_path, jobdir, **sub_kwargs)
                file_obj.write(STREAM_SUB_TEMPLATE%{'sub_path':sub_path, 'job_type':job_type, 'retry':retry})

    return dag_path
