__description__ = "a python module housing SNAX-based classifier datum"
__author__ = "Patrick Godwin (patrick.godwin@ligo.org)"
__doc__ = "\n\n".join([__description__, __author__])

#-------------------------------------------------

from collections import defaultdict
import os
import time
import timeit

import numpy as np

from astropy.table import vstack
from gwpy.table import EventTable
from gwpy.table.filters import in_segmentlist
from ligo.segments import segment, segmentlist

from ... import configparser
from ... import exceptions
from ... import features
from ... import names
from ... import utils
from ... import hookimpl
from . import DataLoader


DEFAULT_CHUNK_SIZE = 60

#-------------------------------------------------
### dataloader implementations


class SNAXDataLoader(DataLoader):
    """an extension meant to read SNAX features off disk

    We assume the following directory structure:
        ${rootdir}/${basename}/${basename}-${gpsMOD1e5}/${basename}-${start}-${dur}.h5

    """
    _default_columns = utils.SNAX_COLUMNS
    _allowed_columns = utils.SNAX_COLUMNS
    _required_kwargs = ['rootdir', 'basename']

    def _load_table(self, cache, channels, selection, verbose=False):
        ### load features
        table = EventTable.read(
            cache,
            channels=channels,
            format='hdf5.snax',
            columns=self.columns + ['channel'],
            selection=selection,
            nproc=self.kwargs.get('nproc', 1),
            compact=True,
            verbose=verbose,
        )

        ### skip groupby if table is empty after selection
        data = {}
        if not table:
            return data

        ### group by channel and drop channel column
        table = table.group_by('channel')
        table.remove_column('channel')
        for key, group in zip(table.groups.keys, table.groups):
            channel = table.meta['channel_map'][key['channel']]
            data[channel] = EventTable(group, copy=True)

        return data

    def _query(self, channels=None, bounds=None, verbose=False, **kwargs):
        """Workhorse data discovery method for SNAX hdf5 files.
        """
        segs = self.kwargs.get('segs', self.segs)

        ### set up cache, filtering by segments
        filename = names.basename2snax_filename(self.kwargs['basename'])
        cache = utils.path2cache(self.kwargs['rootdir'], os.path.join("*", filename))
        cache = [entry for entry in cache if segs.intersects_segment(entry.segment)]

        ### set up filters for selection
        selection = [(utils.SNAX_TIMECOL, in_segmentlist, segs)]
        if bounds:
            for col, (min_, max_) in bounds.items():
                selection.extend([f"{col} >= {min_}", f"{col} <= {max_}"])

        ### iteratively load features, combining their results
        data = defaultdict(list)
        for subcache in utils.chunker(cache, DEFAULT_CHUNK_SIZE):
            chunk = self._load_table(subcache, channels, selection, verbose=verbose)
            for channel, table in chunk.items():
                data[channel].append(table)

        for channel in data.keys():
            data[channel] = vstack(data[channel], join_type="exact")

        ### fill in missing channels
        if channels:
            dtype = [(col, 'float') for col in self.columns]
            for channel in channels:
                if channel not in data:
                    data[channel] = EventTable(data=np.array([], dtype=dtype))

        return data


class SNAXKafkaDataLoader(DataLoader):
    """an extension meant to load streaming SNAX features from Kafka.

    Intended to keep a running current timestamp, and has the ability to poll for new data
    and fill its own ClassifierData objects for use when triggers are to be retrieved.

    NOTE: when called, this will cache all trigger regardless of the bounds. This is done
    to avoid issues with re-querying data from rolling buffers, which is not guaranteed to
    return consistent results. Instead, we record everything we query and filter. 
    """
    _default_columns = utils.SNAX_COLUMNS
    _allowed_columns = utils.SNAX_COLUMNS
    _required_kwargs = ['group', 'port', 'topic', 'poll_timeout', 'retry_cadence', 'sample_rate']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        ### link to Kafka consumer if one has not already been assigned
        if 'CONSUMER' not in self.kwargs:
            ### check if Kafka is installed
            utils.check_kafka()

            ### create a Kafka consumer
            self.kwargs['CONSUMER'] = utils.KafkaConsumer(
                self.kwargs['group'],
                self.kwargs['port'],
                topics=[self.kwargs['topic']],
                **self.kwargs.get('kafka_kwargs', {})
            )

            ### assign to specific partition to allow seeks/consumes
            self.kwargs['CONSUMER'].assign([
                utils.kafka.TopicPartition(self.kwargs['topic'], partition=self.kwargs['CONSUMER'].partition),
            ])

        ### dtype for query results
        self._dtype = [(col, 'float') for col in self.columns]

    def _query(self, channels=None, verbose=False, **kwargs):
        """NOTE: we intentionally ignore bounds here and just store everything
        """
        if not self._data: ### NOTE: this forces data to only be populated once

            data = defaultdict(list)

            if not utils.livetime(self.segs): ### we'll never find any data in the segments because they have zero livetime
                self._data = {}
                if channels is not None:
                    for channel in channels:
                        self._data[channel] = np.array([], dtype=self._dtype)
                return self._data

            ### there is the possibility of finding some data, so we go looking for that
            ### NOTE: assume last processed timestamp is pointed just before this span
            stride = 1. / self.kwargs['sample_rate']
            timestamp = self.start - stride
            segs = segmentlist([segment(self.start, self.end)]) ### the span of this object

            ### attempt to seek to correct offset
            try:
                self.kwargs['CONSUMER'].seek_timestamp(self.kwargs['topic'], self.start, stride)
            except utils.kafka.KafkaException as e:
                print(repr(e))

            ### check if timestamps fall within span of classifier data
            stop = self.end - stride
            while timestamp < stop:

                ### poll for data at requested offset
                start_time = timeit.default_timer()
                new_timestamp, data_buffer = self.kwargs['CONSUMER'].retrieve(timeout=self.kwargs['poll_timeout'])

                if data_buffer:
                    if verbose:
                        print('    timestamp of buffer: %.6f'%new_timestamp)
                        print('    latency: %.6f'%utils.gps2latency(new_timestamp))

                    if new_timestamp == timestamp+stride: ### data is contiguous, which is GOOD
                        if utils.time_in_segments(new_timestamp, self.segs): ### we care about this data

                            ### fill in all data from buffer
                            for channel, rows in data_buffer['features'].items():
                                data[channel].extend([{col: row[col] for col in self.columns} for row in rows if row])

                        else:
                            pass ### we don't care about this data, so we just mosey along

                    else: ### data is NOT contiguous, which could be BAD
                        gap = segment(timestamp+stride, new_timestamp)

                        ### check whether the gap touched things we care about, which is BAD
                        if utils.livetime(utils.segments_intersection(self.segs, segmentlist([gap]))):
                            newer_timestamp = max(utils.floor_div(timestamp, self.end - self.start), self.end) ### move to the next stride
                            self.kwargs['CONSUMER'].seek_timestamp(self.kwargs['topic'], newer_timestamp, stride) ### reset the consumer's position
                            raise exceptions.IncontiguousDataError(gap[0], gap[1], newer_timestamp) ### report the error

                        else:
                            pass ### there is a gap, but it doesn't touch anything we actually want so who cares?

                    timestamp = new_timestamp ### update the timestamp to be the new thing

                ### wait to poll for new data
                elapsed = timeit.default_timer() - start_time
                sleep_time = self.kwargs['retry_cadence'] - min(elapsed, self.kwargs['retry_cadence'])
                time.sleep(sleep_time)

            ### timestamp >= stop
            if data: ### if we received the full data requested, return that

                ### combine rows together
                for channel, rows in data.items():
                    data[channel] = EventTable(rows=rows, names=self.columns)

                ### filter by segs if needed
                if not utils.livetime(self.segs) == utils.livetime(utils.segments_intersection(self.segs, segs)):
                    for channel, table in data.items():
                        data[channel] = table.filter((utils.SNAX_TIMECOL, in_segmentlist, self.segs))

                self._data = data

            else: ### did not find ANY data within the requested time
                raise exceptions.NoDataError(self.start, self.end-self.start)

        ### fill in missing channels
        if channels:
            for channel in channels:
                if channel not in self._data: ### ignore bounds here
                    self._data[channel] = EventTable(data=np.array([], dtype=dtype))

        ### make sure we record that we cached everything for all channels present
        bounds = {} ### we do not impose any bounds
        for channel in self._data: ### update what is considered cached
            if channel in self._cached: ### not yet cached recoreded as cached
                self._cached[channel] = bounds ### we record that we cached everything for all channels

        return self._data


#-------------------------------------------------
### plugin implementations

@hookimpl
def get_dataloaders():
    return {
        "snax": SNAXDataLoader,
        "snax:hdf5": SNAXDataLoader,
        "snax:kafka": SNAXKafkaDataLoader,
    }
