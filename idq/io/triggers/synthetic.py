__description__ = "a python module housing synthetic-based classifier data"
__author__ = "Reed Essick (reed.essick@ligo.org)"
__doc__ = "\n\n".join([__description__, __author__])

#-------------------------------------------------

import numpy as np

from ligo.segments import segment, segmentlist

from ... import configparser
from ... import exceptions
from ... import synthetic
from ... import utils
from ... import hookimpl
from . import DataLoader


#-------------------------------------------------
### classifier data implementations


class MockDataLoader(DataLoader):
    """
    a synthetic data generation object
    """
    _default_columns = synthetic.MOCKCLASSIFIER_COLUMNS
    _allowed_columns = synthetic.MOCKCLASSIFIER_COLUMNS
    _required_kwargs = ['config']

    _default_prob_nodata = 0.
    _default_prob_incontiguous = 0.

    def __init__(self, *args, **kwargs):
        super().__init__(self, *args, **kwargs)
        self._parse_config(self.kwargs['config'])
        self._dtype = [(key, float) for key in self.columns]

        if 'prob_nodata' not in self.kwargs:
            self.kwargs['prob_nodata'] = self._default_prob_nodata
        if 'prob_incontiguous' not in self.kwargs:
            self.kwargs['prob_incontiguous'] = self._default_prob_incontiguous

    def _parse_config(self, path):
        """
        read in parameters from config by delegating to idq.synthetic.parse_mockclassifierdata_config
        """
        self._streams, self._channel2streams = configparser.path2streams(path, self.segs)

    def _query(self, channels=None, bounds=None, verbose=False, **kwargs):
        """
        generates synthetic data on-the-fly based on parameters read from config file
        generates and caches data for all channels declared in config instead of trying to only generate the channels requested
        """
        if bounds is None:
            bounds = dict()

        if self._data:
            synthetic_data = dict(self._data) ### handle caching explicitly here because otherwise calls to triggers() without specifying channels
                                              ### will re-generate the random streams. This is special behavior just for the Synthetic data objects
                                              ### so we take care of it here rather than in the parent class
                                              ### relies on the fact that we generate all data for all channels the first time this is called

        else: ### generate all streams of synthetic triggers

            ### figure out if we want to raise an error
            if np.random.rand() < self.kwargs['prob_nodata']: ### raise a NoDataError
                raise exceptions.NoDataError(self.start, self.end-self.start)

            if np.random.rand() < self.kwargs['prob_incontiguous']: ### raise BadSpanError
                gap_start = self.start+np.random.rand()*(self.end-self.start)
                gap_end = gap_start + np.random.rand()(self.end-gap_start)
                raise exceptions.IncontiguousDataError(gap_start, gap_end, self.end)

            ### no errors raised, so we simulate streams
            all_streams = dict((obj.name, obj) for obj in self._streams)

            ### map those streams into the appropriate witnesses
            synthetic_data = {}
            for channel, streams in self._channel2streams.items():
                arrays = [np.array([], dtype=self._dtype)] ### incrementally add streams to this for concatenation at the end
                for stream, jitters in streams: ### add jitter to each observed stream
                    arrays.append(stream.jittered(**jitters)[self.columns]) ### add jitter and downselect to the columns requested
                synthetic_data[channel] = np.concatenate(tuple(arrays))

            if synthetic.MOCKCLASSIFIER_TIME in self.columns: ### time-order the data if we've requested the time column
                for channel, val in synthetic_data.items():
                    synthetic_data[channel] = val[val[synthetic.MOCKCLASSIFIER_TIME].argsort()]

        if channels is not None:
            for chan in channels: ### fill in missing channels
                if chan not in synthetic_data:
                    synthetic_data[chan] = np.array([], dtype=self._dtype)

        ### NOTE:
        ###     we do not filter by "bounds" here because this will happen within ClassifierData.triggers() and we do not expect the
        ###     memory load associated with generating synthetic data to be large enough that we need to filter at this step
        return synthetic_data


### FIXME: rewrite to not use UmbrellaClassifierData
#class DynamicMockDataLoader(UmbrellaDataLoader):
#    """
#    a synthetic data generation object that allows for multiple correlation configs
#    """
#    _default_columns = synthetic.MOCKCLASSIFIER_COLUMNS
#    _allowed_columns = synthetic.MOCKCLASSIFIER_COLUMNS
#    _required_kwargs = ['configs']
#
#    def __init__(self, *args, **kwargs):
#        ClassifierData.__init__(self, *args, **kwargs)
#
#        ### make children as needed
#        self.kwargs['configs'] = configparser.config2bounds(self.kwargs['configs'])
#        ans = [(segmentlist([segment(s, e)]), path) for path, (s, e) in self.kwargs['configs'].items()]
#        ans.sort(key=lambda _: _[0][0][0])
#        for i, (segs1, path1) in enumerate(ans):
#            for segs2, path2 in ans[i+1:]:
#                if utils.livetime(utils.segments_intersection(segs1, segs2)):
#                    raise ValueError('overlapping segments for %s [%.3f, %.3f) and %s [%.3f, %.3f)'%(path1, segs1[0][0], segs1[0][1], path2, segs2[0][0], segs2[0][1]))
#
#        for segments, path in ans: ### create children as needed
#            intersection = utils.segments_intersection(self.segs, segments)
#            if utils.livetime(intersection): ### this child is relevant, so we include it
#                start = intersection[0][0]
#                end = intersection[-1][1]
#                self.extend_child(MockClassifierData(start, end, segs=intersection, config=path, **self.kwargs))


#-------------------------------------------------
### plugin implementations

@hookimpl
def get_dataloaders():
    return {
        "synthetic": MockDataLoader,
        "synthetic:single": MockDataLoader,
        #"synthetic:multi": DynamicMockDataLoader,
    }
