__description__ = "a python module housing omicron-based classifier datum"
__author__ = "Reed Essick (reed.essick@ligo.org), Patrick Godwin (patrick.godwin@ligo.org)"
__doc__ = "\n\n".join([__description__, __author__])

#-------------------------------------------------

import glob
import os

import numpy as np
from tqdm import tqdm

from gwpy.table import EventTable
from gwpy.table.filters import in_segmentlist
from gwtrigfind import find_trigger_files
from lal.utils import CacheEntry
from ligo.segments import segment, segmentlist

from ... import utils
from ... import hookimpl
from . import DataLoader


#-------------------------------------------------
### classifier data implementations

class OmicronDataLoader(DataLoader):
    """an extension meant to read Omicron triggers off disk
    """
    _default_columns = utils.OMICRON_COLUMNS
    _allowed_columns = utils.OMICRON_COLUMNS
    _required_kwargs = ['instrument']

    _suffix = 'h5'

    def _query(self, channels=None, bounds=None, verbose=False, **kwargs):
        if bounds is None:
            bounds = {}
        if channels is None:
            ifo = self.kwargs['instrument']
            rootdir = os.path.join(os.sep, 'home', 'detchar', 'triggers', ifo)
            channels = [f"{ifo}:{channel.replace('_OMICRON', '')}" for channel in os.listdir(rootdir)]

        data = {}
        dtype = [(key, 'float') for key in self.columns]

        ### set up filters for selection
        selection = [(utils.OMICRON_TIMECOL, in_segmentlist, self.segs)]
        if bounds:
            for col, (min_, max_) in bounds.items():
                selection.extend([f"{col} >= {min_}", f"{col} <= {max_}"])

        ### read in triggers
        for channel in tqdm(channels, desc="loading data", disable=not verbose):
            ### generate file cache and filter by segments
            ### NOTE: some production Omicron files are found
            ### with insufficient permissions which can cause
            ### problems. we can optionally skip over these
            ### problematic files with `skip_bad_files`.
            try:
                cache = find_trigger_files(channel, 'omicron', self.start, self.end, ext=self._suffix)
            except ValueError:
                continue
            else:
                cache = [CacheEntry.from_T050017(path) for path in cache]
                cache = [c for c in cache if self.segs.intersects_segment(c.segment)]
                if self.kwargs.get('skip_bad_files', False):
                    cache = [c for c in cache if os.access(c.path, os.R_OK)]
            if not cache:
                continue

            ### read in triggers
            data[channel] = EventTable.read(
                cache,
                path='triggers',
                format='hdf5',
                columns=self.columns,
                selection=selection,
                nproc=self.kwargs.get('nproc', 1),
            )

        ### fill in missing channels
        for channel in channels:
            if channel not in data:
                data[channel] = EventTable(data=np.array([], dtype=dtype))

        return data


#-------------------------------------------------
### plugin implementations

@hookimpl
def get_dataloaders():
    return {
        "omicron": OmicronDataLoader,
        "omicron:hdf5": OmicronDataLoader,
    }
