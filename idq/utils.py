__description__ = "a python module housing basic utilities"
__author__ = "Reed Essick (reed.essick@ligo.org)"
__doc__ = "\n\n".join([__description__, __author__])

#-------------------------------------------------

import bisect
import functools
import glob
import itertools
import functools
import json
import os
import socket
import sys
import time as TIME
import timeit

import traceback

from collections import defaultdict

import numpy as np

from lal import gpstime
from lal.utils import CacheEntry

from ligo.segments import segment, segmentlist
from dqsegdb2.query import query_segments

try: ### do this so we can run without kafka installed
    import confluent_kafka as kafka
except ImportError:
    kafka = None

from . import exceptions


#-------------------------------------------------
### reference variables that may be useful throughout


# cadence management
DEFAULT_STRIDE = 1.0
DEFAULT_DELAY = 0.0

DEFAULT_MAX_ITERS = np.infty ### the maximum number of iterations in CadenceManager.yield before moving on
DEFAULT_MAX_LATENCY = np.infty ### the maximum latency we allow before skipping ahead
DEFAULT_MAX_TIMESTAMP = np.infty

DEFAULT_MONITORING_CADENCE = 1.0

# I/O ascii conventions
ASCII_COMMENTS = ["#"]

# I/O columns conventions
KW_COLUMNS = 'start_time stop_time time frequency unnormalized_energy normalized_energy chisqdof significance'.split()
KW_TIMECOL = 'time'
KW_SIGCOL = 'significance'
KW_COL2IND = dict((col, i) for i, col in enumerate(KW_COLUMNS))
KW_TIMEIND = KW_COL2IND[KW_TIMECOL]

KW_TEMPLATE = '%.6f %.6f %.6f %d %.3e %.2f %d %.2f'

SNAX_COLUMNS = 'time frequency q snr phase duration'.split()
SNAX_TIMECOL = 'time'
SNAX_SIGCOL = 'snr'

OMICRON_COLUMNS = 'time frequency tstart tend fstart fend snr q amplitude phase'.split()
OMICRON_TIMECOL = 'time'
OMICRON_SIGCOL = 'snr'

# segdb conventions
DEFAULT_SEGDB_URL = os.environ.get('DEFAULT_SEGMENT_SERVER', 'https://segments.ligo.org')

#-------------------------------------------------
### misc utilities

def check_kafka():
    """
    a tiny function to unify how we determine whether kafka was imported correctly
    """
    if kafka is None:
        raise ImportError("you're attempting to use a Kafka backend, but confluent_kafka could not be imported")

def parse_variable_args(args, max_args, default=None):
    """
    a convenience function to fill in unspecified variable arguments with a default value.
    """
    return args + [default for ii in range(max_args-len(args))]

#-------------------------------------------------
### utilities to help with cadence and sleep logic

class CadenceManager(object):
    """
    an object that manages the cadence of our queries, including delay and timeout logic
    """
    # define a few string templates that may be printed repeatedly
    _sleep_template = 'sleeping for %.3f sec'

    def __init__(
            self,
            timestamp=None,
            stride=DEFAULT_STRIDE,
            delay=DEFAULT_DELAY,
            logger=None,
            enforce_integer_strides=True,
            offset=0,
            max_iters=DEFAULT_MAX_ITERS,
            max_latency=DEFAULT_MAX_LATENCY,
            max_timestamp=DEFAULT_MAX_TIMESTAMP, ### this will likely be set to something like "gps_end" in the streaming jobs
            **extra_kwargs
        ):
        self._enforce_integer_strides = enforce_integer_strides ### start the timestamp off at an integer number of strides
        self._stride = stride
        self._offset = offset
        if timestamp is None:
            timestamp = current_gps_time()
        self.timestamp = timestamp ### timestamp setter depends on _stride, _enforce_integer_strides, _offset

        ### save the rest
        self._delay = delay
        self._wait_duration = stride+delay ### timestamp tracks the start of the stride, so we need to wait this long

        self.max_timestamp = max_timestamp

        assert max_iters > 0, 'max_iters must be non-zero'
        self.max_iters = max_iters

        assert max_latency > self._stride, 'max_latency must be larger than stride'
        self.max_latency = max_latency

        self._logger = logger ### hold on to this to print sleep statements as necessary

        self._extra_kwargs = extra_kwargs ### gobble this up in case there's anything useful here, but we probably won't ever use them

    @property
    def timestamp(self):
        return self._timestamp

    @timestamp.setter
    def timestamp(self, new_timestamp):
        if self._enforce_integer_strides:
            new_timestamp = floor_div(new_timestamp, self.stride)
        self._timestamp = new_timestamp + self.offset

    @property
    def stride(self):
        return self._stride

    @property
    def delay(self):
        return self._delay

    @property
    def offset(self):
        return self._offset

    @property
    def max_timestamp(self):
        return self._maxtimestamp

    @max_timestamp.setter
    def max_timestamp(self, timestamp):
        self._max_timestamp = timestamp
        self._end = timestamp + self._delay + self._offset

    @property
    def segs(self):
        return segmentlist([self.seg])

    @property
    def seg(self):
        return segment(self.timestamp, self.timestamp+self.stride)

    @property
    def past_seg(self):
        return segment(self._timestamp-self._stride, self._timestamp)

    @property
    def timeout(self):
        return self._timestamp+self._wait_duration - min(self._end, current_gps_time())

    def wait(self):
        """
        wait until the current time is after timestamp+delay
        then update timestamp (increment by one stride)
        """
        wait = self.timeout
        if wait > 0:
            if self._logger is not None:
                self._logger.info(self._sleep_template%wait)
            TIME.sleep(wait)

        self._timestamp += self._stride ### increment timestamp since we've waited
        return self.past_seg

    def poll(self):
        """
        a generator that returns segments corresponding to strides up until the current time
        This is essentially a multi-stide version of wait
        """
        i = 0
        while (self.timeout < 0):
            if gps2latency(self._timestamp) > self.max_latency: ### we're too far behind real time!
                gps = current_gps_time()
                raise exceptions.LatencyError(gps, gps2latency(self._timestamp), floor_div(gps, self._stride))

            if i < self.max_iters:
                self._timestamp += self._stride
                i += 1
                yield self.past_seg ### we've already bumped timestamp

            else: ### too many iterations
                return

        self.wait()
        yield self.past_seg ### wait automatically bumps the timestamp

#-------------------------------------------------
### draw random times (clean samples)
def draw_random_times(segments, rate=0.1):
    """
    draw random times poisson distributed within segments with corresponding rate (in Hz)
    """
    times = []
    for seg in segments:
        times += draw_random_times_in_segment(*seg, rate=rate)
    return np.array(times, dtype=float).reshape((len(times),))

def draw_random_times_in_segment(start, end, rate=0.1):
    """
    draw random times poisson distributed within [start, end] with corresponding rate (in Hz)
    """
    times = []
    while start < end:
        for dt in draw_poisson_dt(rate, size=int(1+int(end-start)*rate)): ### minimize calls to draw_poisson_dt (expensive?)
            start += dt
            if start > end: ### we're out of this segment, so break
                break
            times.append(start)

    return times

def draw_poisson_dt(rate, size=1):
    return -np.log(1.-np.random.rand(size))/rate

def floor_div(x, n):
    """
    Floor a number by removing its remainder
    from division by another number n.

    >>> floor_div(163, 10)
    160
    >>> floor_div(158, 10)
    150

    """
    assert n > 0

    if isinstance(x, int):
        return (x // n) * n
    else:
        return (float(x) // n) * n

def time2window(time, duration):
    """
    Return the start and end time corresponding to the span rounded down to
    the nearest window time.

    >>> time2window(12335, 100)
    (12300, 12400)
    >>> time2window(12300, 10)
    (12300, 12310)

    """
    return floor_div(time, duration), floor_div(time, duration) + duration

def segs2times(segs, dt):
    """
    Given a segment list, returns a list of inclusive ranges of times,
    spaced by an interval dt, which spans these segments.

    >>> seg1 = segment(12300, 12302)
    >>> seg2 = segment(12345, 12349)
    >>> segs = segmentlist([seg1, seg2])
    >>> segs2times(segs, 1)
    [array([12300., 12301.]), array([12345., 12346., 12347., 12348.])]

    """
    times = []
    for s, e in segs:
        times.append( np.arange(float(s), float(e), dt) )
    return times

def filter_triggers(triggers, bounds=dict()):
    """
    filter the triggers by the associated bounds. Assumes triggers are a numpy structured array and returns an equivalent array of only the surviving triggers
    bounds is a dictionary of the form: (column, (minimum, maximum))
    """
    truth = np.ones(len(triggers), dtype=bool)
    for key, (_min, _max) in bounds.items():
        in_range = np.logical_and(_min <= triggers[key], triggers[key] <= _max)
        truth = np.logical_and(in_range, truth)
    return triggers[truth]

def current_gps_time():
    """
    A convenience function to return the current gps time.
    """
    return int(gpstime.tconvert('now'))

def gps2latency(gps_time):
    """
    Given a gps time, measures the latency to ms precision relative to now.
    """
    return np.round(current_gps_time() + (TIME.time()%1) - gps_time, 3)

#-------------------------------------------------
### utility functions for segments that may be useful throughout

def segdb2segs(start, end, intersect=None, exclude=None, segdb_url=DEFAULT_SEGDB_URL, logger=None):
    """
    query SegDb through the Python API directly
    adapted from a function origionaly written by Chris Pankow
    """
    try:
        segs = segmentlist([segment(start, end)]) ### beginning segments

        ### intersect with the segments we want to keep
        if isinstance(intersect, str):
            intersect = [intersect]
        if intersect:
            for flag in intersect:
                segs = segments_intersection(segs, segdb2active(start, end, flag, segdb_url))

        ### remove the segments we want removed
        if isinstance(exclude, str):
            exclude = [exclude]
        if exclude:
            for flag in exclude:
                segs = remove_segments(segs, segdb2active(start, end, flag, segdb_url))

    except Exception as e: ### catch internal segdb error (line 114 of ~dqsegdb/urifunctions.py)
        if logger is not None:
            traceback_string = traceback.format_exc().strip().replace('\n',' ')
            logger.warn('exception encountered when querying SegDb (%s). Returning an empty segment list!'%traceback_string)
        segs = segmentlist([])

    ### return the resulting list
    return segs

def segdb2active(start, end, flag, segdb_url=DEFAULT_SEGDB_URL):
    """
    return the active segments for a flag
    """
    return query_segments(flag, start, end, host=segdb_url, coalesce=True)["active"]

def start_dur2start_end(x, sep='_', type_=int):
    start, dur = map(type_, x.split(sep))
    return start, start + dur

def float2sec_ns(x):
    sec = np.array(x).astype(int)
    ns = np.rint((x-sec)*1e9)
    return sec, ns

def sec_ns2float(sec, ns):
    return sec + 1.e-9*ns

def times2segments(times, win):
    return segmentlist([segment(time-win, time+win) for time in times]).coalesce()

def time_in_segment(t, seg):
    """
    return True iff time is in segment

    >>> seg = segment(12300, 12302)
    >>> time_in_segment(12346, seg)
    False

    """
    #return t in seg
    return bool((seg[0] <= t)*(t < seg[1])) ### we use this so it is numpy.ndarray friendly (see times_in_segments)

def times_in_segment(times, seg):
    """
    like time_in_segment, but deals with an array instead of just a scalar
    """
    return np.logical_and(seg[0] <= times, times < seg[1])

def time_in_segments(t, segments):
    """
    return True iff time is in segments

    >>> seg1 = segment(12300, 12302)
    >>> seg2 = segment(12345, 12349)
    >>> segs = segmentlist([seg1, seg2])
    >>> time_in_segments(12346, segs)
    True

    """
    return np.any([time_in_segment(t, seg) for seg in segments])

def times_in_segments(times, segments):
    """
    returns a boolean array with the same shape as times representing whether they are within segments
    """
    #return np.array([segmentlist([segment(t,t)]).intersects(segments) for t in times])
    truth = np.zeros_like(times, dtype=bool)
    for seg in segments:
        truth = np.logical_or(times_in_segment(times, seg), truth)
    return truth

def segments_overlap(seg1, seg2):
    """
    return True iff segments overlap

    >>> seg1 = segment(12300, 12302)
    >>> seg2 = segment(12345, 12349)
    >>> segments_overlap(seg1, seg2)
    False

    """
    #return seg1.intersects(seg2) ### replaced previous segments-based solution with this one that should work
    s1, e1 = seg1
    s2, e2 = seg2
    return (s1<=s2<e1) or (s1<e2<=e1) or (s2<=s1<e2) or (s2<e1<=e2)

def livetime(segments):
    """
    return the livetime spanning the segment list
    NOTE: assumes segments are non-overlapping

    >>> seg1 = segment(12300, 12302)
    >>> seg2 = segment(12345, 12349)
    >>> segs = segmentlist([seg1, seg2])
    >>> livetime(segs)
    6

    """
    return np.sum([seg[1]-seg[0] for seg in segments])

def segments_intersection(*segments_lists):
    """
    return a new segment list corresponding to the intersection of these lists of segments
    """
    N = len(segments_lists)
    if N==0:
        raise ValueError('please supply at least one segment list')
    elif N==1:
        return segmentlist(segments_lists[0])
    else:
        return segmentlist(segments_lists[0]) & segments_intersection(*segments_lists[1:])

def segments_union(*segments_lists):
    """
    return a new segment list corresponding to the union of these lists of segments
    """
    N = len(segments_lists)
    if N==0:
        raise ValueError('please supply at least one segment list')
    elif N==1:
        return segmentlist(segments_lists[0])
    else:
        return segmentlist(segments_lists[0]) | segments_union(*segments_lists[1:])

def remove_segments(segments, segments2remove):
    """
    removes segments2remove from segments
    """
    return segments - segments2remove

def expand_segments(segments, span):
    """
    expand segments to a minimum span
    """
    expanded_segs = segmentlist([])
    for seg in segments:
        segspan = seg[1] - seg[0]
        if segspan < span:
            dspan = span - segspan
            expanded_segs.append(seg.protract(dspan/2))
        else:
            expanded_segs.append(seg)
    expanded_segs.coalesce()
    return expanded_segs

def segments_causal_kfold(segments, num_folds, initial_segs=None):
    """
    Split segments into K equal folds, returning a
    list over all possible K splits.

    This is different from a regular K-fold split in that
    successive splits are supersets from the previous iteration.

    Return segments in the form [(k-1) folds, k th fold].

    Also can take in initial_segs, otherwise the first fold will have an empty
    training fold.
    """
    kfold_segments = split_segments_by_livetime(segments, num_folds)

    folds = []
    for k in range(num_folds):
        current_folds = segmentlist(itertools.chain(*kfold_segments[:k]))
        if initial_segs is not None:
            current_folds.extend(initial_segs)
        current_folds.coalesce()
        new_fold = kfold_segments[k]
        folds.append((current_folds, new_fold))

    return folds

def segments_kfold(segments, num_folds):
    """
    Split segments into K equal folds, returning a
    list over all possible K splits.

    Return segments in the form [(k-1) folds, 1 fold].
    """
    return segments_checkerboard(segments, num_folds, 1)

def segments_checkerboard(segments, num_bins, num_segs_per_bin):
    """
    split segments into num_bins*num_segs_per_bin equal folds and then associate segments within bins in a checkerboard pattern.
    return a list over segmentlists, one for each bin
    """
    kfold_segments = split_segments_by_livetime(segments, num_bins*num_segs_per_bin)

    folds = [segmentlist([]) for _ in range(num_bins)]
    for i, segs in enumerate(kfold_segments):
        folds[i%num_bins] += segs

    return [(remove_segments(segments, segs), segs) for segs in folds]

def split_segments_by_livetime(segments, num_folds):
    """
    Split segments into K equal folds by livetime,
    returning a list over all folds.
    """
    total_livetime = livetime(segments)
    kfold_segments = [segmentlist([]) for i in range(num_folds)]

    ### calculate livetime for each fold, ensuring
    ### start, end edges fall on integer boundaries
    small_fold, remainder = divmod(float(total_livetime), num_folds)
    big_fold = small_fold + remainder
    kfold_livetime = [big_fold if n == 0 else small_fold for n in range(num_folds)]

    ### determine folds
    fold = 0
    for seg in segments:

        ### add entire segment to current fold if livetime doesn't spill over
        current_livetime = livetime(kfold_segments[fold])
        if current_livetime + livetime(segmentlist([seg])) <= kfold_livetime[fold]:
            kfold_segments[fold] |= segmentlist([seg])

        ### else, split segment and put spill-over into next fold(s)
        else:
            diff_livetime = kfold_livetime[fold] - current_livetime
            needed_seg = segmentlist([segment(seg[0], seg[0] + diff_livetime)])
            kfold_segments[fold] |= needed_seg

            ### if segment is still too big, keep splitting until it isn't
            remainder = segmentlist([segment(seg[0] + diff_livetime, seg[1])])
            while livetime(remainder) > kfold_livetime[fold]:
                remainder_start = remainder[0][0]
                remainder_mid = remainder[0][0] + kfold_livetime[fold]
                kfold_segments[fold+1] |= segmentlist([segment(remainder_start, remainder_mid)])
                remainder = segmentlist([segment(remainder_mid, seg[1])])
                fold += 1

            ### divvy up final piece
            if fold < num_folds-1:
                fold += 1
            kfold_segments[fold] |= remainder

    return kfold_segments

def split_segments_by_stride(start, end, segments, stride):
    """
    Split segments into multiple segments with a maximum
    extent defined by stride.
    Returns a list of segment lists.
    """
    extent = segment(start, end)
    splits = [dsets[0] for dsets in segs2datasets(segments, stride=stride, keytype='segment')]
    return [(split_segs & extent) for split_segs in splits]

#-------------------------------------------------
### utility functions for I/O

def chunker(sequence, chunk_size):
    """
    split up a sequence into sub-sequences of a maximum chunk size
    """
    for i in range(0, len(sequence), chunk_size):
        yield sequence[i:i+chunk_size]

def path2cache(rootdir, pathname):
    """
    given a rootdir and a glob-compatible pathname that may contain shell-style wildcards,
    will find all files that match and populate a Cache.
    NOTE: this will only work with files that comply with the T050017 file convention.
    """
    return [CacheEntry.from_T050017(file_) for file_ in glob.iglob(os.path.join(rootdir, pathname))]

def file2cache(rootdir, cachefile):
    """
    given a rootdir and a cachefile, will load and populate a Cache object.
    NOTE: this will only work with files that comply with the T050017 file convention.
    """
    return [CacheEntry(file_) for file_ in open(os.path.join(rootdir, cachefile))]

def cache2unique_segments(cache):
    """
    given a cache, will return a list of unique segments
    spanned by files contained within the cache
    """
    return list(set(c.segment for c in cache))

SEGS2DATASETS_KEY_TEMPLATE = "%d_%d"
def segs2datasets(segs, stride=32, keytype='string'):
    """
    given a segment list, will give names for all relevant datasets
    that could be within an hdf5 file alongside the relevant segments within
    each dataset in a pair.
    the stride is given for a span of a single dataset.
    """
    pairs = defaultdict(segmentlist)
    for seg in segs:
        gps_start = floor_div(seg[0], stride)
        while gps_start < seg[1]:
            if keytype == 'string':
                key = SEGS2DATASETS_KEY_TEMPLATE%(gps_start, stride)
            elif keytype == 'segment':
                key = segment(gps_start, gps_start + stride)
            else:
                raise ValueError('keytype %s not one of (string/seglist)'%keytype )

            pairs[key].append(seg)
            gps_start += stride

    ### coalesce all segment lists before returning
    pairs = {dset: seglist.coalesce() for dset, seglist in pairs.items()}
    return pairs.items()

def find_unique_segs(seg, seglist):
    """
    given a segment and a sorted list of segments,
    will return all segments that overlap with target segment.
    """
    start_idx = bisect.bisect_left(seglist, seg)
    start_idx = max(start_idx-1, 0) ### guarantee that indexing is within seglist

    end_idx = start_idx
    while end_idx < len(seglist) and seglist[end_idx].intersects(seg):
        end_idx += 1

    return seglist[start_idx:end_idx]

#-------------------------------------------------
### utils to help with pickling kafka objects

class KafkaConsumer(object):
    """
    a wrapper for confluent_kafka.Consumer objects
    """
    def __init__(self, group, port, topics=[], **kafka_kwargs):
        check_kafka()
        kafka_kwargs['group.id'] = group
        kafka_kwargs['bootstrap.servers'] = port
        self.__setstate__({'kafka_kwargs': kafka_kwargs, 'topics': topics}) ### set up the internal objects, but don't subscribe to anything
        self._partition = 0 ### we only ever use one partition, so we hard code this here for common reference
        self._timestamp = 0

    @property
    def kafka_kwargs(self):
        return self._kafka_kwargs

    @property
    def group(self):
        return self.kafka_kwargs['group.id']

    @property
    def partition(self):
        return self._partition

    @property
    def port(self):
        return self.kafka_kwargs['bootstrap.servers']

    @property
    def topics(self):
        return self._topics

    @property
    def consumer(self):
        return self._consumer

    @property
    def timestamp(self):
        return self._timestamp

    ### the following are used to modify how pickle reads/writes these objects
    ### we pass everything we need to know about constructing kafka objects without passing the objects themselves
    def __getstate__(self):
        return {'kafka_kwargs': self.kafka_kwargs, 'topics': self.topics}

    def __setstate__(self, state):
        self._kafka_kwargs = state['kafka_kwargs']
        self._consumer = kafka.Consumer(**state['kafka_kwargs'])
        self._topics = set()
        if state['topics']:
            self.subscribe(state['topics'])

    ### we define a few convenience functions here, but more are accessible via accessing KafkaConsumer.consumer directly
    def assign(self, *args, **kwargs):
        return self.consumer.assign(*args, **kwargs)

    def subscribe(self, topics, *args, **kwargs):
        for topic in topics: ### expects topics to be a list
            self._topics.add(topic)
        return self.consumer.subscribe(topics, *args, on_assign=self.on_assign, **kwargs)

    def offsets_for_times(self, *args, **kwargs):
        return self.consumer.offsets_for_times(*args, **kwargs)

    def poll(self, *args, **kwargs):
        return self.consumer.poll(*args, **kwargs)

    def seek(self, *args, **kwargs):
        return self.consumer.seek(*args, **kwargs)

    def position(self, *args, **kwargs):
        return self.consumer.position(*args, **kwargs)

    def retrieve(self, **kwargs):
        """
        retrieves messages in a timestamp-aware way
        NOTE: timestamp field in message is required
        """
        data_buffer = self.consumer.poll(**kwargs)

        ### retrieve only if there is a message and there isn't an error involved
        if data_buffer and not data_buffer.error():
            data_buffer = json.loads(data_buffer.value())
            data_timestamp = data_buffer['timestamp']
            self._timestamp = data_timestamp

            return data_timestamp, data_buffer

        else:
            return None, None

    def seek_timestamp(self, topic, timestamp, stride):
        """
        seek to the offset in a given topic corresponding to the timestamp specified
        """
        if not (self.timestamp+stride == timestamp): ### only seek if offset needs to be changed
            offset_timestamp = int(timestamp * 1e3) # NOTE: convert to ms
            time_partition = kafka.TopicPartition(topic, partition=self._partition, offset=offset_timestamp)
            self.seek(self.offsets_for_times([time_partition])[0])

    def seek_to_latest(self, topic):
        ### find the low and high watermarks for the current
        ### partition to find the last buffer in the queue
        watermark_partition = kafka.TopicPartition(topic, partition=self._partition)
        low, high = self.consumer.get_watermark_offsets(watermark_partition)

        ### NOTE: seek to this watermark. it is not clear to me whether the low or the high
        ###       watermark should be used, but some initial testing appears to prefer the low better
        offset_partition = kafka.TopicPartition(topic, partition=self._partition, offset=low)
        self.seek(offset_partition)

    @staticmethod
    def on_assign(consumer, partitions):
        """
        callback to assign starting offset when subscribing
        """
        consumer.assign(partitions)

class KafkaProducer(object):
    """
    a wrapper for confluent_kafka.Producer objects
    """
    def __init__(self, group, port, **kafka_kwargs):
        check_kafka()
        kafka_kwargs['group.id'] = group
        kafka_kwargs['bootstrap.servers'] = port
        self.__setstate__({'kafka_kwargs': kafka_kwargs})

    @property
    def kafka_kwargs(self):
        return self._kafka_kwargs

    @property
    def group(self):
        return self.kafka_kwargs['group.id']

    @property
    def port(self):
        return self.kafka_kwargs['bootstrap.servers']

    @property
    def producer(self):
        return self._producer

    ### the following are used to modify how pickle reads/writes these objects
    ### we pass everything we need to know about constructing kafka objects without passing the objects themselves
    def __getstate__(self):
        return {'kafka_kwargs': self.kafka_kwargs}

    def __setstate__(self, state):
        self._kafka_kwargs = state['kafka_kwargs']
        self._producer = kafka.Producer(**state['kafka_kwargs'])

    ### we define a few convenience functions here, but more are accessible via KafkaProducer.producer
    def poll(self, *args, **kwargs):
        return self.producer.poll(*args, **kwargs)

    def produce(self, *args, **kwargs):
        return self.producer.produce(*args, **kwargs)

    def flush(self, *args, **kwargs):
        return self.producer.flush(*args, **kwargs)

#-------------------------------------------------
### debugging utils

def elapsed_time(func):
    """
    measures the elapsed time that a function call makes
    usage: add @elapsed_time decorator to function/method you want to time
    """
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        start_time = timeit.default_timer()
        result = func(*args, **kwargs)
        elapsed = timeit.default_timer() - start_time
        print('%r took %.3f sec'% (func.__name__, elapsed))
        return result

    return wrapper

#-------------------------------------------------
### utility functions for multiprocessing

def unpack(func):
    """
    Unpacks an argument tuple and calls the target function 'func'.
    Used as a workaround for python 2 missing multiprocessing.Pool.starmap.

    Implemented from https://stackoverflow.com/a/52671399.

    """
    @functools.wraps(func)
    def wrapper(arg_tuple):
        return func(*arg_tuple)
    return wrapper
